import logging
from collections.abc import Callable
from functools import wraps
from typing import Any

import authlib.oauth2
import pydantic
from authlib.integrations.flask_oauth2 import ResourceProtector
from flask import Response, abort, flash, make_response, request
from flask_babel import lazy_gettext as _
from flask_login import current_user
from marshmallow import ValidationError
from pydantic import BaseModel

from app.exceptions.base import ValidationException
from app.extensions import login_manager
from app.models.user import User
from app.oauth_scopes import Scopes
from app.service import role_service

_logger = logging.getLogger(__name__)


def require_role(*roles):
    def real_decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            has_role, need_tfa = role_service.user_has_role_and_tfa(
                current_user, *roles
            )

            if has_role:
                return f(*args, **kwargs)
            else:
                if need_tfa and not request.blueprint.startswith("api"):
                    # The user would have been authed if they had tfa enabled.
                    flash(
                        _(
                            "You need two-factor authentication "
                            "enabled to view this page."
                        ),
                        "warning",
                    )

                _logger.debug(
                    f"Denied {request.url}, user {current_user.id}"
                    f" missing " + " ".join(str(r) for r in roles)
                )
                return abort(403)

        return wrapper

    return real_decorator


def require_membership(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if current_user.is_anonymous or not current_user.has_paid:
            flash(_("Membership is required to view this page"), "warning")
            return abort(403)
        else:
            return f(*args, **kwargs)

    return wrapper


def require_favourer_or_membership(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if current_user.is_anonymous or not (
            current_user.has_paid or current_user.favourer
        ):
            flash(_("Membership is required to view this page"), "warning")
            return abort(403)
        else:
            return f(*args, **kwargs)

    return wrapper


def response_headers(headers=None):
    if headers is None:
        headers = dict()

    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            resp = make_response(f(*args, **kwargs))
            h = resp.headers
            for header, value in headers.items():
                h[header] = value
            return resp

        return wrapper

    return decorator


def json_schema(schema):
    def real_decorator(f):
        @wraps(f)
        def wrapper(self, *args, **kwargs):
            if not request.is_json:
                abort(400)
            try:
                json = request.get_json(force=True)
                return f(self, schema.load(json), *args, **kwargs)
            except ValidationError as e:
                raise ValidationException(
                    "Schema validation failed.", errors=e.messages
                ) from e
                # abort(400, )

        return wrapper

    return real_decorator


def form_schema(schema):
    def real_decorator(f):
        @wraps(f)
        def wrapper(self, *args, **kwargs):
            try:
                return f(self, schema.load(request.form), *args, **kwargs)
            except ValidationError as e:
                raise ValidationException(
                    "Schema validation failed.", errors=e.messages
                ) from e

        return wrapper

    return real_decorator


def query_parameter_schema(schema: type[BaseModel]) -> Callable[..., Any]:
    def real_decorator(f: Callable[..., Any]) -> Callable[..., Any]:
        @wraps(f)
        def wrapper(self: Any, *args: Any, **kwargs: Any):
            try:
                query_parameters = schema.parse_obj(request.args)
                return f(self, *args, query_parameters, **kwargs)
            except pydantic.ValidationError as e:
                raise ValidationException(
                    "Schema validation failed.", errors=e.errors()
                ) from e

        return wrapper

    return real_decorator


class ViaResourceProtector(ResourceProtector):
    def __call__(self, scope: Scopes = None, **kwargs):
        if scope:
            kwargs["scopes"] = scope.name
        return super().__call__(**kwargs)


require_oauth: ViaResourceProtector = ViaResourceProtector()


@login_manager.request_loader
def set_flask_login_user_on_authenticated(request) -> User | None:
    try:
        token = require_oauth.acquire_token()
        if token.user:
            return token.user
        return None
    except authlib.oauth2.OAuth2Error:
        return None


def response(content="html", document_type="text", charset="utf-8", name=None):
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            r = f(*args, **kwargs)

            resp = Response(
                r, content_type=f"{document_type}/{content}; " f"charset={charset}"
            )
            if name is not None:
                resp.headers["Content-Disposition"] = (
                    f"attachment; " f'filename="{name}"'
                )
            return resp

        return wrapper

    return decorator
