from http import HTTPStatus

from flask import Response, abort
from flask.views import MethodView
from marshmallow import ValidationError, fields, validates_schema

from app.api.schema import (
    PageCourseSearchParameters,
    PaginatedResponseSchema,
    RestSchema,
)
from app.decorators import (
    json_schema,
    query_parameter_schema,
    require_oauth,
    require_role,
)
from app.exceptions.base import BusinessRuleException
from app.models.course import Course
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import course_service, datanose_service


class CourseSchema(RestSchema):
    id = fields.Integer(dump_only=True)
    name = fields.String()
    datanose_code = fields.String()

    @validates_schema
    def validate_xor(self, data, **kwargs):
        if data.get("name") and data.get("datanose_code"):
            raise ValidationError(
                {"name": "name and datanose_code cannot both be specified"}
            )
        if not data.get("name") and not data.get("datanose_code"):
            raise ValidationError(
                {"datanose_code": "name or datanose_code is required"}
            )


class CourseResource(MethodView):
    schema_get = CourseSchema()
    schema_put = CourseSchema(only=("name",))

    @require_oauth(Scopes.examination)
    def get(self, course: Course):
        return self.schema_get.dump(course)

    @require_oauth(Scopes.examination)
    @require_role(Roles.EXAMINATION_WRITE)
    @json_schema(schema_put)
    def put(self, data, course: Course):
        course_updated = course_service.update_course(course, name=data.get("name"))
        return self.schema_get.dump(course_updated)

    @require_oauth(Scopes.examination)
    @require_role(Roles.EXAMINATION_WRITE)
    def delete(self, course: Course):
        course_service.delete_course(course)
        return Response(status=HTTPStatus.NO_CONTENT)


class UserCoursesResource(MethodView):
    schema = CourseSchema(many=True)

    @require_oauth()
    def get(self, user):
        if not user.student_id_confirmed:
            raise BusinessRuleException("StudentID unconfirmed")

        courses = datanose_service.get_enrolled_courses(user.student_id)
        return self.schema.dump(courses)


class CourseListResource(MethodView):
    schema_get = PaginatedResponseSchema(CourseSchema(many=True))
    schema_post = CourseSchema()

    @query_parameter_schema(PageCourseSearchParameters)
    def get(self, pagination: PageCourseSearchParameters):
        """Fetch courses based on given parameters."""
        dn_course_ids = pagination.dn_course_ids
        user_educations = pagination.user_educations

        if dn_course_ids or user_educations:
            # Fetch course ids of courses from the selected educations.
            education_course_ids = course_service.get_course_ids_by_education_year(
                user_educations
            )

            # Query all unique selected datanose and education courses.
            course_ids: list[int] = list({*dn_course_ids, *education_course_ids})
            courses = course_service.paginated_get_courses_by_ids(
                course_ids, pagination
            )
        else:
            # Find all courses that contain the whole search term and sort them
            # in alphabetical order.
            # An explicit choice has been made not to use a word_similarity
            # function, since the user knows what course he is looking for. The
            # service uses an ilike({%{search}%}) search within the course
            # names since the user might abbreviate its search criteria.
            courses = course_service.paginated_search_courses(pagination)

        return self.schema_get.dump(courses)

    @require_oauth(Scopes.examination)
    @json_schema(schema_post)
    def post(self, data):
        if data.get("datanose_code"):
            course = course_service.add_course_using_datanose(data.get("datanose_code"))
        else:
            return abort(400)

        return self.schema_post.dump(course)

    @require_oauth(Scopes.examination)
    @require_role(Roles.EXAMINATION_WRITE)
    def patch(self):
        """Gets called when the Datanose courses need synchronization."""
        datanose_service.sync_datanose_courses()
        return Response(status=HTTPStatus.OK)
