from http import HTTPStatus

from flask import Response
from flask.views import MethodView
from marshmallow import fields

from app.api.schema import (
    PageSearchParameters,
    PaginatedResponseSchema,
    RestSchema,
)
from app.decorators import query_parameter_schema, require_oauth, require_role
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import education_service


class EducationSchema(RestSchema):
    id = fields.Integer(dump_only=True)
    nl_name = fields.String()
    en_name = fields.String()
    programme_type = fields.Function(
        dump_only=True,
        serialize=lambda x: x.programme_type.name.lower(),
        # TODO load status
        # deserialize=load_status
    )
    duration = fields.Integer()


class EducationResource(MethodView):
    @require_oauth(Scopes.examination)
    @require_role(Roles.EXAMINATION_WRITE)
    # @json_schema(schema_post_delete)
    def post(self, data, group_id: int):
        raise NotImplementedError()

    @require_oauth(Scopes.examination)
    @require_role(Roles.EXAMINATION_WRITE)
    def delete(self, education_id: int):
        education_service.delete_education(education_id)
        return Response(status=HTTPStatus.NO_CONTENT)


class UserEducationsResource(MethodView):
    schema = EducationSchema(many=True)

    @require_oauth(Scopes.examination)
    def get(self, user):
        """
        Fetch all current educations by users id and then fetch year of
        longest coupled course from the education_course table.
        """
        educations = education_service.get_current_educations_by_user_id(user.id)
        for e in educations:
            e.duration = education_service.get_education_duration(e.id)
        return self.schema.dump(educations)


class EducationListResource(MethodView):
    schema_get = PaginatedResponseSchema(EducationSchema(many=True))

    @query_parameter_schema(PageSearchParameters)
    def get(self, pagination: PageSearchParameters):
        pagination = education_service.paginated_search_educations(
            pagination=pagination
        )
        return self.schema_get.dump(pagination)
