from http import HTTPStatus
from typing import Any

from flask import Response, abort
from flask.views import MethodView
from marshmallow import fields, validate

from app.api.schema import RestSchema
from app.decorators import json_schema, require_oauth, require_role
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import mailinglist_service


class MailingListSchema(RestSchema):
    id = fields.Integer(dump_only=True)
    nl_name = fields.Str(validate=validate.Length(max=64), required=True)
    en_name = fields.Str(validate=validate.Length(max=64), required=True)
    nl_description = fields.Str(required=True)
    en_description = fields.Str(required=True)
    copernica_column_name = fields.Str(validate=validate.Length(max=64), required=True)
    members_only = fields.Bool(required=True)

    default = fields.Bool(required=True)


class MailingListListResource(MethodView):
    schema_get = MailingListSchema(many=True)
    schema_post = MailingListSchema()

    @require_oauth(Scopes.mailinglist)
    @require_role(Roles.MAILINGLIST_ADMIN)
    def get(self):
        mailinglists = mailinglist_service.get_all_mailinglists(
            include_members_only=True
        )
        return self.schema_get.dump(mailinglists)

    @require_oauth(Scopes.mailinglist)
    @require_role(Roles.MAILINGLIST_ADMIN)
    @json_schema(schema=schema_post)
    def post(self, mailinglist: dict[str, Any]):
        created_mailinglist = mailinglist_service.create_mailing(
            nl_name=mailinglist["nl_name"],
            en_name=mailinglist["en_name"],
            nl_description=mailinglist["nl_description"],
            en_description=mailinglist["en_description"],
            copernica_column_name=mailinglist["copernica_column_name"],
            members_only=mailinglist["members_only"],
            default=mailinglist["default"],
        )

        return self.schema_post.dump(created_mailinglist), HTTPStatus.CREATED


class MailingListResource(MethodView):
    schema = MailingListSchema()

    @require_oauth(Scopes.mailinglist)
    @require_role(Roles.MAILINGLIST_ADMIN)
    def get(self, mailinglist_id: int):
        if not mailinglist_id:
            return abort(400)

        mailinglist = mailinglist_service.get_mailinglist(mailinglist_id)
        return self.schema.dump(mailinglist)

    @require_oauth(Scopes.mailinglist)
    @require_role(Roles.MAILINGLIST_ADMIN)
    @json_schema(schema=schema)
    def put(self, mailinglist_update: dict[str, Any], mailinglist_id: int):
        mailinglist_service.update_mailinglist(
            mailinglist_id=mailinglist_id,
            nl_name=mailinglist_update["nl_name"],
            en_name=mailinglist_update["en_name"],
            nl_description=mailinglist_update["nl_description"],
            en_description=mailinglist_update["en_description"],
            copernica_column_name=mailinglist_update["copernica_column_name"],
            members_only=mailinglist_update["members_only"],
            default=mailinglist_update["default"],
        )

        return self.get(mailinglist_id)

    @require_oauth(Scopes.mailinglist)
    @require_role(Roles.MAILINGLIST_ADMIN)
    def delete(self, mailinglist_id: int):
        mailinglist_service.delete_mailinglist(mailinglist_id)

        return Response(status=HTTPStatus.NO_CONTENT)
