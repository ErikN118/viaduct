from http import HTTPStatus
from typing import Any

from flask import Response
from flask.views import MethodView
from marshmallow import fields

from app.api.schema import PageSearchParameters, PaginatedResponseSchema, RestSchema
from app.decorators import (
    json_schema,
    query_parameter_schema,
    require_oauth,
    require_role,
)
from app.models.redirect import Redirect
from app.oauth_scopes import Scopes
from app.repository import model_service
from app.roles import Roles
from app.service import redirect_service


class RedirectSchema(RestSchema):
    id = fields.Integer(dump_only=True)
    fro = fields.Str(required=True)
    to = fields.Str(required=True)


class RedirectResource(MethodView):
    schema = RedirectSchema()

    @require_oauth(Scopes.redirects)
    @require_role(Roles.REDIRECT_WRITE)
    def get(self, redirect: Redirect):
        return self.schema.dump([model_service.get_by_id(Redirect, redirect.id)])

    @require_oauth(Scopes.redirects)
    @require_role(Roles.REDIRECT_WRITE)
    def delete(self, redirect: Redirect):
        model_service.delete(redirect)

        return Response(status=HTTPStatus.NO_CONTENT)


class RedirectListResource(MethodView):
    schema_get = PaginatedResponseSchema(RedirectSchema(many=True))
    schema_post = RedirectSchema()

    @require_oauth(Scopes.redirects)
    @require_role(Roles.REDIRECT_WRITE)
    @query_parameter_schema(PageSearchParameters)
    def get(self, pagination: PageSearchParameters):
        pagination = redirect_service.paginated_search_all_redirects(pagination)
        return self.schema_get.dump(pagination)

    @require_oauth(Scopes.redirects)
    @require_role(Roles.REDIRECT_WRITE)
    @json_schema(schema=schema_post)
    def put(self, data: dict[str, Any]):
        return self.schema_post.dump(
            redirect_service.upsert_redirection(data["fro"], data["to"])
        )
