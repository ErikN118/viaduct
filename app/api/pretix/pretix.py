from flask import abort, request
from flask.views import MethodView
from flask_login import current_user
from marshmallow import fields

from app.api.schema import MultilangStringField, RestSchema
from app.api.user.user import UserSchema
from app.decorators import require_oauth, require_role
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import pretix_service


class PretixEventSchema(RestSchema):
    name = MultilangStringField(required=True)
    slug = fields.Str(required=True)


class PretixUserEvents(MethodView):
    schema = PretixEventSchema(many=True)

    @require_oauth(Scopes.pretix)
    @require_oauth(Scopes.activity)
    @require_role(Roles.ACTIVITY_WRITE)
    def get(self):
        events = pretix_service.get_events_for_user(
            current_user, request.args.to_dict()
        )
        return self.schema.dump(events)


class PretixUserCheck(MethodView):
    schema_get = UserSchema()

    @require_oauth(Scopes.pretix)
    def get(self, fernet_token):
        user = pretix_service.get_user_from_pretix_link(fernet_token)
        if not user:
            abort(404)
        return self.schema_get.dump(user)
