from datetime import datetime

from dateutil.relativedelta import relativedelta
from flask.views import MethodView
from flask_login import current_user
from marshmallow import ValidationError, fields, validate, validates, validates_schema

from app.api.examination.education import EducationSchema
from app.api.schema import (
    PageSearchParameters,
    PaginatedResponseSchema,
    RestSchema,
)
from app.decorators import (
    json_schema,
    query_parameter_schema,
    require_oauth,
    require_role,
)
from app.models.user import User
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import role_service, user_service


class UserSchema(RestSchema):
    id = fields.Integer(dump_only=True)

    # Login details
    email = fields.Email(required=True, validate=validate.Length(max=200))
    disabled = fields.Boolean(load_default=False)

    first_name = fields.String(required=True, validate=validate.Length(max=256))
    last_name = fields.String(required=True, validate=validate.Length(max=256))
    student_id = fields.Str(dump_only=True)
    student_id_confirmed = fields.Boolean(dump_only=True)

    birth_date = fields.Date(required=True)
    phone_nr = fields.String(validate=validate.Length(max=16))
    locale = fields.String()

    educations = fields.Nested(EducationSchema, many=True)
    alumnus = fields.Boolean()

    tfa_enabled = fields.Boolean(dump_default=False)

    address = fields.String(required=True, validate=validate.Length(max=256))
    zip = fields.String(required=True, validate=validate.Length(max=8))
    city = fields.String(required=True, validate=validate.Length(max=256))
    country = fields.String(required=True, validate=validate.Length(max=256))
    iban = fields.String(validate=validate.Length(max=32))

    # Membership
    member = fields.Boolean(attribute="has_paid")
    paid_date = fields.Date(dump_only=True)
    member_of_merit_date = fields.Date(allow_none=True)
    favourer = fields.Boolean()

    @validates("birth_date")
    def validate_age(self, data):
        sixteen_years_ago = datetime.now().date() - relativedelta(years=16)

        if data < sixteen_years_ago:
            raise ValidationError("Users need to be at least 16 years old.")

    @validates_schema
    def validate_member_favourer(self, data, **kwargs):
        if data.get("has_paid") is True and data.get("favourer") is True:
            msg = "Member and favourer are mutually exclusive"
            raise ValidationError({"member": msg, "favourer": msg})

    @classmethod
    def get_list_schema(cls):
        return cls(
            many=True,
            only=(
                "id",
                "email",
                "first_name",
                "last_name",
                "student_id",
                "member",
                "favourer",
            ),
        )

    @classmethod
    def get_patch_schema(cls):
        return cls(
            partial=True,
            only=(
                "email",
                "first_name",
                "last_name",
                "phone_nr",
                "member",
                "member_of_merit_date",
                "favourer",
                "disabled",
                "address",
                "zip",
                "city",
                "iban",
            ),
        )


class UserResource(MethodView):
    schema_get = UserSchema()
    schema_patch = UserSchema.get_patch_schema()

    @require_oauth(Scopes.user)
    def get(self, user: User):
        return self.schema_get.dump(user)

    @require_oauth(Scopes.user)
    @json_schema(schema=schema_patch)
    def patch(self, user_update, user: User):
        if {"has_paid", "member_of_merit_date", "favourer", "disabled"}.intersection(
            set(user_update.keys())
        ):
            role_service.check_user_has_role(current_user, Roles.USER_WRITE)

            member_of_merit = (
                user_update.get("member_of_merit_date"),
                "member_of_merit_date" in user_update,
            )
            user_service.set_admin_properties(
                user,
                member_of_merit=member_of_merit,
                has_paid=user_update.get("has_paid"),
                favourer=user_update.get("favourer"),
                disabled=user_update.get("disabled"),
            )

        user_service.set_user_properties(
            user,
            email=user_update.get("email"),
            first_name=user_update.get("first_name"),
            last_name=user_update.get("last_name"),
            phone_nr=user_update.get("phone_nr"),
            city=user_update.get("city"),
            zip=user_update.get("zip"),
            address=user_update.get("address"),
            iban=user_update.get("iban"),
        )

        return self.get(user)


class UserListResource(MethodView):
    schema_get = PaginatedResponseSchema(UserSchema.get_list_schema())
    schema_post = UserSchema()

    @require_oauth(Scopes.user)
    @require_role(Roles.USER_WRITE)
    @query_parameter_schema(PageSearchParameters)
    def get(self, pagination: PageSearchParameters):
        pagination = user_service.paginated_search_all_users(pagination)

        return self.schema_get.dump(pagination)

    @require_oauth(Scopes.user)
    @require_role(Roles.USER_WRITE)
    @json_schema(schema_post)
    def post(self, new_user: dict):
        raise NotImplementedError()
