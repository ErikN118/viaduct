from flask.views import MethodView
from marshmallow import fields

from app.api.schema import RestSchema
from app.decorators import require_oauth
from app.models.user import User
from app.oauth_scopes import Scopes
from app.service import declaration_service


class UserDeclarationsSchema(RestSchema):
    id = fields.Integer(dump_only=True)
    created = fields.DateTime(required=True)
    modified = fields.DateTime(required=True)
    committee = fields.String(required=True)
    amount = fields.Float(required=True)
    reason = fields.String(required=True)
    user_id = fields.Integer(required=True)


class UserDeclarationsResource(MethodView):
    schema = UserDeclarationsSchema(many=True)

    @require_oauth(Scopes.declaration)
    def get(self, user: User):
        declarations = declaration_service.get_all_declarations_by_user_id(user.id)
        return self.schema.dump(declarations)
