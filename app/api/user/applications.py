from http import HTTPStatus

from flask import Response
from flask.views import MethodView
from marshmallow import fields

from app.api.schema import RestSchema
from app.api.user.user import UserSchema
from app.decorators import require_oauth
from app.models.user import User
from app.oauth_scopes import Scopes
from app.service import oauth_service


class OAuthApplicationSchema(RestSchema):
    client_id = fields.String(dump_only=True)
    client_name = fields.String(dump_only=True)
    user = fields.Nested(
        UserSchema(
            only=("id", "first_name", "last_name"),
            dump_only=("id", "first_name", "last_name"),
        )
    )


class UserApplicationResource(MethodView):
    schema = OAuthApplicationSchema(many=True)

    @require_oauth(Scopes.user)
    def get(self, user: User):
        clients = oauth_service.get_approved_clients_by_user_id(user.id)
        return self.schema.dump(clients)


class UserRevokeApplicationResource(MethodView):
    @require_oauth(Scopes.user)
    def post(self, user: User, client_id: str):
        oauth_service.revoke_user_tokens_by_client_id(user.id, client_id)
        return Response(status=HTTPStatus.NO_CONTENT)
