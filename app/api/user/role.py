from flask.views import MethodView
from marshmallow import fields

from app.api.schema import RestSchema
from app.decorators import require_oauth
from app.roles import Roles
from app.service import role_service


class UseRoleSchema(RestSchema):
    has_role = fields.Boolean()
    needs_tfa = fields.Boolean()


RolesSchema = RestSchema.from_dict(
    {role.name: fields.Nested(UseRoleSchema) for role in Roles}
)


class UserRoleResource(MethodView):
    schema_get = RolesSchema()

    @require_oauth()
    def get(self, user):
        roles = role_service.user_role_summary(user)
        return self.schema_get.dump({role.name: value for role, value in roles.items()})
