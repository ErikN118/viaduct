from flask.views import MethodView
from marshmallow import fields

from app.api.company.company import CompanyModuleSchemaMixin
from app.api.schema import RestSchema
from app.decorators import json_schema, require_oauth, require_role
from app.models.company import Company
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import company_service


class CompanyBannerSchema(RestSchema, CompanyModuleSchemaMixin):
    website = fields.String()


class BannerSchema(RestSchema):
    company_id = fields.Integer()
    logo_id = fields.Integer()
    website = fields.URL()


class BannerResource(MethodView):
    schema = BannerSchema(many=True)

    def get(self):
        banners = company_service.find_all_banners(filter_inactive=True)
        banners = [
            {
                "company_id": banner.company_id,
                "website": banner.website,
                "logo_id": banner.company.logo_file_id,
            }
            for banner in banners
        ]
        return self.schema.dump(banners)


class CompanyBannerResource(MethodView):
    schema = CompanyBannerSchema()

    @require_oauth(Scopes.company)
    @require_role(Roles.COMPANY_WRITE)
    def get(self, company: Company):
        banner = company_service.get_company_banner_by_company_id(company.id)
        return self.schema.dump(banner)

    @require_oauth(Scopes.company)
    @require_role(Roles.COMPANY_WRITE)
    @json_schema(schema)
    def post(self, company_banner: dict, company: Company):
        banner = company_service.create_company_banner(
            company_id=company.id, **company_banner
        )
        return self.schema.dump(banner)

    @require_oauth(Scopes.company)
    @require_role(Roles.COMPANY_WRITE)
    @json_schema(schema)
    def put(self, company_banner: dict, company: Company):
        banner = company_service.edit_company_banner(
            company_id=company.id, **company_banner
        )
        return self.schema.dump(banner)
