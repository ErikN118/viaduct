from collections import defaultdict
from typing import Any

from flask.views import MethodView
from flask_login import current_user
from marshmallow import ValidationError, fields, pre_dump, validate

from app.api.group.group import GroupSchema
from app.api.schema import RestSchema
from app.decorators import json_schema, require_oauth, require_role
from app.models.group import Group
from app.models.user import User
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import group_service, pimpy_service

status_raw = ["new", "started", "done", "remove", "finished", "deleted"]


def dump_status(obj):
    return status_raw[obj.status]


def load_status(value):
    try:
        return status_raw.index(value)
    except ValueError as e:
        raise ValidationError(f"'{value}' not valid for status.") from e


class TaskSchema(RestSchema):
    # List view
    b32_id = fields.String(dump_only=True)
    title = fields.String(required=True)
    group_id = fields.Integer(required=True)
    status = fields.Function(
        serialize=dump_status,
        deserialize=load_status,
        load_default=lambda: load_status("new"),
    )

    # Detail view
    created = fields.Date(dump_only=True)
    modified = fields.Date(dump_only=True)
    users = fields.List(
        fields.String(),
        attribute="users_names",
        validate=validate.Length(min=1),
        required=True,
    )
    content = fields.String(load_default="")
    minute_id = fields.Integer(dump_only=True)
    line = fields.Integer(dump_only=True)

    @pre_dump
    def transform_fields(self, task, **kwargs):
        task.users_names = [user.name for user in task.users]
        return task


class GroupsTasksSchema(RestSchema):
    group = fields.Nested(GroupSchema)
    tasks = fields.Nested(TaskSchema, many=True)


class TaskResource(MethodView):
    schema_get = TaskSchema()
    schema_patch = TaskSchema(partial=True)

    @require_oauth(Scopes.pimpy)
    @require_role(Roles.PIMPY_READ)
    def get(self, task_id):
        task = pimpy_service.get_task_by_b32_id(task_id)

        pimpy_service.check_user_can_access_task(current_user, task)

        return self.schema_get.dump(task)

    @require_oauth(Scopes.pimpy)
    @require_role(Roles.PIMPY_WRITE)
    @json_schema(schema=schema_patch)
    def patch(self, task_update, task_id):
        task = pimpy_service.get_task_by_b32_id(task_id)

        pimpy_service.check_user_can_access_task(current_user, task)

        users = ",".join(task_update.get("users_names", list())) or None

        pimpy_service.edit_task_property(
            task=task,
            content=task_update.get("content"),
            title=task_update.get("title"),
            users_property=users,
        )

        if "status" in task_update:
            pimpy_service.set_task_status(task, task_update["status"])

        return self.get(task_id)


class TaskListResource(MethodView):
    schema_get = GroupsTasksSchema(many=True)
    schema_post = TaskSchema()

    @require_oauth(Scopes.pimpy)
    @require_role(Roles.PIMPY_READ)
    def get(self):
        """Tasks for all users in all groups of the requesting user."""
        tasks_per_group_id = defaultdict(list)
        groups = set()
        tasks = pimpy_service.get_all_tasks_for_users_in_groups_of_user(current_user)
        for task in tasks:
            tasks_per_group_id[task.task.group_id].append(task.task)
            groups.add(task.task.group)
        return self.schema_get.dump(
            [
                {"group": group, "tasks": tasks_per_group_id[group.id]}
                for group in groups
            ]
        )

    @require_oauth(Scopes.pimpy)
    @require_role(Roles.PIMPY_WRITE)
    @json_schema(schema_post)
    def post(self, task: dict[str, Any]):
        group_id = task["group_id"]

        group_service.check_user_member_of_group(current_user, group_id)

        group = group_service.get_by_id(group_id)

        new_task = pimpy_service.add_task_by_user_string(
            title=task.get("title"),
            content=task.get("content"),
            group_id=group.id,
            users_text=",".join(task.get("users_names", [])),
            line=None,
            minute=None,
            status=task.get("status"),
        )
        return self.schema_post.dump(new_task), 201


class GroupTaskListResource(MethodView):
    schema_get = GroupsTasksSchema(many=True)

    @require_oauth(Scopes.pimpy)
    @require_role(Roles.PIMPY_READ)
    def get(self, group: Group):
        """Tasks for all users in group."""
        group_service.check_user_member_of_group(current_user, group.id)

        tasks_per_group_id = defaultdict(list)
        tasks = pimpy_service.get_all_tasks_for_group(group)
        for task in tasks:
            tasks_per_group_id[task.task.group_id].append(task.task)

        return self.schema_get.dump(
            [{"group": group, "tasks": tasks_per_group_id[group.id]}]
        )


class UserTaskListResource(MethodView):
    schema_get = GroupsTasksSchema(many=True)
    schema_post = TaskSchema()

    @require_oauth(Scopes.pimpy)
    @require_role(Roles.PIMPY_READ)
    def get(self, user: User):
        """Tasks for requesting user in all groups of requesting user."""
        tasks_per_group_id = defaultdict(list)
        groups = set()
        tasks = pimpy_service.get_all_tasks_for_user(user)
        for task in tasks:
            tasks_per_group_id[task.task.group_id].append(task.task)
            groups.add(task.task.group)
        return self.schema_get.dump(
            [
                {"group": group, "tasks": tasks_per_group_id[group.id]}
                for group in groups
            ]
        )
