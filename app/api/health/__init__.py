from flask.views import MethodView
from marshmallow import fields

from app import worker
from app.api.schema import RestSchema
from app.service import health_service


class SystemHealthSchema(RestSchema):
    healthy = fields.Boolean()
    worker = fields.Boolean()
    database = fields.Boolean()


class HealthCheckResource(MethodView):
    schema_get = SystemHealthSchema()

    def get(self):
        status = health_service.get_system_health(worker)
        return self.schema_get.dump(status), 200 if status["healthy"] else 503
