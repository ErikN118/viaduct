from http import HTTPStatus
from typing import Any

from flask import Response, request
from flask.views import MethodView
from flask_login import current_user
from marshmallow import fields, validate

from app.api.schema import AutoMultilangStringField, RestSchema
from app.decorators import (
    json_schema,
    require_oauth,
    require_role,
)
from app.models.committee import Committee
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import committee_service, role_service


class CommitteeSchema(RestSchema):
    id = fields.Integer(dump_only=True)
    created = fields.Date(dump_only=True)
    modified = fields.Date(dump_only=True)

    name = AutoMultilangStringField(required=True)
    picture_file_id = fields.Integer(dump_default=None)

    class CoordinatorSchema(RestSchema):
        id = fields.Integer(required=True)
        email = fields.Email(dump_only=True)
        first_name = fields.String(dump_only=True)
        last_name = fields.String(dump_only=True)

    class CommitteeSchema(RestSchema):
        id = fields.Integer(required=True)
        name = fields.String(dump_only=True)

    class PageSchema(RestSchema):
        id = fields.Integer(required=True)
        path = fields.String(dump_only=True)

    class TagSchema(RestSchema):
        id = fields.Integer(required=True)
        name = AutoMultilangStringField(dump_only=True)

    coordinator = fields.Nested(CoordinatorSchema())
    group = fields.Nested(CommitteeSchema())
    page = fields.Nested(PageSchema())
    coordinator_interim = fields.Boolean(dump_default=False)
    open_new_members = fields.Boolean(dump_default=False)
    tags = fields.Nested(TagSchema, many=True)
    pressure = fields.Integer(dump_default=0)
    description = AutoMultilangStringField(
        required=True, validate=validate.Length(max=256)
    )

    @classmethod
    def get_list_schema(cls):
        return cls(many=True)

    @classmethod
    def get_anonymous_list_schema(cls):
        return cls(many=True, exclude=("coordinator",))


class CommitteeTagSchema(RestSchema):
    id = fields.Integer(dump_only=True)
    name = AutoMultilangStringField(dump_only=True)

    @classmethod
    def get_list_schema(cls):
        return cls(many=True)


class CommitteeResource(MethodView):
    schema = CommitteeSchema()

    @require_oauth(Scopes.group)
    @require_role(Roles.GROUP_READ)
    def get(self, committee: Committee):
        return self.schema.dump(committee)

    @require_oauth(Scopes.group)
    @require_role(Roles.GROUP_WRITE)
    @json_schema(schema)
    def put(self, data: dict[str, Any], committee: Committee):
        committee_service.edit_committee(
            committee,
            data["name"],
            data["coordinator"]["id"],
            data["group"]["id"],
            data["page"]["id"],
            data["coordinator_interim"],
            data["open_new_members"],
            data["tags"],
            data["pressure"],
            data["description"],
        )

        return Response(status=HTTPStatus.NO_CONTENT)

    @require_oauth(Scopes.group)
    @require_role(Roles.GROUP_WRITE)
    def delete(self, committee: Committee):
        committee_service.delete_committee(committee)
        return Response(status=HTTPStatus.NO_CONTENT)


class CommitteeListResource(MethodView):
    schema_get = CommitteeSchema.get_list_schema()
    schema_get_anonymous = CommitteeSchema.get_anonymous_list_schema()
    schema_post = CommitteeSchema()

    @require_oauth(optional=True)
    def get(self):
        is_admin = role_service.user_has_role(current_user, Roles.GROUP_WRITE)

        result = committee_service.get_all_committees()
        if is_admin:
            return self.schema_get.dump(result)
        else:
            return self.schema_get_anonymous.dump(result)

    @require_oauth(Scopes.group)
    @require_role(Roles.GROUP_WRITE)
    @json_schema(schema_post)
    def post(self, data: dict):
        committee_created = committee_service.create_committee(
            data["name"],
            data["coordinator"]["id"],
            data["group"]["id"],
            data["page"]["id"],
            data["coordinator_interim"],
            data["open_new_members"],
            data["tags"],
            data["pressure"],
            data["description"],
        )
        return self.schema_post.dump(committee_created)


class CommitteePictureResource(MethodView):
    @require_oauth(Scopes.activity)
    @require_role(Roles.ACTIVITY_WRITE)
    def put(self, committee: Committee):
        if "file" not in request.files:
            return Response(status=HTTPStatus.BAD_REQUEST)

        committee_service.set_committee_picture(committee, request.files["file"])
        return Response(status=HTTPStatus.NO_CONTENT)


class CommitteeTagsResource(MethodView):
    schema_get = CommitteeTagSchema.get_list_schema()

    @require_oauth(Scopes.group)
    @require_role(Roles.GROUP_WRITE)
    def get(self):
        return self.schema_get.dump(committee_service.get_all_tags())
