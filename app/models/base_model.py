"""
Extra functionality that is used by all models.

It extends db.Model with extra functions.
"""
from datetime import datetime, timezone
from typing import Any

from sqlalchemy import Column, DateTime, Integer
from sqlalchemy.orm import declarative_mixin

from app.utils.serialize_sqla import serialize_sqla


@declarative_mixin
class BaseEntity:
    __table_args__: Any = {"sqlite_autoincrement": True}

    # Columns (in order) to be printed when an instance of the object is
    # printed
    prints: tuple[str, ...] = ("id",)

    # Columns to be shown when the to_dict function is used. This should only
    # be changed when certain values should not be shown in the dictionary.
    # Relationships should be added when a relationship is supposed to be in
    # the dictionary as well.
    json_excludes: tuple[str, ...] = tuple()
    jsons = None
    json_relationships = None
    json_relationship_ids: tuple[str, ...] = tuple()

    # Columns that every model needs
    id: int = Column(Integer, primary_key=True)
    created: datetime = Column(
        DateTime(timezone=True),
        default=lambda: datetime.now(timezone.utc),
        nullable=False,
    )
    modified: datetime = Column(
        DateTime(timezone=True),
        default=lambda: datetime.now(timezone.utc),
        onupdate=lambda: datetime.now(timezone.utc),
        nullable=False,
    )

    # Function used by print to print a model at server side.
    # It uses the prints attribute from the object to determine what values to
    # print. This attribute is the id of the object by default.
    def __repr__(self):
        first = True
        string = "<%s(" % type(self).__name__

        for attr in self.prints:
            if not first:
                string += ", "
            string += '"%s"' % (getattr(self, attr))
            first = False

        string += ")>"

        return string

    # Functionality after this point is a bit hard to understand. Just read the
    # function comments and that should be enough.

    # Function to convert a sqlalchemy object instance to a dictionary. This is
    # needed for json serialization of an object. The jsons attribute is used
    # to determine what values to serialize (password hashes and such should
    # not in there)
    def to_dict(self, exclude=True, **kwargs):
        attrs = {}

        if not self.jsons or not exclude:
            if exclude:
                jsons = (
                    column.name
                    for column in self.__table__.columns
                    if column.name not in self.json_excludes
                )
            else:
                jsons = (column.name for column in self.__table__.columns)
        else:
            jsons = self.jsons

        for column in jsons:
            value = serialize_sqla(getattr(self, column), **kwargs)
            attrs[column] = value

        if self.json_relationships:
            for rel in self.json_relationships:
                attrs[rel] = serialize_sqla(getattr(self, rel).all(), **kwargs)

        for rel in self.json_relationship_ids:
            attrs[rel] = tuple(a[0] for a in getattr(self, rel).values("id"))

        return attrs
