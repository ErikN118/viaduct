import sqlalchemy.types as types
from sqlalchemy.dialects.postgresql.base import ischema_names

__version__ = "1.8.0"


class CIText(types.Concatenable, types.UserDefinedType):  # type: ignore[misc]
    """
    Copied from sqlalchemy-citext==1.8.0 fork, since MR for SQLAlchemy is not merged.

    MR: https://github.com/mahmoudimus/sqlalchemy-citext/pull/28
    https://github.com/rouge8/sqlalchemy-citext
    /blob/14e14c5bbd9721e37bd3365186b6827339a723e0/citext/__init__.py
    """

    cache_ok = True

    # This is copied from the `literal_processor` of sqlalchemy's own `String`
    # type.
    def literal_processor(self, dialect):
        def process(value):
            value = value.replace("'", "''")

            if dialect.identifier_preparer._double_percents:
                value = value.replace("%", "%%")

            return "'%s'" % value

        return process

    def get_col_spec(self):
        return "CITEXT"

    def bind_processor(self, dialect):
        def process(value):
            return value

        return process

    def result_processor(self, dialect, coltype):
        def process(value):
            return value

        return process


# Register CIText to SQLAlchemy's Postgres reflection subsystem.
ischema_names["citext"] = CIText
