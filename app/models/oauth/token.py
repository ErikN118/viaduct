import time

from authlib.integrations.sqla_oauth2 import OAuth2TokenMixin
from sqlalchemy import Column, ForeignKey, Integer
from sqlalchemy.orm import declarative_mixin, relationship

from app.extensions import mapper_registry
from app.models.user import User


@mapper_registry.mapped
# TODO https://github.com/sqlalchemy/sqlalchemy/issues/6228
class OAuthToken(declarative_mixin(OAuth2TokenMixin)):  # type: ignore
    __tablename__ = "oauth_token"

    id = Column(Integer, primary_key=True)

    user: User = relationship("User")
    user_id = Column(Integer, ForeignKey("user.id"))

    def is_access_token_expired(self):
        return (self.issued_at + self.expires_in) < time.time()

    def is_refresh_token_expired(self):
        """Refresh tokens expire within a month."""
        return (self.issued_at + 2592000) < time.time()
