from authlib.integrations.sqla_oauth2 import OAuth2AuthorizationCodeMixin
from sqlalchemy import Column, ForeignKey, Integer
from sqlalchemy.orm import relationship

from app.extensions import mapper_registry
from app.models.user import User


@mapper_registry.mapped
class OAuthAuthorizationCode(OAuth2AuthorizationCodeMixin):
    __tablename__ = "oauth_authorization_code"

    id = Column(Integer, primary_key=True)

    user: User = relationship("User")
    user_id = Column(Integer, ForeignKey("user.id", ondelete="CASCADE"))
