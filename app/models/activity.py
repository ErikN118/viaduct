import re
from datetime import datetime, timezone
from functools import cached_property
from typing import TYPE_CHECKING
from urllib.parse import unquote

import pytz
from babel.dates import format_timedelta
from flask import has_request_context
from flask_babel import lazy_gettext as _
from sqlalchemy import Column, DateTime, ForeignKey, Integer, String, Text
from sqlalchemy.orm import backref, relationship

from app import constants, get_locale
from app.extensions import mapper_registry
from app.models.base_model import BaseEntity

if TYPE_CHECKING:
    from app.models.user import User


@mapper_registry.mapped
class Activity(BaseEntity):
    __tablename__ = "activity"
    prints = ("id", "name", "start_time", "end_time")

    owner_id = Column(Integer, ForeignKey("user.id"))
    en_name: str = Column(String(256), nullable=False)
    en_description: str = Column(Text, nullable=False)
    nl_name: str = Column(String(256), nullable=False)
    nl_description: str = Column(Text, nullable=False)
    start_time: datetime = Column(DateTime(timezone=True), nullable=False)
    end_time: datetime = Column(DateTime(timezone=True), nullable=False)
    location: str = Column(String(1024), nullable=False)
    price: str = Column(String(256), nullable=False)
    picture_file_id = Column(Integer, ForeignKey("file.id"))

    google_event_id = Column(String(64))
    owner: "User" = relationship("User", backref=backref("activities", lazy="dynamic"))

    pretix_event_slug = Column(String(64))

    def __str__(self):
        if self.start_time.date() == self.end_time.date():
            end_time_fmt = constants.TIME_FORMAT
        else:
            end_time_fmt = constants.DT_FORMAT

        tz = pytz.timezone("Europe/Amsterdam")
        return "{} ({} - {})".format(
            self.name,
            self.start_time.astimezone(tz).strftime(constants.DT_FORMAT),
            self.end_time.astimezone(tz).strftime(end_time_fmt),
        )

    def is_in_future(self):
        return datetime.now(timezone.utc) < self.start_time

    def is_in_past(self):
        return datetime.now(timezone.utc) >= self.end_time

    def get_timedelta_to_start(self):
        """Locale based description of time left to start."""
        now = datetime.now(timezone.utc)
        return format_timedelta(now - self.start_time, locale=get_locale())

    def get_timedelta_from_end(self):
        """Locale based description of time in the past."""
        now = datetime.now(timezone.utc)
        return format_timedelta(now - self.end_time, locale=get_locale())

    def get_timedelta_to_end(self):
        """Locale based description of time until the end of the activity."""
        now = datetime.now(timezone.utc)
        return format_timedelta(self.end_time - now, locale=get_locale())

    def till_now(self):
        """Locale based description of datetimedelta till now."""
        if self.is_in_future():
            return _("in") + " %s" % (self.get_timedelta_to_start())

        if self.is_in_past():
            return "%s " % (self.get_timedelta_from_end()) + _("ago")

        return _("still") + " %s" % (self.get_timedelta_to_end())

    @staticmethod
    def to_slug(activity: "Activity"):
        slug = activity.name.replace(" ", "-")
        return re.sub("[^a-zA-Z0-9-]", "", slug.lower())

    @cached_property
    def name(self):
        locale = "en"
        if has_request_context():
            locale = get_locale()
        return self.get_localized_name_desc(locale)[0]

    @cached_property
    def description(self):
        locale = "en"
        if has_request_context():
            locale = get_locale()
        return self.get_localized_name_desc(locale)[1]

    @property
    def location_name(self) -> str:
        if self.location.startswith("https://maps.google.com/?q="):
            return unquote(self.location[len("https://maps.google.com/?q=") :])
        elif self.location.startswith("http://"):
            return unquote(self.location[len("http://") :])
        elif self.location.startswith("https://"):
            return unquote(self.location[len("https://") :])
        return self.location

    def get_localized_name_desc(self, locale=None):
        if not locale:
            locale = get_locale()

        nl_available = self.nl_name and self.nl_description
        en_available = self.en_name and self.en_description
        if locale == "nl" and nl_available:
            name = self.nl_name
            description = self.nl_description
        elif locale == "en" and en_available:
            name = self.en_name
            description = self.en_description
        elif nl_available:
            name = self.nl_name + " (Dutch)"
            description = self.nl_description
        elif en_available:
            name = self.en_name + " (Engels)"
            description = self.en_description
        else:
            name = "N/A"
            description = "N/A"

        return name, description
