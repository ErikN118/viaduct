from typing import TYPE_CHECKING, Any

from flask import has_request_context
from flask_babel import gettext as _
from sqlalchemy import (
    Boolean,
    Column,
    DateTime,
    ForeignKey,
    Integer,
    String,
    Text,
)
from sqlalchemy.orm import Mapped, backref, relationship, validates

from app import get_locale
from app.extensions import mapper_registry
from app.models.base_model import BaseEntity

if TYPE_CHECKING:
    from app.models.navigation import NavigationEntry  # noqa
    from app.models.user import User


@mapper_registry.mapped
class Page(BaseEntity):
    __tablename__ = "page"

    # Page paths are saved without the left and right slash.
    path: Mapped[str] = Column(String(200), unique=True)  # TODO: explicit set nullable.
    needs_paid = Column(Boolean, nullable=False)
    deleted = Column(DateTime(timezone=True), nullable=True)
    hide_in_sitemap = Column(Boolean, nullable=False, default=False)

    type = Column(String(256))

    # TODO Remove any: https://github.com/sqlalchemy/sqlalchemy/issues/6229
    navigation_entries: Any = relationship(
        "NavigationEntry", lazy="dynamic", uselist=True, back_populates="page"
    )

    @validates("type")
    def validates_type(self, _, page_type):
        if page_type not in ("page", "committee"):
            raise ValueError("Page type must either be page or committee")
        return page_type


@mapper_registry.mapped
class PageRevision(BaseEntity):
    __tablename__ = "page_revision"

    nl_title = Column(String(128))
    en_title = Column(String(128))

    nl_content = Column(Text)
    en_content = Column(Text)

    comment = Column(String(1024))

    user_id = Column(Integer, ForeignKey("user.id"))
    user: "User" = relationship("User", backref=backref("page_edits", lazy="dynamic"))

    page_id = Column(Integer, ForeignKey("page.id"))
    page: Page = relationship(
        "Page",
        backref=backref("page_revisions", lazy="dynamic", cascade="all,delete"),
    )

    @property
    def nl_available(self) -> bool:
        return bool(self.nl_title and self.nl_content)

    @property
    def en_available(self) -> bool:
        return bool(self.en_title and self.en_content)

    @property
    def title(self):
        locale = "en"
        if has_request_context():
            locale = get_locale()

        if locale == "nl" and self.nl_available:
            return self.nl_title
        elif locale == "en" and self.en_available:
            return self.en_title
        elif self.nl_available:
            return self.nl_title + " (" + _("Dutch") + ")"
        elif self.en_available:
            return self.en_title + " (" + _("English") + ")"
        return "N/A"

    @property
    def content(self):
        locale = "en"
        if has_request_context():
            locale = get_locale()

        if locale == "nl" and self.nl_available:
            return self.nl_content
        elif locale == "en" and self.en_available:
            return self.en_content
        elif self.nl_available:
            return self.nl_content
        elif self.en_available:
            return self.en_content
        return "N/A"

    def get_comparable(self):
        return self.nl_content, self.en_content
