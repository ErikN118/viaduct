from typing import TYPE_CHECKING

from flask import has_request_context, url_for
from sqlalchemy import Boolean, Column, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.schema import ForeignKey

from app import get_locale
from app.extensions import mapper_registry
from app.models.activity import Activity
from app.models.base_model import BaseEntity

if TYPE_CHECKING:
    from app.models.page import Page


@mapper_registry.mapped
class NavigationEntry(BaseEntity):
    __tablename__ = "navigation_entry"
    prints = ("id", "parent_id", "nl_title", "en_title", "url", "external")

    parent_id = Column(Integer, ForeignKey("navigation_entry.id"))
    page_id = Column(Integer, ForeignKey("page.id"))
    nl_title = Column(String(256))
    en_title = Column(String(256))
    url = Column(String(256))
    external = Column(Boolean)
    activity_list = Column(Boolean)
    position: int = Column(Integer, nullable=False)
    order_children_alphabetically = Column(Boolean(), default=False, nullable=False)

    # Both parent and children have explicit raiseload to prevent unwanted
    # explosion of queries. Please see navigation_repo for respective loaders.
    parent: "NavigationEntry" = relationship(
        "NavigationEntry",
        remote_side="NavigationEntry.id",
        back_populates="children",
        lazy="raise",
    )

    children: list["NavigationEntry"] = relationship(
        "NavigationEntry",
        remote_side="NavigationEntry.parent_id",
        order_by="NavigationEntry.position",
        back_populates="parent",
        lazy="raise",
    )

    page: "Page" = relationship(
        "Page", lazy="joined", back_populates="navigation_entries"
    )

    @property
    def title(self) -> str:
        locale = "en"
        if has_request_context():
            locale = get_locale()
        if locale == "nl" and self.nl_title:
            return self.nl_title
        elif locale == "en" and self.en_title:
            return self.en_title
        elif self.nl_title:
            return self.nl_title
        elif self.en_title:
            return self.en_title
        return "N/A"

    @property
    def href(self):
        if self.activity_list:
            return url_for("activity.view")
        elif self.page:
            return f"/{self.page.path}"
        elif self.external and self.url:
            return "https://" + self.url
        else:
            return f"/{self.url}"

    @property
    def activities(self) -> list[Activity]:
        from app.service import activity_service

        if not self.activity_list:
            raise NotImplementedError()
        else:
            return activity_service.get_activities_ending_after_now()
