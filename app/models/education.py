from sqlalchemy import Boolean, Column, Enum, String

from app.enums import ProgrammeType
from app.extensions import mapper_registry
from app.models.base_model import BaseEntity


@mapper_registry.mapped
class Education(BaseEntity):
    __tablename__ = "education"

    nl_name: str = Column(String(128), nullable=False)
    en_name: str = Column(String(128), nullable=False)

    programme_type: ProgrammeType = Column(
        Enum(ProgrammeType, name="programme_type"), nullable=False
    )

    is_via_programme = Column(Boolean, default=False, nullable=False)

    datanose_code = Column(String(64), nullable=True)
