from datetime import date, datetime

from sqlalchemy import Boolean, Column, Date, ForeignKey, Integer, String, Text
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import declarative_mixin, relationship

from app.extensions import mapper_registry
from app.models.base_model import BaseEntity


@mapper_registry.mapped
class Company(BaseEntity):
    __tablename__ = "company"
    name: str = Column(String(200), unique=True, nullable=False)

    # TODO REMOVE DESCRIPTION
    description = Column(Text())
    logo_file_id = Column(Integer, ForeignKey("file.id"), nullable=True)
    website: str = Column(String(256), nullable=False)

    # Human readable string used in URL.
    slug: str = Column(String(200), unique=True, nullable=False)

    # Hard overwrite for the contract period.
    contract_start_date = Column(Date)
    contract_end_date = Column(Date)

    @property
    def active(self):
        return self.contract_start_date <= date.today() <= self.contract_end_date


@declarative_mixin
class CompanyModuleMixin:
    enabled = Column(Boolean(), nullable=False, default=False)
    start_date = Column(Date, nullable=False)
    end_date = Column(Date, nullable=False)

    @declared_attr
    def company_id(self) -> "Column[Integer]":  # pylint: disable=no-self-use
        return Column(Integer, ForeignKey("company.id"))

    @declared_attr
    def company(self) -> "relationship[Company]":  # pylint: disable=no-self-use
        return relationship("Company")

    @property
    def active(self):
        if not self.company.active:
            return False

        if not self.enabled:
            return False

        return self.start_date <= datetime.date(datetime.utcnow()) <= self.end_date
