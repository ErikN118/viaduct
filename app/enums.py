from enum import Enum, IntEnum


class FileCategory(Enum):
    UPLOADS = 1
    EXAMINATION = 2
    ACTIVITY_PICTURE = 3
    ALV_DOCUMENT = 4
    COMPANY_LOGO = 5
    USER_AVATAR = 6
    UPLOADS_MEMBER = 7
    COMMITTEE_PICTURE = 8
    DOMJUDGE_BANNER = 9

    @classmethod
    def all_uploads(cls):
        return [cls.UPLOADS, cls.UPLOADS_MEMBER]


class PimpyTaskStatus(Enum):
    NOT_STARTED = 0
    STARTED = 1
    DONE = 2
    NOT_DONE = 3
    CHECKED = 4
    DELETED = 5
    MAX = 5


class ProgrammeType(IntEnum):
    """
    IDs of the enums are in order of the education level.
    This is used internally to select highest educations from a lists.
    Other includes the types: None and Exchange.
    All FNWI types can be found at:
    http://api.datanose.nl/2020/Faculty/FNWI/Programmes/.
    """

    OTHER = 0
    BACHELOR = 1
    MAJOR = 2
    MINOR = 3
    PRE_MASTER = 4
    MASTER = 5

    @property
    def abbreviation(self) -> str:
        abbreviations = {
            ProgrammeType.OTHER: "Other",
            ProgrammeType.BACHELOR: "BSc",
            ProgrammeType.MAJOR: "Maj",
            ProgrammeType.MINOR: "Min",
            ProgrammeType.PRE_MASTER: "PreMSc",
            ProgrammeType.MASTER: "MSc",
        }

        return abbreviations[self]
