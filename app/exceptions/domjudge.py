from app.exceptions.base import BusinessRuleException, ResourceNotFoundException


class ProblemTextNotFoundException(ResourceNotFoundException):
    pass


class ContestNotActiveException(BusinessRuleException):
    pass
