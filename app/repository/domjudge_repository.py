from app import db
from app.models.domjudge import DOMjudgeContestSettings


def find_all_contest_settings() -> list[DOMjudgeContestSettings]:
    return db.session.query(DOMjudgeContestSettings).all()


def find_contest_settings_by_contest_id(
    contest_id: int,
) -> DOMjudgeContestSettings | None:
    return (
        db.session.query(DOMjudgeContestSettings)
        .filter(DOMjudgeContestSettings.contest_id == contest_id)
        .one_or_none()
    )


def create_contest_settings_for_contest(contest_id: int) -> DOMjudgeContestSettings:
    settings = DOMjudgeContestSettings()
    settings.contest_id = contest_id

    return settings


def save_contest_settings(contest_settings: DOMjudgeContestSettings):
    db.session.add(contest_settings)
    db.session.commit()
