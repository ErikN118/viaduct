from sqlalchemy import select

from app import db
from app.models.declaration import Declaration


def save(declaration: Declaration):
    db.session.add(declaration)
    db.session.commit()


def find_all_declarations_by_user_id(user_id: int) -> list[Declaration]:
    stmt = (
        select(Declaration)
        .filter(Declaration.user_id == user_id)
        .order_by(Declaration.created.desc())
    )
    return db.session.execute(stmt).scalars().all()
