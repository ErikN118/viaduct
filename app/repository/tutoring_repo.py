from sqlalchemy import func

from app import db
from app.api.schema import PageSearchParameters
from app.models.course import Course
from app.models.tutoring import Tutor, Tutoring
from app.models.user import User
from app.repository.utils.pagination import search_columns


def create_tutoring():
    return Tutoring()


def save(t: Tutoring | Tutor):
    db.session.add(t)
    db.session.commit()
    return t


def get_tutorings_for_tutee(user: User):
    return (
        db.session.query(Tutoring)
        .filter(Tutoring.tutee == user, Tutoring.deleted.is_(None))
        .all()
    )


def get_tutorings_for_tutor(user: User):
    return (
        db.session.query(Tutoring)
        .filter(Tutoring.tutor == user, Tutoring.deleted.is_(None))
        .all()
    )


def find_tutors(pagination: PageSearchParameters):
    q = (
        db.session.query(Tutor)
        .join(User, Tutor.user_id == User.id)
        .join(Course, Tutor.course_id == Course.id)
        .order_by(Tutor.approved.desc())
    )

    q = search_columns(
        q,
        pagination.search,
        func.concat(User.first_name, " ", User.last_name),
        Course.name,
    )

    return q.paginate(pagination.page, pagination.limit, False)


def find_tutors_for_course(course: Course) -> list[Tutor]:
    return (
        db.session.query(Tutor)
        .filter(Tutor.course == course, Tutor.approved.isnot(None))
        .all()
    )


def find_courses_for_tutor(tutor: User) -> list[Tutor]:
    return db.session.query(Tutor).filter(Tutor.user == tutor).all()


def register_tutor(user: User, course: Course, grade: int) -> Tutor:
    tutor = Tutor()
    tutor.user = user
    tutor.course = course
    tutor.grade = grade
    tutor.approved = None
    db.session.add(tutor)
    db.session.commit()
    return tutor


def find_tutor(user: User, course: Course) -> Tutor | None:
    return (
        db.session.query(Tutor)
        .filter(Tutor.user == user, Tutor.course == course)
        .one_or_none()
    )


def get_tutorings(is_open: bool = None, tutor: User = None):
    q = (
        db.session.query(Tutoring)
        .select_from(Tutoring)
        .filter(Tutoring.deleted.is_(None))
        .order_by(Tutoring.created.desc())
        .order_by(Tutoring.tutor_id.desc())
    )
    if is_open is not None:
        if is_open:
            q = q.filter(Tutoring.tutor_id.is_(None))
        else:
            q = q.filter(Tutoring.tutor_id.isnot(None))
    if tutor is not None:
        q = q.join(Tutor, Tutoring.course_id == Tutor.course_id).filter(
            Tutor.user == tutor, Tutor.approved.isnot(None)
        )
    return q.all()
