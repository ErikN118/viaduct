from typing import Any

from app import db
from app.models.mailinglist_model import MailingList
from app.models.user import User


def create_mailinglist() -> MailingList:
    return MailingList()


def find_all_mailinglists(include_members_only: bool = True) -> list[MailingList]:
    q = db.session.query(MailingList)
    if not include_members_only:
        q = q.filter_by(members_only=False)

    return q.order_by(MailingList.created).all()


def find_default_mailinglists() -> list[MailingList]:
    return (
        db.session.query(MailingList)
        .filter_by(default=True)
        .order_by(MailingList.created)
        .all()
    )


def find_mailinglist_by_nl_name(
    name: str, current_mailinglist: MailingList = None
) -> MailingList | None:
    return _find_mailinglist_by_filter({"nl_name": name}, current_mailinglist)


def find_mailinglist_by_en_name(
    name: str, current_mailinglist: MailingList = None
) -> MailingList | None:
    return _find_mailinglist_by_filter({"en_name": name}, current_mailinglist)


def find_mailinglist_by_copernica_column_name(
    column_name: str, current_mailinglist: MailingList = None
) -> MailingList | None:
    return _find_mailinglist_by_filter(
        {"copernica_column_name": column_name}, current_mailinglist
    )


def _find_mailinglist_by_filter(
    filter: dict[str, Any], current_mailinglist: MailingList = None
) -> MailingList | None:
    q = db.session.query(MailingList).filter_by(**filter)
    if current_mailinglist is not None:
        q = q.filter(MailingList.id != current_mailinglist.id)

    return q.one_or_none()


def find_mailinglist_by_id(mailinglist_id: int) -> MailingList:
    return db.session.query(MailingList).filter_by(id=mailinglist_id).one_or_none()


def find_user_subscribed_mailinglists(user: User) -> list[MailingList]:
    return user.mailinglists.all()


def find_subscribed_users_of_mailinglist(mailinglist: MailingList) -> list[User]:
    return mailinglist.users.all()


def save_mailinglist(mailinglist: MailingList):
    db.session.add(mailinglist)
    db.session.commit()


def delete_mailinglist(mailinglist: MailingList):
    db.session.delete(mailinglist)
    db.session.commit()
