import datetime
import json

import flickrapi
from flickrapi.auth import FlickrAccessToken
from pydantic import SecretStr

from app import db
from app.exceptions.photo import FlickrAPIException
from app.service.setting_service import DatabaseSettingsMixin


class FlickrSettings(DatabaseSettingsMixin):
    flickr_user: str = "dummy"
    flickr_api_key = SecretStr("dummy")
    flickr_secret = SecretStr("dummy")
    flickr_token = SecretStr("dummy")
    flickr_token_secret = SecretStr("dummy")


def init_api(settings: FlickrSettings):
    access_token = FlickrAccessToken(
        settings.flickr_token.get_secret_value(),
        settings.flickr_token_secret.get_secret_value(),
        "read",
    )
    return flickrapi.FlickrAPI(
        settings.flickr_api_key.get_secret_value(),
        settings.flickr_secret.get_secret_value(),
        token=access_token,
        format="json",
    )


def image_url(farm, server, id, secret, format="jpg", size=""):
    if size != "":
        size = "_" + size
    return "https://farm{}.staticflickr.com/{}/{}_{}{}.{}".format(
        farm, server, id, secret, size, format
    )


def short_url(photo_id):
    return "https://flic.kr/p/" + base58(photo_id)


def base58(num):
    alphabet = "123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ"
    bc = len(alphabet)
    enc = ""
    while num >= bc:
        div, mod = divmod(num, bc)
        enc = alphabet[mod] + enc
        num = int(div)
    enc = alphabet[num] + enc
    return enc


def to_dict(a):
    return json.loads(a.decode("utf-8"))


def find_album_index():
    settings = FlickrSettings(db_session=db.session)
    flickr = init_api(settings)
    response = flickr.photosets.getList(user_id=settings.flickr_user)
    photosets = to_dict(response)

    if photosets["stat"] != "ok":
        raise FlickrAPIException(
            endpoint="photosets.getList", error_code=photosets["code"]
        )

    albums = list()
    for photoset in photosets["photosets"]["photoset"]:
        epoch = int(photoset["date_create"])
        url = image_url(
            photoset["farm"],
            photoset["server"],
            photoset["primary"],
            photoset["secret"],
        )
        album = {
            "id": photoset["id"],
            "photos": int(photoset["photos"]),
            "date_created": datetime.datetime.fromtimestamp(epoch),
            "title": photoset["title"]["_content"],
            "description": photoset["description"]["_content"],
            "primary_photo": url,
        }
        albums.append(album)

    return albums


def get_highest_res_url(photo):
    sizes = ["url_k", "url_h", "url_b", "url_c", "url_o"]

    for size in sizes:
        if size in photo:
            return photo[size]
    return None


def find_album(album_id):
    # Get list of photos
    settings = FlickrSettings(db_session=db.session)
    flickr = init_api(settings)
    response = flickr.photosets.getPhotos(
        user_id=settings.flickr_user, photoset_id=album_id, extras="url_k,url_o"
    )
    photos = to_dict(response)

    if photos["stat"] != "ok":
        if photos["code"] == 1:
            return None
        else:
            raise FlickrAPIException("photosets.getPhotos", photos["code"])

    photolist = list()
    for photo in photos["photoset"]["photo"]:
        url = image_url(photo["farm"], photo["server"], photo["id"], photo["secret"])

        original_url = photo["url_o"]
        item = {
            "id": photo["id"],
            "title": photo["title"],
            "url": url,
            "highres_url": get_highest_res_url(photo),
            "origin_url": original_url,
            "shorturl": short_url(int(photo["id"])),
        }
        photolist.append(item)

    # Get info about photo set
    response = flickr.photosets.getInfo(
        user_id=settings.flickr_user, photoset_id=album_id
    )
    info = to_dict(response)

    if info["stat"] != "ok":
        raise FlickrAPIException("photosets.getInfo", info["code"])

    info = info["photoset"]
    epoch = int(info["date_create"])
    result = {
        "photos": photolist,
        "title": info["title"]["_content"],
        "description": info["description"]["_content"],
        "primary_photo": image_url(
            info["farm"], info["server"], info["primary"], info["secret"], size="b"
        ),
        "date_created": datetime.datetime.fromtimestamp(epoch),
        "photo_count": int(info["photos"]),
    }
    return result
