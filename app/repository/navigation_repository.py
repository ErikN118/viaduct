import logging

from sqlalchemy.orm import joinedload, raiseload

from app import db
from app.models.navigation import NavigationEntry
from app.models.page import Page

_logger = logging.getLogger(__name__)


def find_entry_by_path(path: str) -> NavigationEntry | None:
    return (
        db.session.query(NavigationEntry)
        .options(
            joinedload(NavigationEntry.children).raiseload(NavigationEntry.children)
        )
        .filter_by(url=path)
        .first()
    )


def find_entry_by_page(page: Page) -> NavigationEntry | None:
    return page.navigation_entries.options(joinedload(NavigationEntry.children)).first()


def get_parent(entry: NavigationEntry, children_depth: int) -> NavigationEntry:
    q = db.session.query(NavigationEntry).filter(NavigationEntry.id == entry.parent_id)

    options = raiseload(NavigationEntry.children)
    for i in range(children_depth):
        if i == 0:
            options = joinedload(NavigationEntry.children)
        else:
            options.joinedload(NavigationEntry.children)

    return q.options(options).one_or_none()


def get_root_entries(children_depth: int) -> list[NavigationEntry]:
    q = (
        db.session.query(NavigationEntry)
        .filter_by(parent_id=None)
        .order_by(NavigationEntry.position)
    )

    options = raiseload(NavigationEntry.children)
    for i in range(children_depth):
        if i == 0:
            options = joinedload(NavigationEntry.children)
        else:
            options.joinedload(NavigationEntry.children)

    return q.options(options).all()


def find_entry_by_id(entry_id: int, children_depth=0) -> NavigationEntry | None:
    q = db.session.query(NavigationEntry).filter(NavigationEntry.id == entry_id)

    options = raiseload(NavigationEntry.children)
    for i in range(children_depth):
        if i == 0:
            options = joinedload(NavigationEntry.children)
        else:
            options.joinedload(NavigationEntry.children)
    return q.options(options).one_or_none()


def save(entry):
    db.session.add(entry)
    db.session.commit()
