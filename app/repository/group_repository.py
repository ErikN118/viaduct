from datetime import datetime

from flask_sqlalchemy import Pagination
from sqlalchemy import delete, func, select
from sqlalchemy.orm import Session, joinedload

from app import db
from app.api.schema import PageSearchParameters
from app.models.group import Group, UserGroup, UserGroupHistory
from app.models.user import User
from app.repository.utils.pagination import search_columns


def save(group: Group):
    db.session.add(group)
    db.session.commit()


def find_by_id(group_id: int) -> Group:
    return db.session.scalar(
        select(Group).filter(
            Group.id == group_id,
            Group.deleted_at.is_(None),
        )
    )


def get_group_by_name(group_name: str) -> Group | None:
    return db.session.scalar(
        select(Group).filter(
            Group.name == group_name,
            Group.deleted_at.is_(None),
        )
    )


def get_group_by_maillist(maillist: str) -> Group | None:
    return db.session.scalar(
        select(Group).filter(
            Group.maillist == maillist,
            Group.deleted_at.is_(None),
        )
    )


def paginated_search_all_groups(pagination: PageSearchParameters) -> Pagination:
    q = (
        db.session.query(Group)
        .filter(Group.deleted_at.is_(None))
        .order_by(func.lower(Group.name).asc())
    )

    q = search_columns(q, pagination.search, Group.name, Group.maillist)

    return q.paginate(pagination.page, pagination.limit, False)


def get_groups_for_user(user: User) -> list[UserGroup]:
    stmt = (
        select(UserGroup)
        .join(Group)
        .filter(
            UserGroup.user_id == user.id,
            Group.deleted_at.is_(None),
        )
        .order_by(Group.name)
        .options(joinedload(UserGroup.group))
    )
    return db.session.scalars(stmt).all()


def paginated_search_group_users(
    group_id: int, pagination: PageSearchParameters
) -> Pagination:
    q = (
        db.session.query(User)
        .join(UserGroup, UserGroup.user_id == User.id)
        .join(Group)
        .order_by(User.id.asc())
        .filter(
            UserGroup.group_id == group_id,
            Group.deleted_at.is_(None),
        )
    )

    q = search_columns(
        q,
        pagination.search,
        User.email,
        User.first_name,
        User.last_name,
        User.student_id,
    )

    return q.paginate(pagination.page, pagination.limit, False)


def get_group_users(group_id: int) -> list[User]:
    return db.session.scalars(
        select(User)
        .join(UserGroup, UserGroup.user_id == User.id)
        .join(Group)
        .filter(
            UserGroup.group_id == group_id,
            Group.deleted_at.is_(None),
        )
    ).all()


def remove_group_users(group: Group, user_ids: list[int]) -> None:
    for user_id in user_ids:
        user_group = (
            db.session.query(UserGroup)
            .filter(UserGroup.group_id == group.id, UserGroup.user_id == user_id)
            .one_or_none()
        )
        history = UserGroupHistory.from_user_group(user_group)
        db.session.add(history)
        db.session.delete(user_group)
    db.session.commit()


def add_group_users(group: Group, users: list[User]) -> None:
    group.users.extend(users)
    db.session.add(group)
    db.session.commit()


def create_group(name: str, mailtype: str, maillist: str | None):
    g = Group(name=name, maillist=maillist, mailtype=mailtype)
    db.session.add(g)
    db.session.commit()
    return g


def delete_group(group: Group) -> None:
    group.deleted_at = datetime.now()
    db.session.add(group)
    db.session.commit()


def remove_user_history(db_session: Session, user: User) -> None:
    stmt1 = delete(UserGroup).where(UserGroup.user_id == user.id)
    stmt2 = delete(UserGroupHistory).where(UserGroupHistory.user_id == user.id)
    db_session.execute(stmt1)
    db_session.execute(stmt2)


def get_groups_history_for_user(
    db_session: Session, user: User
) -> list[UserGroupHistory]:
    stmt = (
        select(UserGroupHistory)
        .filter(UserGroupHistory.user_id == user.id)
        .options(joinedload(UserGroupHistory.group))
    )
    return db_session.scalars(stmt).all()
