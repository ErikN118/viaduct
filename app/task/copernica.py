import logging

from celery import states

from app import app, worker
from app.exceptions.base import BusinessRuleException, TaskFailure
from app.repository import user_repository
from app.service import (
    copernica_service,
    mailinglist_service,
    user_mailing_service,
    user_service,
)

_logger = logging.getLogger(__name__)


@worker.task(bind=True)
def update_all_users(self) -> None:
    with app.app_context():
        user_ids = user_service.find_all_user_ids()
        self.update_state(
            state=states.STARTED, meta={"count": len(user_ids), "current": 0}
        )
        for idx, user_id in enumerate(user_ids):
            _logger.debug(
                "[%d/%d] Updating user %d's copernica details",
                idx,
                len(user_ids),
                user_id,
            )
            self.update_state(
                state=states.STARTED, meta={"count": len(user_ids), "current": idx}
            )
            user = user_service.get_user_by_id(user_id)
            copernica_service.update_profile(user)


@worker.task
def create_user(user_id: int) -> None:
    with app.app_context():
        user = user_service.get_user_by_id(user_id)
        copernica_service.create_profile(user)


@worker.task
def update_user(user_id: int) -> None:
    with app.app_context():
        user = user_service.get_user_by_id(user_id)
        copernica_service.update_profile(user)


@worker.task
def synchronize_copernica_disabling_selection_profiles() -> None:
    with app.app_context():
        selections = copernica_service.get_disabling_selections()

        for selection, selection_id in selections.items():
            _logger.info("Disabling all mailinglists for profiles in %s", selection)
            profile_ids = copernica_service.get_selection_profiles(selection_id)

            users = user_repository.find_active_mailing_users_with_copernica_id(
                profile_ids
            )

            for user in users:
                _logger.debug("Disabling all mailinglists for %s", user)
                user_mailing_service.set_mailing_list_subscriptions(
                    user, [], update_copernica=True
                )


@worker.task
def send_welcome_mail(user_id: int) -> None:
    logging.debug("loading app context, starting database session")
    with app.app_context():
        logging.debug("retrieving user from database")
        user = user_service.get_user_by_id(user_id)
        copernica_service.send_welcome_mail(user)


@worker.task
def validate_database_structure():
    """Validate that all configured database columns actually exist in Copernica."""
    with app.app_context():
        lists = mailinglist_service.get_all_mailinglists()
        database_fields = copernica_service.get_database_fields()
        any_error: BusinessRuleException | None = None
        for mail_list in lists:
            try:
                _logger.debug(
                    "Validating Copernica column '%s'", mail_list.copernica_column_name
                )
                copernica_service.validate_column(
                    mail_list.copernica_column_name, database_fields
                )
                _logger.info(
                    "Copernica column '%s' validated correctly",
                    mail_list.copernica_column_name,
                )
            except BusinessRuleException as e:
                any_error = e
                _logger.exception(
                    "Copernica column '%s' did not validate",
                    mail_list.copernica_column_name,
                )

        if any_error is not None:
            raise TaskFailure("Copernica database out-of-sync") from any_error
