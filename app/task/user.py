import logging
from datetime import timedelta

from app import app, db, worker
from app.repository import user_repository
from app.service import datanose_service, education_service
from app.service.setting_service import DatabaseSettingsMixin

_logger = logging.getLogger(__name__)


class UserEducationSetting(DatabaseSettingsMixin):
    user_education_updates_enabled: bool = True


@worker.task()
def update_most_historic_user_educations():
    with app.app_context():
        if not UserEducationSetting(
            db_session=db.session
        ).user_education_updates_enabled:
            return

        users = user_repository.get_users_with_educations_not_updated_for(
            db.session, num=25, interval=timedelta(days=30)
        )

        for user in users:
            _logger.info("Updating user educations [user_id=%d]", user.id)
            dn_programmes = datanose_service.get_study_programmes(user.student_id)
            educations = education_service.get_educations_by_datanose_codes(
                [p.code for p in dn_programmes]
            )

            user_repository.set_user_education(db.session, user, educations)

            db.session.commit()
