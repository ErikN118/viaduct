from flask import (
    Blueprint,
    abort,
    flash,
    redirect,
    render_template,
    url_for,
)
from flask_babel import gettext as _
from flask_login import current_user

from app import db
from app.decorators import require_membership, require_role
from app.exceptions.base import BusinessRuleException
from app.models.alv_model import Alv
from app.roles import Roles
from app.service import alv_service
from app.views.file import send_file_inline

blueprint = Blueprint("alv", __name__, url_prefix="/alv")


@blueprint.route("/", methods=["GET"])
@require_membership
def root():
    return render_template("vue_content.htm", title=_("ALV"))


@blueprint.route("/<alv:alv>/", methods=["GET"])
@require_membership
def view(alv):
    if current_user.locale == "nl":
        title = alv.nl_name
    else:
        title = alv.en_name

    return render_template("vue_content.htm", title=title)


@blueprint.route("/latest/", methods=["GET"])
@require_membership
def view_latest():
    alv = alv_service.get_latest_alv(db.session)
    return redirect(url_for("alv.view", alv=alv))


@blueprint.route("/documents/<int:alv_document_id>/<int:version>/", methods=["GET"])
@blueprint.route(
    "/<alv:_alv>/documents/<int:alv_document_id>/<int:version>/", methods=["GET"]
)
@require_membership
def view_document_version(alv_document_id, version, _alv=None):
    if version <= 0:
        return abort(404)

    alv_document = alv_service.get_alv_document_by_id(
        alv_document_id, include_versions=True
    )

    version_idx = version - 1
    if version_idx >= len(alv_document.versions):
        return abort(404)

    alv_document_version = alv_document.versions[version_idx]
    _file = alv_service.get_alv_document_version_file(alv_document_version)

    fn = alv_service.get_alv_document_version_filename(alv_document, version, _file)

    return send_file_inline(_file, filename=fn)


@blueprint.route("/documents/<int:alv_document_id>/", methods=["GET"])
@blueprint.route("/<alv:_alv>/documents/<int:alv_document_id>/", methods=["GET"])
@require_membership
def view_document_version_latest(alv_document_id, _alv=None):
    alv_document = alv_service.get_alv_document_by_id(
        alv_document_id, include_versions=True
    )

    version_idx = len(alv_document.versions) - 1

    alv_document_version = alv_document.versions[version_idx]
    _file = alv_service.get_alv_document_version_file(alv_document_version)

    fn = alv_service.get_alv_document_version_filename(alv_document, version_idx, _file)

    return send_file_inline(_file, filename=fn)


@blueprint.route("/<alv:alv>/document/minutes/")
@require_membership
def view_minutes(alv: Alv):
    _file = alv_service.get_alv_minutes_file(alv)
    fn = alv_service.get_alv_minutes_filename(alv, _file)

    return send_file_inline(_file, fn)


@blueprint.route("/create/", methods=["GET", "POST"])
@blueprint.route("/<alv:alv>/edit/<string:__>/", methods=["GET", "POST"])
@blueprint.route("/<alv:alv>/edit/", methods=["GET", "POST"])
@require_role(Roles.ALV_WRITE)
def create_edit(alv=None, __=None):
    if alv:
        title = _("Edit") + " ALV"
    else:
        title = _("Create") + " ALV"
    return render_template("vue_content.htm", title=title)


@blueprint.route("/<alv:alv>/remove/", methods=["POST"])
@require_role(Roles.ALV_WRITE)
def delete(alv: Alv):
    try:
        alv_service.delete_alv(alv)
    except BusinessRuleException:
        flash(_("Cannot delete %s, as it has documents attached.") % alv.name, "danger")
        return redirect(url_for("alv.view", alv=alv))

    return redirect(url_for("alv.root"))
