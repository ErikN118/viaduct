from authlib.oauth2.rfc6749 import OAuth2Error
from flask import Blueprint, flash, redirect, render_template, request, url_for
from flask_babel import _
from flask_login import current_user, login_required

from app.decorators import response_headers
from app.extensions import oauth_server
from app.service import oauth_service

blueprint = Blueprint("oauth", __name__, url_prefix="/oauth")


@blueprint.route("/authorize", methods=["GET", "POST"])
@login_required
@response_headers({"X-Frame-Options": "SAMEORIGIN"})
def authorize():
    if request.method == "GET":
        try:
            grant = oauth_server.get_consent_grant(end_user=current_user)
        except OAuth2Error as error:
            flash(
                _("OAuth authorize request was invalid. ")
                + error.get_error_description(),
                "danger",
            )
            return redirect(url_for("home.home"))

        if grant.client.auto_approve:
            return oauth_server.create_authorization_response(grant_user=current_user)

        if oauth_service.user_has_approved_client(
            user_id=current_user.id, client=grant.client
        ):
            return oauth_server.create_authorization_response(grant_user=current_user)

        kwargs = {
            "grant": grant,
            "user": current_user,
            "state": request.args.get("state"),
            "scopes": oauth_service.get_scope_descriptions(),
        }
        return render_template("oauth/oauthorize.html", **kwargs)

    confirm = request.form.get("confirm", False)
    if confirm:
        # granted by resource owner
        return oauth_server.create_authorization_response(grant_user=current_user)
    # denied by resource owner
    return oauth_server.create_authorization_response(grant_user=None)


@blueprint.route("/token", methods=["POST"])
def issue_token():
    return oauth_server.create_token_response()


@blueprint.route("/revoke", methods=["POST"])
def revoke_token():
    return oauth_server.create_endpoint_response(
        oauth_service.RevocationEndpoint.ENDPOINT_NAME
    )


@blueprint.route("/introspect", methods=["POST"])
def introspect_token():
    return oauth_server.create_endpoint_response(
        oauth_service.IntrospectionEndpoint.ENDPOINT_NAME
    )


@blueprint.route("/errors", methods=["GET"])
def errors():
    error = request.args.get("error", _("Unknown OAuth error"))
    error_description = request.args.get("error_description", _("Please try again"))
    return render_template(
        "oauth/error.htm", error=error, description=error_description
    )
