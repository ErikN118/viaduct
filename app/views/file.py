"""Views for the file module."""
import logging

from flask import (
    Blueprint,
    Response,
    abort,
    flash,
    redirect,
    render_template,
    request,
    send_file,
    url_for,
)
from flask_login import current_user

from app import app, hashfs
from app.decorators import require_role
from app.enums import FileCategory
from app.forms.file import FileForm
from app.models.file import File
from app.roles import Roles
from app.service import file_service, role_service
from app.task import file

blueprint = Blueprint("file", __name__, url_prefix="/files")
_logger = logging.getLogger(__name__)


def send_picture(
    file_id: int | None, filename: str, picture_type: str, thumbnail_size=None
):
    if picture_type not in ("normal", "thumbnail"):
        picture_type = "normal"

    picture_file = file_service.get_file_by_id(file_id)

    filename = f"{filename}.{picture_file.extension}"
    if picture_type == "normal":
        return send_file_inline(picture_file, filename=filename)
    else:
        thumbnail = file_service.get_thumbnail_of_file(picture_file, thumbnail_size)
        r = send_file(thumbnail, download_name=filename)
        r.headers["Content-Disposition"] = f'inline; filename="{filename}"'
        return r


def send_file_inline(_file: File, filename=None):
    address = hashfs.get(_file.hash)
    if not address:
        return abort(404)

    # Pass the attachment_filename argument, as it is used for mimetype.
    r: Response = send_file(address.abspath, download_name=filename)
    if filename:
        r.headers["Content-Disposition"] = f'inline; filename="{filename}"'

    return r


@blueprint.route("/", methods=["GET"])
@require_role(Roles.FILE_READ)
def list():
    """List all files that are not assigned to a page."""

    search = request.args.get("search", None)
    try:
        page_nr = int(request.args.get("page", 1))
    except (TypeError, ValueError):
        page_nr = 1
    per_page = 30

    if search:
        files = file_service.search_files_in_uploads(search)[:per_page]
        files_paginated = None
    else:
        files_paginated = file_service.get_all_uploads(page_nr, per_page)
        files = files_paginated.items

    form = FileForm()

    can_write = role_service.user_has_role(current_user, Roles.FILE_WRITE)

    return render_template(
        "files/list.htm",
        files=files,
        form=form,
        files_paginated=files_paginated,
        search=search,
        can_write=can_write,
    )


@blueprint.route("/", methods=["POST"])
@require_role(Roles.FILE_WRITE)
def upload():
    """Upload a file."""
    try:
        page_nr = int(request.args.get("page", 1))
    except (TypeError, ValueError):
        page_nr = 1
    form = FileForm()
    new_files = []

    if form.validate_on_submit():
        category = FileCategory.UPLOADS
        if form.members_only.data:
            category = FileCategory.UPLOADS_MEMBER
        for new_file in request.files.getlist("file"):
            # File upload request, but no added files
            if new_file.filename == "":
                flash("No files selected", "danger")
                return redirect(url_for(".list", page=page_nr))

            f = file_service.add_file(category, new_file)
            new_files.append(f)

    files_paginated = file_service.get_all_uploads(page_nr, 30)
    files = files_paginated.items

    can_write = role_service.user_has_role(current_user, Roles.FILE_WRITE)

    return render_template(
        "files/list.htm",
        files=files,
        form=form,
        files_paginated=files_paginated,
        search=None,
        new_files=new_files,
        can_write=can_write,
    )


@blueprint.route("/content/<file:file>/<string:file_hash>/", methods=["GET"])
def content(file: File, file_hash):
    if file.category not in FileCategory.all_uploads() or file.hash != file_hash:
        return abort(404)
    if file.require_membership and not current_user.has_paid:
        return abort(403)
    return send_file_inline(file, file.full_display_name)


@app.after_request
def check_static_file(response: Response) -> Response:
    """
    Uploaded files in /static/files/* move scheduler.

    Before we used HashFS, files were directly uploaded in the static
    directory. These files are readable without proper authentication and can
    be scraped. This after request hook serves the file and then initiates
    move to the new system.

    This causes the file to be served and removed afterwards by the worker.
    """
    if response.status_code >= 400:
        return response

    if request.endpoint != "static" or "files" not in request.url:
        return response
    if not request.view_args:
        return response
    filename = request.view_args.get("filename")
    if not filename:
        return response

    # Disable this for now, as it is broken.
    file.move_file_into_hash_fs.delay(filename, {"base_url": request.host_url})
    return response
