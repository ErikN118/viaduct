from datetime import datetime, timedelta
from urllib.parse import urljoin

from flask import (
    Blueprint,
    Response,
    abort,
    flash,
    jsonify,
    redirect,
    render_template,
    request,
    url_for,
)
from flask_babel import gettext as _
from flask_login import current_user
from sqlalchemy import or_

from app import db
from app.decorators import require_role
from app.exceptions.base import BusinessRuleException
from app.models.activity import Activity
from app.roles import Roles
from app.service import activity_service, role_service
from app.service.pretix_service import PretixSettings
from app.service.setting_service import DatabaseSettingsMixin
from app.utils.serialize_sqla import serialize_sqla
from app.views.file import send_picture
from app.views.viewmodel.activity import ActivityViewModel

blueprint = Blueprint("activity", __name__, url_prefix="/activities")


class GoogleCalendarPublicSettings(DatabaseSettingsMixin):
    events_calendar_add_calendar_url: str = (
        "https://calendar.google.com/calendar/render?"
        "cid=via.uvastudent.org_rdn1ffk47v0gmla0oni69egmhk%40"
        "group.calendar.google.com"
    )

    events_calendar_ics_url: str = (
        "https://calendar.google.com/calendar/ical/"
        "via.uvastudent.org_rdn1ffk47v0gmla0oni69egmhk%40"
        "group.calendar.google.com/public/basic.ics"
    )


# Overview of activities
@blueprint.route("/", methods=["GET"])
@blueprint.route("/<any(archive):archive>/", methods=["GET"])
def view(archive=None):
    search = request.args.get("search", "")
    query = db.session.query(Activity)

    settings = GoogleCalendarPublicSettings(db_session=db.session)

    if archive == "archive":
        if current_user.is_anonymous:
            abort(403)
        query = query.filter(Activity.end_time < datetime.today())
        query = query.order_by(Activity.start_time.desc())
    else:
        query = query.order_by(Activity.start_time.asc())
        query = query.filter(Activity.end_time > (datetime.now() - timedelta(hours=12)))

    if search:
        search_key = f"%{search}%"
        query = query.filter(
            or_(Activity.en_name.ilike(search_key), Activity.nl_name.ilike(search_key))
        )

    can_write = role_service.user_has_role(current_user, Roles.ACTIVITY_WRITE)

    pagination = query.paginate(per_page=30, error_out=False)
    pagination.items = [ActivityViewModel(a, current_user) for a in pagination.items]

    return render_template(
        "activity/view.htm",
        activities=pagination,
        settings=settings,
        archive=archive,
        can_write=can_write,
        search=search,
    )


@blueprint.route("/<activity:activity>/remove/", methods=["POST"])
@require_role(Roles.ACTIVITY_WRITE)
def remove_activity(activity):
    try:
        activity_service.remove_activity(activity)
        flash(_("Activity has been removed successfully"))
    except BusinessRuleException:
        flash(
            _("Unable to delete activity: it is referenced by a newsletter"), "danger"
        )

    return redirect(url_for("activity.view"))


@blueprint.route("/<activity:activity>/", methods=["GET"])
@blueprint.route("/<activity_slug:activity>/", methods=["GET"])
def get_activity(activity: Activity):
    """
    Activity register and update endpoint.

    Register and update for an activity, with handling of custom forms
    and payment.
    """
    if activity.is_in_past() and current_user.is_anonymous:
        abort(403)

    view_model = ActivityViewModel(activity, current_user)
    return view_model.render()


@blueprint.route("/create/", methods=["GET"])
@require_role(Roles.ACTIVITY_WRITE)
def create_pretix():
    title = _("Create activity")
    activity = request.args.get("based_on", None)
    return render_template("vue_content.htm", title=title, based_on=activity)


@blueprint.route("/<activity:activity>/pretix/", methods=["GET"])
def pretix_view(activity: Activity):
    if not activity.pretix_event_slug:
        abort(404)
    pretix_settings = PretixSettings(db_session=db.session)
    slug = activity.pretix_event_slug
    return redirect(
        urljoin(
            str(pretix_settings.pretix_host),
            f"/control/event/{pretix_settings.pretix_organizer}/{slug}/",
        )
    )


@blueprint.route("/<activity:activity>/edit/", methods=["GET"])
@require_role(Roles.ACTIVITY_WRITE)
def edit(activity: Activity):
    title = _("Edit") + " " + str(activity.name)
    return render_template("vue_content.htm", title=title)


# TODO Word deze route door iets uberhaupt gebruikt?
@blueprint.route("/export/", methods=["GET"])
def export_activities():
    activities = (
        db.session.query(Activity)
        .filter(Activity.end_time > (datetime.now() - timedelta(hours=12)))
        .order_by(Activity.start_time.asc())
        .all()
    )
    return jsonify(data=serialize_sqla(activities))


@blueprint.route("/rss/", methods=["GET"])
@blueprint.route("/rss/<string:locale>/")
def rss(locale="en"):
    title = "via activiteiten" if locale == "nl" else "via activities"
    activities = (
        db.session.query(Activity)
        .filter(Activity.end_time > (datetime.now() - timedelta(hours=12)))
        .order_by(Activity.start_time.asc())
        .all()
    )

    rv = render_template(
        "activity/rss.xml", title=title, activities=activities, locale=locale
    )
    return Response(rv, mimetype="application/atom+xml")


@blueprint.route("/<activity:activity>/picture/")
@blueprint.route("/<activity:activity>/picture/<picture_type>/")
def picture(activity: Activity, picture_type=None):
    if activity.picture_file_id is None or (
        current_user.is_anonymous and activity.is_in_past()
    ):
        return redirect("/static/img/via_activity_default.png")

    return send_picture(
        activity.picture_file_id, f"activity_picture_{activity.name}", picture_type
    )
