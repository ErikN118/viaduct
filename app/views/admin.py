from flask import Blueprint, flash, redirect, render_template, url_for
from flask_babel import gettext
from flask_babel import lazy_gettext as _
from flask_login import current_user, login_required

from app.decorators import require_role
from app.forms.admin import BannerForm, ContractsSettingsForm, PrivacyPoliciesForm
from app.roles import Roles
from app.service import banner_service, contracts_service, privacy_service, role_service
from app.service.banner_service import Banner
from app.service.contracts_service import ContractsSetting
from app.service.privacy_service import PrivacyPolicies

blueprint = Blueprint("admin", __name__, url_prefix="/admin")


@blueprint.route("/", methods=["GET"])
@login_required
def view():
    def current_user_has_role(role):
        return role_service.user_has_role(current_user, role)

    if (
        role_service.user_needs_two_factor_authentication(current_user)
        and not current_user.tfa_enabled
    ):
        flash(
            _(
                "You have roles that require two-factor authentication, "
                "but you do not have two-factor authentication enabled!"
            ),
            "warning",
        )

    return render_template(
        "admin/view.htm", has_role=current_user_has_role, roles=Roles
    )


@blueprint.route("/banner/", methods=["GET", "POST"])
@require_role(Roles.BANNER_ADMIN)
def banner():
    banner = banner_service.find_banner()
    if not banner:
        banner = Banner("", "")

    form = BannerForm(obj=banner)

    if form.validate_on_submit():
        form.populate_obj(banner)
        banner_service.set_banner(banner)

        return redirect(url_for("admin.banner"))

    return render_template("admin/banner.htm", banner=banner, form=form)


@blueprint.route("/privacy/", methods=["GET", "POST"])
@require_role(Roles.BANNER_ADMIN)
def privacy():
    policies = privacy_service.find_policies()

    form = PrivacyPoliciesForm(obj=policies)
    if form.validate_on_submit():
        policies = PrivacyPolicies(url_nl=form.url_nl.data, url_en=form.url_en.data)
        privacy_service.set_privacy_policy(policies)
        return redirect(url_for("admin.privacy"))

    return render_template("admin/privacy.htm", policies=policies, form=form)


@blueprint.route("/async/", methods=["GET"])
@require_role(Roles.USER_WRITE)
def celery():
    return render_template("vue_content.htm", title=_("Async tasks"))


@blueprint.route("/company/")
@blueprint.route("/company/<path:_>")
@require_role(Roles.COMPANY_WRITE)
def company(_: str = ""):
    return render_template("vue_content.htm", title=gettext("Companies"))


@blueprint.route("/contracts/", methods=["GET", "POST"])
@require_role(Roles.COMPANY_WRITE)
def contracts():
    policies = contracts_service.find_settings()

    form = ContractsSettingsForm(obj=policies)
    if form.validate_on_submit():
        settings = ContractsSetting(
            sign_1=form.sign_1.data,
            sign_2=form.sign_2.data,
            contract_field_id=form.contract_field_id.data,
            invoice_data_field_id=form.invoice_data_field_id.data,
            kvk_field_id=form.kvk_field_id.data,
        )
        contracts_service.set_settings(settings)
        return redirect(url_for("admin.view"))

    return render_template("admin/contracts.htm", policies=policies, form=form)
