from flask import Blueprint, render_template
from flask_babel import gettext as _
from flask_login import login_required

blueprint = Blueprint("declaration", __name__, url_prefix="/declaration")


@blueprint.route("/", methods=["GET"])
@login_required
def view_declaration():
    return render_template("vue_content.htm", title=_("Declaration"))
