from flask import Blueprint, abort, redirect, request

blueprint = Blueprint("telegram", __name__, url_prefix="/telegram")


@blueprint.route("/<string:bot_username>/redirect/", methods=["GET"])
def convert_oauth_response(bot_username: str):
    code = request.args.get("code")
    if not code:
        return abort(400)
    redir = f"https://telegram.me/{bot_username}/?start={code}"
    return redirect(redir)
