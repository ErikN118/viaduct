import re
from datetime import timedelta

from flask import Blueprint, flash, make_response, redirect, render_template, url_for
from flask_babel import _
from flask_login import current_user, login_user, logout_user

from app.decorators import response_headers
from app.exceptions.base import (
    AuthorizationException,
    ResourceNotFoundException,
    ValidationException,
)
from app.forms.user import SignInForm, SignInOtpForm
from app.service import oauth_service, saml_service, user_service, user_tfa_service
from app.service.user_tfa_service import SignInSession
from app.views import get_safe_redirect_url, set_redirect_location_if_not_set

blueprint = Blueprint("login", __name__)

LOGIN_KWARGS = {"duration": timedelta(days=14), "remember": True}


@blueprint.route("/sign-in/", methods=["GET", "POST"])
@response_headers({"X-Frame-Options": "SAMEORIGIN"})
def sign_in():
    if current_user.is_authenticated:
        return redirect(get_safe_redirect_url())

    set_redirect_location_if_not_set()

    form = SignInForm()

    if form.validate_on_submit():
        try:
            email, password = form.email.data, form.password.data
            user = user_service.get_user_by_login(email, password)

            if user.tfa_enabled:
                user_tfa_service.session_init(True, user.id)

                return redirect(url_for("login.sign_in_otp"))

            # Notify the login manager that the user has been signed in.
            login_user(user, **LOGIN_KWARGS)

            return redirect(get_safe_redirect_url())

        except ResourceNotFoundException:
            flash(
                _(
                    "It appears that this account does not exist. Try again, or "
                    "contact the website administration at "
                    "ict (at) svia (dot) nl."
                )
            )
        except AuthorizationException:
            flash(
                _("Your account has been disabled, you are not allowed " "to log in"),
                "danger",
            )
        except ValidationException:
            flash(_("The password you entered appears to be incorrect."), "danger")

    return render_template("user/sign_in.htm", form=form, show_uvanetid_login=True)


@blueprint.route("/sign-in/otp/", methods=["GET", "POST"])
@response_headers({"X-Frame-Options": "SAMEORIGIN"})
def sign_in_otp():
    # Redirect the user to the index page if he or she has been authenticated
    # already.
    if current_user.is_authenticated:
        return redirect(get_safe_redirect_url())

    # The user has not signed in with their password yet
    session: SignInSession | None = user_tfa_service.session_get()
    if not session or not session.is_single_authenticated:
        return redirect(url_for("login.sign_in"))

    form = SignInOtpForm()

    if form.validate_on_submit():
        try:
            user = user_service.get_user_by_id(session.user_id)
            user_tfa_service.verify_totp(user.id, form.otp.data)

            login_user(user, **LOGIN_KWARGS)

            if session.confirming_student_id:
                user_service.set_confirmed_student_id(user, user.student_id)

                flash(_("Your account is now linked to this UvA account."), "success")

            user_tfa_service.session_clear_data()

            return redirect(get_safe_redirect_url())

        except ResourceNotFoundException:
            flash(
                _(
                    "It appears that this account does not exist. Try again, or "
                    "contact the website administration at "
                    "ict (at) svia (dot) nl."
                )
            )
        except AuthorizationException:
            flash(_("Invalid authentication code given."), "danger")

    return render_template("user/sign_in_otp.htm", form=form)


@blueprint.route("/sign-in/process-saml-response/", methods=["GET"])
@response_headers({"X-Frame-Options": "SAMEORIGIN"})
def sign_in_saml_response():
    has_redirected = False

    try:
        # Redirect the user to the index page if he or she has been
        # authenticated already.
        if current_user.is_authenticated:
            return redirect(get_safe_redirect_url())

        if not saml_service.user_is_authenticated():
            flash(_("Authentication failed. Please try again."), "danger")
            return redirect(get_safe_redirect_url())

        try:
            user = saml_service.get_user_by_uid(needs_confirmed=False)

            if user.student_id_confirmed:
                if user.tfa_enabled:
                    user_tfa_service.session_init(True, user.id)

                    return redirect(url_for("login.sign_in_otp"))

                login_user(user, **LOGIN_KWARGS)
            else:
                has_redirected = True
                return redirect(url_for("login.sign_in_confirm_student_id"))

        except (ResourceNotFoundException, ValidationException):
            flash(
                _(
                    "There is no via account linked to this UvA account. "
                    "On this page you can create a new via account that "
                    "is linked to your UvA account."
                )
            )
            has_redirected = True
            return redirect(url_for("signup.sign_up_saml_response"))

        return redirect(get_safe_redirect_url())
    finally:
        # Only clear the SAML data when we did not redirect to the sign up page
        if not has_redirected:
            saml_service.clear_saml_data()


@blueprint.route("/sign-in/confirm-student-id/", methods=["GET", "POST"])
@response_headers({"X-Frame-Options": "SAMEORIGIN"})
def sign_in_confirm_student_id():
    # Redirect the user if he or she has been authenticated already.
    if current_user.is_authenticated:
        return redirect(get_safe_redirect_url())

    if not saml_service.user_is_authenticated():
        flash(_("Authentication failed. Please try again."), "danger")
        return redirect(get_safe_redirect_url())

    student_id = saml_service.get_uid_from_attributes()

    try:
        users = user_service.get_all_users_with_unconfirmed_student_id(student_id)
    except ResourceNotFoundException:
        saml_service.clear_saml_data()
        return redirect(url_for("login.sign_in"))

    form = SignInForm()

    if form.validate_on_submit():
        try:
            user = user_service.get_user_by_login(form.email.data, form.password.data)

            if user.student_id != student_id:
                flash(
                    _(
                        "This account's student ID does not match "
                        "student ID '%(student_id)s'",
                        student_id=student_id,
                    ),
                    "danger",
                )
            elif user.tfa_enabled:
                user_tfa_service.session_init(True, user.id, True)

                return redirect(url_for("login.sign_in_otp"))

            else:
                user_service.set_confirmed_student_id(user, student_id)

                flash(_("Your account is now linked to this UvA account."), "success")

                # Notify the login manager that the user has been signed in.
                login_user(user, **LOGIN_KWARGS)

                saml_service.clear_saml_data()

                return redirect(get_safe_redirect_url())

        except ResourceNotFoundException:
            flash(
                _(
                    "It appears that this account does not exist. Try again, "
                    "or contact the website administration at "
                    "ict (at) svia (dot) nl."
                )
            )
        except AuthorizationException:
            flash(
                _("Your account has been disabled, you are not allowed " "to log in"),
                "danger",
            )
        except ValidationException:
            flash(_("The password you entered appears to be incorrect."), "danger")

    if len(users) == 1:
        user = users[0]
        [local_part, domain_part] = user.email.split("@")
        local_part_hidden = re.sub(r"(?<=.{2}).(?=.{2})", "*", local_part)
        domain_part_hidden = re.sub(r"(?<=.{2}).(?=.{3})", "*", domain_part)
        email_hidden = f"{local_part_hidden}@{domain_part_hidden}"

        flash(
            _(
                "The student ID '%(student_id)s' corresponds with via "
                "account %(email)s, but is not yet confirmed. Please login "
                "with your email address and password to prove that you own "
                "this account and to confirm your student ID.",
                student_id=student_id,
                email=email_hidden,
            )
        )
    else:
        flash(
            _(
                "The student ID '%(student_id)s' corresponds with "
                "multiple via accounts, but is not yet confirmed. "
                "Please login with your email address and password to prove "
                "that you own one of these accounts and "
                "to confirm your student ID.",
                student_id=student_id,
            )
        )

    return render_template("user/sign_in.htm", form=form, show_uvanetid_login=False)


@blueprint.route("/sign-out/")
def sign_out():
    if current_user.is_anonymous:
        flash(_("You must be logged in to sign-out."))
        return redirect(get_safe_redirect_url())

    # Remove all tokens and access_token cookie for user.
    oauth_service.revoke_user_tokens_by_client_id(current_user.id, "VIADUCT")
    redirect_url = make_response(redirect(get_safe_redirect_url()))
    redirect_url.set_cookie("access_token", "", expires=0)

    # Notify the login manager that the user has been signed out.
    logout_user()

    return redirect_url
