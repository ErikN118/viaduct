from app import get_locale
from app.service import banner_service


class BannerViewModel:
    def __init__(self):
        self.banner = banner_service.find_banner()

    @property
    def show_banner(self):
        if not self.banner:
            return False

        msg = self.banner.message_en if get_locale() == "en" else self.banner.message_nl

        return len(msg) > 0

    def get_message_for_locale(self):
        if not self.banner:
            return ""

        return (
            self.banner.message_en if get_locale() == "en" else self.banner.message_nl
        )
