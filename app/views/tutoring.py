from flask import Blueprint, render_template
from flask_babel import gettext as _
from flask_login import login_required

from app.decorators import require_role
from app.roles import Roles

blueprint = Blueprint("tutoring", __name__)


@blueprint.route("/tutoring/", methods=["GET"])
@blueprint.route("/tutoring/request/", methods=["GET"])
@login_required
def overview():
    return render_template("vue_content.htm", title=_("Tutoring"))


@blueprint.route("/tutoring/<tutoring:tutoring>/", methods=["GET"])
def tutoring(tutoring):
    return render_template("vue_content.htm", title=_("Tutoring"))


@blueprint.route("/tutors/", methods=["GET"])
@require_role(Roles.EXAMINATION_WRITE)
@login_required
def tutors():
    return render_template("vue_content.htm", title=_("Tutor overview"))


@blueprint.route("/tutors/<user_self:user>/", methods=["GET"])
@login_required
def tutor(user):
    return render_template("vue_content.htm", title=_("Tutor %s" % user.name))


@blueprint.route("/tutors/<user_self:user>/course/", methods=["GET"])
@login_required
def tutor_register(user):
    return render_template("vue_content.htm", title=_("Tutor %s" % user.name))
