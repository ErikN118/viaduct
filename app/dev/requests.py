def mocked_requests_get_json(json, status_code):
    """This method will be used by the mock to replace requests.get."""

    def mocked_request_get(*args, **kwargs):
        class MockResponse:
            def __init__(self, json_data, status_code):
                self.json_data = json_data
                self.status_code = status_code

            def json(self):
                return self.json_data

        return MockResponse(json, status_code)

    return mocked_request_get
