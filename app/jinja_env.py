import datetime
import json

from flask import Markup, render_template
from flask_login import current_user
from jinja2 import StrictUndefined
from markdown import markdown

from app import app, constants, get_locale, static_url
from app.forms.util import FormWrapper
from app.service import user_service
from app.utils import template_filters
from app.utils.frontend import css_files, js_files
from app.utils.seo import get_seo_fields
from app.utils.serialize_sqla import serialize_sqla

template_filters.register_filters(app)

# Set jinja global variables
app.jinja_env.undefined = StrictUndefined
app.jinja_env.globals.update(enumerate=enumerate)
app.jinja_env.globals.update(render_template=render_template)
app.jinja_env.globals.update(markdown=markdown)
app.jinja_env.globals.update(Markup=Markup)
app.jinja_env.globals.update(datetime=datetime)
app.jinja_env.globals.update(json=json)
app.jinja_env.globals.update(serialize_sqla=serialize_sqla)
app.jinja_env.globals.update(len=len)
app.jinja_env.globals.update(isinstance=isinstance)
app.jinja_env.globals.update(list=list)
app.jinja_env.globals.update(static_url=static_url)
app.jinja_env.globals.update(get_locale=get_locale)
app.jinja_env.globals.update(app_config=app.config)
app.jinja_env.globals.update(constants=constants)
app.jinja_env.globals.update(FormWrapper=FormWrapper)
app.jinja_env.globals.update(current_user=current_user)
# For the payment pending message that is used for all templates extending the
# base template:
app.jinja_env.globals.update(user_service=user_service)
app.jinja_env.globals.update(get_seo_fields=get_seo_fields)
app.jinja_env.globals.update(css_files=css_files)
app.jinja_env.globals.update(js_files=js_files)
