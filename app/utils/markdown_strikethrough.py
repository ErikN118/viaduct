import xml.etree.ElementTree as etree

from markdown.extensions import Extension
from markdown.inlinepatterns import InlineProcessor


class StrikethroughInlineProcessor(InlineProcessor):
    def handleMatch(self, m, data):
        el = etree.Element("del")
        el.text = m.group(1)
        return el, m.start(0), m.end(0)


class MarkdownStrikethroughExtension(Extension):
    def extendMarkdown(self, md):
        DEL_PATTERN = r"~~(.*?)~~"  # like ~~del~~
        md.inlinePatterns.register(
            StrikethroughInlineProcessor(DEL_PATTERN, md), "del", 175
        )
