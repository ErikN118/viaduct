def zegge(n: int) -> str:
    def below_20(m: int) -> str:
        below_20_strings = [
            "nul",
            "een",
            "twee",
            "drie",
            "vier",
            "vijf",
            "zes",
            "zeven",
            "acht",
            "negen",
            "tien",
            "elf",
            "twaalf",
            "dertien",
            "veertien",
        ]
        if n <= 14:
            return below_20_strings[m]
        return below_20_strings[n - 10] + "tien"

    if n < 20:
        return below_20(n)

    if n < 100:
        tens = [
            "",
            "tien",
            "twintig",
            "dertig",
            "veertig",
            "vijftig",
            "zestig",
            "zeventig",
            "tachtig",
            "negentig",
        ]

        q, rest = divmod(n, 10)
        if rest == 0:
            return tens[q]
        b = zegge(rest)
        return b + ("ën" if b.endswith("e") else "en") + tens[q]

    if n < 1000:
        q, rest = divmod(n, 100)
        return (zegge(q) if q > 1 else "") + "honderd" + (zegge(rest) if rest else "")

    if n % 1000 and not n % 100 and n < 10000:
        return zegge(n // 100) + "honderd"

    q, rest = divmod(n, 1000)
    return (zegge(q) if q > 1 else "") + "duizend" + (zegge(rest) if rest else "")
