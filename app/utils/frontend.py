import os

from flask import url_for

from app import app, static_url

# Ideally these functions would be constants, however, at this stage url_for does not
# work yet.

if app.debug and os.environ.get("VITE_SERVER"):
    vite_server = os.environ.get("VITE_SERVER", "http://localhost:5173")
    app.logger.debug(f"Connecting to Vite server at '{vite_server}'")

    def css_files():
        return [f"{vite_server}/frontend/assets/scss/app.scss"]

    def js_files():
        return [
            f"{vite_server}/{js}"
            for js in [
                "@vite/client",
                "frontend/index.ts",
            ]
        ]

else:
    app.logger.debug("Loading compiled static JS/CSS server")

    def css_files():
        return [static_url(url_for("static", filename="styles.css"))]

    def js_files():
        return [static_url(url_for("static", filename="main.js"))]
