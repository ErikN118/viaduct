import re
import xml.etree.ElementTree as etree

from markdown.extensions import Extension
from markdown.inlinepatterns import IMAGE_LINK_RE, ImageInlineProcessor

# From https://stackoverflow.com/a/38134765
YOUTUBE_LINK_RE = (
    r"(?:.+?)?(?:\/v\/|watch\/|\?v=|\&v=|youtu\.be\/"
    + r"|\/v=|^youtu\.be\/|watch\%3Fv\%3D)([a-zA-Z0-9_-]{11})+"
)


class MarkdownYoutubeExtension(Extension):
    def extendMarkdown(self, md):
        md.registerExtension(self)
        md.inlinePatterns.register(
            YoutubeInlineProcessor(IMAGE_LINK_RE, md), "youtube_link", 160
        )


class YoutubeInlineProcessor(ImageInlineProcessor):
    def handleMatch(self, m, data):
        text, index, handled = self.getText(data, m.end(0))
        if not handled:
            return None, None, None

        src, title, index, handled = self.getLink(data, index)
        if not handled:
            return None, None, None

        regex_match = re.match(YOUTUBE_LINK_RE, src)

        if not regex_match:
            return None, None, None

        (youtube_id,) = regex_match.groups()

        el = etree.Element("iframe")

        el.set("src", "https://www.youtube-nocookie.com/embed/" + youtube_id)
        el.set("width", "640")
        el.set("height", "480")
        el.set("frameborder", "0")
        el.set("allow", "autoplay; encrypted-media")

        if title is not None:
            el.set("title", title)

        el.set("alt", self.unescape(text))
        return el, m.start(0), index
