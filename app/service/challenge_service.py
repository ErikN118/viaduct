from datetime import date, datetime

from sqlalchemy import and_
from sqlalchemy.orm import Session

from app.exceptions.base import DuplicateResourceException, ValidationException
from app.extensions import db
from app.models.challenge import Challenge, Competitor, Submission
from app.repository import challenge_repo, model_service
from app.service import user_service

ALLOWED_EXTENSIONS = {"png", "gif", "jpg", "jpeg"}


def create_challenge(
    name: str,
    description: str,
    hint: str,
    start_date: datetime,
    end_date: datetime,
    weight: int,
    type_: str,
    answer: str,
):
    """Create a new challenge."""
    if name_exists(name):
        raise DuplicateResourceException("challenge", name)

    new_challenge = Challenge(
        name=name,
        description=description,
        hint=hint,
        start_date=start_date,
        end_date=end_date,
        weight=weight,
        type=type_,
        answer=answer,
    )
    model_service.save(new_challenge)
    return new_challenge


def name_exists(name, existing_challenge_id=None):
    query = db.session.query(Challenge).filter(Challenge.name == name)

    if existing_challenge_id:
        query = query.filter(Challenge.id != existing_challenge_id)

    return query.count() > 0


def create_submission(
    challenge: Challenge, user_id=None, submission: str = None, image_path=None
):
    """Create a submission for a challenge and user."""
    if is_approved(challenge, user_id):
        raise ValidationException("Already approved")
    if not is_open_challenge(challenge):
        raise ValidationException("Challenge closed")

    user = user_service.get_user_by_id(user_id)
    # convert the name.
    new_submission = Submission(
        challenge_id=challenge.id,
        user=user,
        answer=submission,
        image_path=image_path,
        approved=False,
    )
    db.session.add(new_submission)
    db.session.commit()

    return new_submission


def can_auto_validate(challenge: Challenge):
    """Check if a challenge can be auto validated."""
    return challenge.type == "Text"


def validate_question(submission: Submission, challenge: Challenge):
    """
    Check if a question is valid.

    Submission: String to be validated
    Challenge: Challenge object
    """
    if not can_auto_validate(challenge):
        return False, False

    if (
        submission.answer
        and challenge.answer
        and submission.answer.lower() == challenge.answer.lower()
    ):
        submission.approved = True
        assign_points_to_user(challenge.weight, submission.user_id)
        db.session.add(submission)
        db.session.commit()
        return True, True
    else:
        return True, False


def is_open_challenge(challenge: Challenge):
    challenge = (
        db.session.query(Challenge)
        .filter(
            and_(
                Challenge.id == challenge.id,
                Challenge.start_date <= date.today(),
                Challenge.end_date >= date.today(),
            )
        )
        .first()
    )

    return challenge is not None


def is_approved(challenge: Challenge, user_id: int):
    submission = (
        db.session.query(Submission)
        .filter(
            and_(
                Submission.user_id == user_id,
                Submission.challenge_id == challenge.id,
                Submission.approved.is_(True),
            )
        )
        .first()
    )

    return submission is not None


def get_points(user_id):
    competitor = (
        db.session.query(Competitor).filter(Competitor.user_id == user_id).first()
    )

    if competitor is None:
        return 0
    else:
        return competitor.points


def assign_points_to_user(points, user_id):
    competitor = (
        db.session.query(Competitor).filter(Competitor.user_id == user_id).first()
    )

    if competitor is None:
        competitor = Competitor(user_id=user_id)
        competitor.points = points
        save_competitor(competitor)
    else:
        competitor.points += points
        save_competitor(competitor)


def save_competitor(competitor: Competitor):
    db.session.add(competitor)
    db.session.commit()


def get_ranking():
    competitors = db.session.query(Competitor).order_by(Competitor.points.desc()).all()
    return competitors


def find_with_approved_submission(closed: bool, user_id: int):
    return challenge_repo.find_with_approved_submission(closed, user_id)


def update(
    challenge: Challenge,
    name: str,
    description: str,
    hint: str,
    start_date: datetime,
    end_date: datetime,
    weight: int,
    answer: str,
):
    if name_exists(name, challenge.id):
        raise DuplicateResourceException("challenge", name)
    challenge.name = name
    challenge.description = description
    challenge.hint = hint
    challenge.start_date = start_date
    challenge.end_date = end_date
    challenge.weight = weight
    challenge.answer = answer
    model_service.save(challenge)


def clear_ranking(db_session: Session) -> None:
    challenge_repo.clear_all_competitors(db_session)
    challenge_repo.clear_all_submissions(db_session)
