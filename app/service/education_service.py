import logging

from flask_sqlalchemy import Pagination

from app.api.schema import PageSearchParameters
from app.enums import ProgrammeType
from app.exceptions.base import (
    BusinessRuleException,
    DuplicateResourceException,
    ResourceNotFoundException,
)
from app.models.education import Education
from app.repository import education_repository
from app.repository.education_repository import get_educations_by_user_id

_logger = logging.getLogger(__name__)


def find_by_datanose_code(datanose_code: str) -> Education | None:
    return education_repository.find_by_datanose_code(datanose_code)


def get_education_duration(education_id: int) -> int:
    """Return the year of the latest bounded course, or 0."""
    return education_repository.get_education_duration(education_id)


def get_education_by_datanose_code(datanose_code: str) -> Education:
    education = education_repository.find_by_datanose_code(datanose_code)
    if not education:
        raise ResourceNotFoundException("Education", datanose_code)
    return education


def get_current_educations_by_user_id(user_id: int) -> list[Education]:
    """
    Fetches the highest education registered for the given user.
    Returns multiple educations when a user is enrolled for multiple educations
    of the same level.
    """
    educations = get_educations_by_user_id(user_id)
    programme_types = [education.programme_type for education in educations]
    return [
        education
        for education in educations
        if education.programme_type == max(programme_types)
    ]


def get_educations_by_datanose_codes(datanose_codes: list[str]) -> list[Education]:
    educations = []
    for dn_code in datanose_codes:
        education = find_by_datanose_code(dn_code)
        if education:
            educations.append(education)
        else:
            _logger.info("Unknown DataNose study [dn_code=%s]", dn_code)
    return educations


def get_education_name(education: Education, locale: str) -> str:
    if locale == "nl":
        name = education.nl_name
    else:
        name = education.en_name

    if education.programme_type != ProgrammeType.OTHER:
        name = f"{education.programme_type.abbreviation} {name}"

    return name


def get_all_educations() -> list[Education]:
    return education_repository.get_all_educations()


def get_all_via_educations() -> list[Education]:
    return education_repository.get_all_via_educations()


def paginated_search_educations(pagination: PageSearchParameters) -> Pagination:
    return education_repository.paginated_search_all_educations(pagination=pagination)


def add_education(
    nl_name: str,
    en_name: str,
    programme_type: ProgrammeType,
    is_via_programme: bool,
    datanose_code: str | None,
) -> Education | None:
    existing_education = education_repository.get_education_by_nl_name(
        nl_name
    ) or education_repository.get_education_by_en_name(en_name)

    if existing_education:
        raise DuplicateResourceException((nl_name, en_name), existing_education.id)

    education = education_repository.create_education()
    education.nl_name = nl_name
    education.en_name = en_name
    education.programme_type = programme_type
    education.is_via_programme = is_via_programme
    education.datanose_code = datanose_code if datanose_code else None

    education_repository.save_education(education)

    return education


def update_education(
    education_id: int,
    nl_name: str,
    en_name: str,
    programme_type: ProgrammeType,
    is_via_programme: bool,
    datanose_code: str | None,
) -> Education | None:
    education = education_repository.get_education_by_id(education_id)

    if not education:
        raise ResourceNotFoundException("Education", f"ID {education_id}")

    if education.en_name != en_name and education_repository.get_education_by_en_name(
        en_name
    ):
        raise DuplicateResourceException("Education: en_name", en_name)

    if education.nl_name != nl_name and education_repository.get_education_by_nl_name(
        nl_name
    ):
        raise DuplicateResourceException("Education: nl_name", nl_name)

    education.en_name = en_name
    education.nl_name = nl_name
    education.programme_type = programme_type
    education.is_via_programme = is_via_programme
    education.datanose_code = datanose_code if datanose_code else None

    education_repository.save_education(education)

    return education


def count_courses_by_education_id(education_id: int) -> int:
    return education_repository.count_courses_by_education_id(education_id)


def delete_education(education_id: int):
    if education_repository.count_courses_by_education_id(education_id) >= 1:
        raise BusinessRuleException("Education has courses")
    else:
        education_repository.delete_education(education_id)
