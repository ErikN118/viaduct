from typing import TypedDict

from flask import url_for
from sqlalchemy import func

from app.api.schema import PageSearchParameters
from app.exceptions.base import BusinessRuleException, DuplicateResourceException
from app.models.course import Course
from app.models.tutoring import Tutor, Tutoring
from app.models.user import User
from app.repository import model_service, tutoring_repo
from app.service import mail_service
from app.task.mail import MailCommand


def create_tutoring(user: User, course_id: int, num_hours: int, notes: str):
    course = model_service.get_by_id(Course, course_id)

    t = tutoring_repo.create_tutoring()
    t.tutee = user
    t.course = course
    t.num_hours = num_hours
    t.notes = notes

    tutoring_repo.save(t)

    tutoring_profile_url = url_for("tutoring.tutor", user="self", _external=True)
    tutee_profile_url = url_for("user.view_single_user", user=t.tutee, _external=True)

    command = MailCommand("bijles@svia.nl")
    command.with_template(
        "tutoring_request",
        locale="en",
        tutoring=t,
        tutoring_profile_url=tutoring_profile_url,
        tutee_profile_url=tutee_profile_url,
    )
    mail_service.send_mail(command)

    for tutor in tutoring_repo.find_tutors_for_course(course):
        command = MailCommand(tutor.user.email)
        command.with_template(
            "tutoring_request",
            locale="en",
            tutoring=t,
            tutoring_profile_url=tutoring_profile_url,
            tutee_profile_url=tutee_profile_url,
        )
        mail_service.send_mail(command)

    return t


def update_tutoring(
    tutoring: Tutoring, num_hours: int = None, notes: str = None, course_id: int = None
):
    if num_hours is not None:
        tutoring.num_hours = num_hours
    if notes is not None:
        tutoring.notes = notes
    if course_id is not None:
        tutoring.course_id = course_id
    tutoring_repo.save(tutoring)
    return tutoring


def get_tutorings_for_tutee(user: User) -> list[Tutoring]:
    return tutoring_repo.get_tutorings_for_tutee(user)


def get_tutorings_for_tutor(user: User) -> list[Tutoring]:
    return tutoring_repo.get_tutorings_for_tutor(user)


def delete_tutoring(tutoring: Tutoring) -> None:
    tutoring.deleted = func.now()
    tutoring_repo.save(tutoring)


def accept_tutoring(tutoring, user):
    if tutoring.tutor:
        raise BusinessRuleException("Tutoring already has a tutor")
    tutoring.tutor = user
    tutoring_repo.save(tutoring)

    command = MailCommand(to=tutoring.tutee.email)
    command.with_template(
        "tutoring_accepted_tutee",
        locale=tutoring.tutee.locale,
        tutoring=tutoring,
    )
    mail_service.send_mail(command)

    command = MailCommand(to="bijles@svia.nl")
    command.with_template(
        "tutoring_accepted",
        locale="en",
        tutoring=tutoring,
    )
    mail_service.send_mail(command)


def withdraw_tutoring(tutoring):
    ex_tutor = tutoring.tutor
    tutoring.tutor = None
    tutoring_repo.save(tutoring)

    command = MailCommand(to=tutoring.tutee.email)
    command.with_template(
        "tutoring_withdrawn_tutee",
        locale=tutoring.tutee.locale,
        tutoring=tutoring,
        ex_tutor=ex_tutor,
        tutoring_url=url_for("tutoring.tutoring", tutoring=tutoring, _external=True),
    )
    mail_service.send_mail(command)

    command = MailCommand(to="bijles@svia.nl")
    command.with_template(
        "tutoring_withdrawn",
        locale="en",
        tutoring=tutoring,
        ex_tutor=ex_tutor,
        tutoring_url=url_for("tutoring.tutoring", tutoring=tutoring, _external=True),
    )
    mail_service.send_mail(command)


def find_tutors(pagination: PageSearchParameters) -> list[Tutor]:
    return tutoring_repo.find_tutors(pagination)


class CourseRegistration(TypedDict):
    grade: int
    course_id: int


def register_tutor_courses(
    user: User, courses: list[CourseRegistration]
) -> list[Tutor]:
    courses_cache = {}

    # Do all validations before registering a single course.
    for tutor_course in courses:
        course_id = tutor_course["course_id"]
        courses_cache[course_id] = model_service.get_by_id(Course, course_id)
        if tutoring_repo.find_tutor(user=user, course=courses_cache[course_id]):
            raise DuplicateResourceException("tutor", courses_cache[course_id].name)

    registrations = []
    for tutor_course in courses:
        registrations.append(
            tutoring_repo.register_tutor(
                user=user,
                course=courses_cache[tutor_course["course_id"]],
                grade=tutor_course["grade"],
            )
        )

    command = MailCommand(to="bijles@svia.nl")
    command.with_template(
        "tutor_register",
        locale="en",
        user=user,
        registrations=registrations,
        tutor_overview_url=url_for("tutoring.tutors", _external=True),
    )
    mail_service.send_mail(command)

    return registrations


def get_tutorings(is_open: bool = None, tutor: User = None):
    return tutoring_repo.get_tutorings(is_open=is_open, tutor=tutor)


def approve_tutor(tutor: Tutor):
    tutor.approved = func.now()
    tutoring_repo.save(tutor)

    command = MailCommand(to=tutor.user.email)
    command.with_template(
        "tutor_approved",
        locale=tutor.user.locale,
        tutor=tutor,
        tutoring_profile_url=url_for("tutoring.tutor", user="self", _external=True),
    )
    mail_service.send_mail(command)


def find_courses_for_tutor(tutor: User) -> list[Tutor]:
    return tutoring_repo.find_courses_for_tutor(tutor)


def delete_tutor(tutor: Tutor) -> None:
    return model_service.delete(tutor)
