import io
import logging
import mimetypes
import re
import secrets

from fuzzywuzzy import fuzz
from PIL import Image
from werkzeug.datastructures import FileStorage

from app import hashfs
from app.enums import FileCategory
from app.exceptions.base import ResourceNotFoundException
from app.models.file import File
from app.repository import file_repository

FILENAME_REGEX = re.compile(r"(.+)\.([^\s.]+)")

_logger = logging.getLogger(__name__)


def add_file(category: FileCategory, data: FileStorage, thumbnail=False):
    filename = data.filename or secrets.token_hex(3)
    m = FILENAME_REGEX.match(filename)
    if not m:
        orig_display_name = filename
        extension = get_extension_from_mimetype(data.mimetype)
    else:
        orig_display_name = m.group(1)
        extension = m.group(2)

    if thumbnail:
        data.stream = image_resize(data, extension)

    display_name: str | None
    if category in FileCategory.all_uploads():
        display_name = orig_display_name
        filename_unique = False
        i = 0

        # Create a unique full display name
        while not filename_unique:
            i += 1
            duplicate = file_repository.find_file_by_display_name(
                display_name, extension
            )
            if duplicate is not None:
                display_name = f"{orig_display_name}_{i}"
            else:
                filename_unique = True
    else:
        display_name = None

    address = hashfs.put(data)

    f = file_repository.create_file()
    f.display_name = display_name
    f.hash = address.id
    f.extension = extension
    f.category = category

    file_repository.save(f)

    return f


def delete_file(_file):
    _hash = _file.hash
    file_repository.delete(_file)
    if not file_repository.find_all_files_by_hash(_hash):
        hashfs.delete(_hash)


def get_file_by_id(file_id: int | None) -> File:
    f = file_repository.find_file_by_id(file_id)
    if not f:
        raise ResourceNotFoundException("file", file_id)
    return f


def get_thumbnail_of_file(_file: File, thumbnail_size=None) -> io.BytesIO:
    if not thumbnail_size:
        thumbnail_size = (400, 400)

    try:
        with hashfs.open(_file.hash) as f:
            return image_resize(f, _file.extension, thumbnail_size)
    except OSError as e:
        _logger.error("Error processing thumbnail", exc_info=True)
        raise ResourceNotFoundException("file content", _file.hash) from e


def image_resize(f: FileStorage, extension, to_size=None) -> io.BytesIO:
    if not to_size:
        to_size = (400, 400)

    im = Image.open(f)
    im.thumbnail(to_size)

    out = io.BytesIO()
    im.save(out, format=im.format or extension)
    out.seek(0)
    return out


def get_file_mimetype(_file, add_http_text_charset="utf-8"):
    try:
        mimetype = mimetypes.types_map["." + _file.extension]
        if mimetype.startswith("text/") and add_http_text_charset is not None:
            mimetype += "; charset=" + add_http_text_charset

        return mimetype
    except KeyError:
        return "application/octet-stream"


def get_extension_from_mimetype(mimetype: str):
    ext = mimetypes.guess_extension(mimetype)
    if ext:
        return ext[1:]
    return ""


def get_all_uploads(page_nr=None, per_page=None):
    return file_repository.find_all_files_by_categories(
        FileCategory.all_uploads(), page_nr, per_page
    )


def get_all_files(page_nr=None, per_page=None):
    return file_repository.find_all_files(page_nr, per_page)


def search_files_in_uploads(search):
    search = search.lower()
    all_files = get_all_uploads()
    file_scores = {}

    for f in all_files:
        score = fuzz.partial_ratio(search, f.full_display_name.lower())
        if score > 75:
            file_scores[f] = score

    files = sorted(file_scores, key=file_scores.get, reverse=True)
    return files
