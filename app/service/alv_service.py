from datetime import datetime, timezone
from itertools import chain

from sqlalchemy.orm import Session
from werkzeug.utils import secure_filename

from app.enums import FileCategory
from app.exceptions.base import BusinessRuleException, ResourceNotFoundException
from app.models.activity import Activity
from app.models.alv_model import Alv, AlvEventEntry, AlvUser, AlvUserProxy
from app.models.user import User
from app.repository import alv_repository, model_service, user_repository
from app.service import file_service
from app.service.alv_parse_service import AlvEvent, AlvEventData, AlvMetaData


def add_minutes(alv: Alv, minutes_file):
    if alv.activity and alv.activity.end_time > datetime.now(timezone.utc):
        raise BusinessRuleException("ALV has not yet taken place.")

    _file = file_service.add_file(FileCategory.ALV_DOCUMENT, minutes_file)
    alv.minutes_file_id = _file.id

    alv_repository.save(alv)


def find_all_alv():
    return alv_repository.find_all_alv()


def find_alv_by_id(alv_id, include_presidium, include_documents):
    return alv_repository.find_alv_by_id(
        alv_id, include_presidium=include_presidium, include_documents=include_documents
    )


def get_latest_alv(db_session: Session) -> Alv:
    alv = alv_repository.get_latest_alv(db_session)
    if not alv:
        raise ResourceNotFoundException("alv", "latest")
    return alv


def get_alv_by_id(
    alv_id: int, include_presidium: bool = True, include_documents: bool = False
) -> Alv:
    alv = find_alv_by_id(alv_id, include_presidium, include_documents)
    if not alv:
        raise ResourceNotFoundException("alv", alv_id)
    return alv


def find_alv_document_by_id(alv_document_id, include_versions):
    return alv_repository.find_alv_document_by_id(alv_document_id, include_versions)


def get_alv_document_by_id(alv_document_id, include_versions=False):
    alv_document = find_alv_document_by_id(alv_document_id, include_versions)
    if not alv_document:
        raise ResourceNotFoundException("alv document", alv_document_id)
    return alv_document


def get_alv_document_version_filename(alv_document, version_number, _file):
    basename = alv_document.get_localized_basename()

    fn = secure_filename(basename)
    if version_number > 1:
        fn += f"_v{version_number}"

    if _file.extension:
        fn += f".{_file.extension}"

    return fn


def get_alv_document_version_file(alv_document_version):
    _file = file_service.get_file_by_id(alv_document_version.file_id)
    return _file


def get_alv_minutes_file(alv):
    _file = file_service.get_file_by_id(alv.minutes_file_id)
    return _file


def get_alv_minutes_filename(alv, _file):
    basename = alv.get_localized_basename()

    fn = f"{secure_filename(basename)}_minutes"

    if _file.extension:
        fn += "." + _file.extension

    return fn


def add_document(alv, file_storage, nl_name, en_name):
    alv_document = alv_repository.create_document()
    alv_document.alv = alv
    alv_document.en_name = en_name
    alv_document.nl_name = nl_name

    add_document_version(alv_document, file_storage)

    alv_repository.save_document(alv_document)

    return alv_document


def add_document_version(alv_document, file_storage):
    _file = file_service.add_file(FileCategory.ALV_DOCUMENT, file_storage)

    alv_doc_version = alv_repository.create_document_version()
    alv_doc_version.alv_document = alv_document
    alv_doc_version.file = _file

    alv_repository.save_document_version(alv_doc_version)


def update_document(alv_document, file_storage, nl_name, en_name):
    """Update a ALV document's names and add a new version."""
    alv_document.nl_name = nl_name
    alv_document.en_name = en_name

    if file_storage:
        add_document_version(alv_document, file_storage)

    alv_repository.save_document(alv_document)


def delete_alv(alv: Alv):
    if alv.minutes_file_id or alv.documents or alv.meta:
        raise BusinessRuleException(
            "Cannot delete ALV that has documents or parsed data."
        )
    alv_repository.delete_alv(alv)


def add(
    nl_name: str,
    en_name: str,
    date: datetime,
    chairman_user_id: int = None,
    secretary_user_id: int = None,
    activity_id: int = None,
):
    alv = Alv()
    alv.nl_name = nl_name
    alv.en_name = en_name
    alv.date = date

    if chairman_user_id:
        alv.chairman = model_service.get_by_id(User, chairman_user_id)

    if secretary_user_id:
        alv.secretary = model_service.get_by_id(User, secretary_user_id)

    if activity_id:
        alv.activity = model_service.get_by_id(Activity, activity_id)

    alv_repository.save(alv)
    return alv


def update(
    alv: Alv,
    nl_name,
    en_name,
    date,
    chairman_user_id=None,
    secretary_user_id=None,
    activity_id=None,
    minutes_accepted=None,
):
    alv.nl_name = nl_name
    alv.en_name = en_name
    alv.date = date
    alv.minutes_accepted = minutes_accepted

    if chairman_user_id and (
        chairman := model_service.get_by_id(User, chairman_user_id)
    ):
        alv.chairman = chairman

    if secretary_user_id and (
        secretary := model_service.get_by_id(User, secretary_user_id)
    ):
        alv.secretary = secretary

    if activity_id and (activity := model_service.get_by_id(Activity, activity_id)):
        alv.activity = activity

    alv_repository.save(alv)
    return alv


def replace_parsed_alv_data(
    alv: Alv, meta: AlvMetaData, events: list[AlvEventData], errors: list[str]
):
    # Combines errors into event list
    event_errors = [
        AlvEventData(event_type=AlvEvent.PARSE_WARNING, data={"message": err})
        for err in errors
    ]
    events.extend(event_errors)

    # Convert all events to AlvEventEntry
    entries = [
        AlvEventEntry(alv_id=alv.id, event_type=event["event_type"], data=event["data"])
        for event in events
    ]

    # Find user.ids for present users
    user_ids = user_repository.get_user_ids_like(meta["present"])
    alv_users = [
        AlvUser(alv_id=alv.id, user_id=_id, name=name) for _id, name in user_ids
    ]

    # Get a flat list of all people mentioned in proxies
    all_proxy_names = list(set(chain(*meta["proxies"])))

    # Get a list of AlvUserProxy items
    proxy_map = {
        name: _id for _id, name in user_repository.get_user_ids_like(all_proxy_names)
    }
    alv_proxies = [
        AlvUserProxy(
            alv_id=alv.id,
            absentee_name=absentee,
            voter_name=voter,
            absentee_id=proxy_map[absentee] if absentee in proxy_map else None,
            voter_id=proxy_map[voter] if voter in proxy_map else None,
        )
        for absentee, voter in meta["proxies"]
    ]

    alv_repository.delete_parsed_alv_data(alv)

    # We save these seperately
    del meta["present"]
    del meta["proxies"]

    alv.meta = meta

    alv_repository.create_parsed_alv_data(alv, entries, alv_users, alv_proxies)
    alv_repository.save(alv)


def get_parsed_alv_data(
    alv: Alv,
) -> tuple[AlvMetaData, list[AlvEventData], list[str]] | None:
    if not alv.meta:
        return None

    alv_events, alv_users, alv_proxies = alv_repository.get_parsed_alv_data(alv)

    events: list[AlvEventData] = []
    errors: list[str] = []

    for e in alv_events:
        if e.event_type == AlvEvent.PARSE_WARNING:
            # TODO !745 sqlalchemy 1.4 stubs will allow this.
            errors.append(e.data["message"])  # type: ignore
        else:
            events.append(AlvEventData(event_type=e.event_type, data=e.data))

    present = [u.name for u in alv_users]
    proxies = [(p.absentee_name, p.voter_name) for p in alv_proxies]

    meta = alv.meta
    # TODO !745 sqlalchemy 1.4 stubs will allow this.
    meta["present"] = present  # type: ignore
    meta["proxies"] = proxies  # type: ignore

    # TODO !745 sqlalchemy 1.4 stubs will allow this.
    return meta, events, errors  # type: ignore
