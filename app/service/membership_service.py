import celery

from app import db
from app.enums import ProgrammeType
from app.exceptions.base import ValidationException
from app.models.education import Education
from app.models.mollie import Transaction, TransactionMembership
from app.models.user import User
from app.repository import user_repository
from app.service import mollie_service, user_service
from app.service.setting_service import DatabaseSettingsMixin
from app.task import copernica


class MembershipCostSettings(DatabaseSettingsMixin):
    membership_normal_price: int = 2000
    membership_master_price: int = 0


def can_pay_membership(user: User) -> bool:
    educations: list[Education] = user.educations.all()
    return is_user_valid_for_auto_confirmation(user) and is_any_study_from_via(
        educations
    )


def is_user_valid_for_auto_confirmation(user: User) -> bool:
    return (
        not user.is_anonymous
        and user.is_authenticated
        and not user.has_paid
        and user.is_active
        and not user.alumnus
        and user.student_id_confirmed
    )


def is_any_study_from_via(educations: list[Education]) -> bool:
    # TODO: this relies on that the studies are up-to-date
    # they are almost always when the user signs up when the account was just
    # created, but there might have to be a task that repeatedly updates this.
    for edu in educations:  # type: Education
        if edu.is_via_programme:
            return True

    return False


def get_membership_price_from_via_studies(educations: list[Education]) -> int:
    # Get the membership price for the given list of studies.
    # Calling this without checking if any study is from via will result in a
    # ValidationException.
    cost_settings = MembershipCostSettings(db_session=db.session)

    for edu in educations:
        if edu.is_via_programme and (
            edu.programme_type == ProgrammeType.MASTER
            or edu.programme_type == ProgrammeType.PRE_MASTER
        ):
            return cost_settings.membership_master_price

    for edu in educations:
        if edu.is_via_programme:
            return cost_settings.membership_normal_price

    raise ValidationException("None of the educations are from via")


def confirm_free_membership(user: User):
    educations: list[Education] = user.educations.all()
    if (
        not is_user_valid_for_auto_confirmation(user)
        or not is_any_study_from_via(educations)
        or get_membership_price_from_via_studies(educations) != 0
    ):
        raise ValidationException("Not eligible for free membership")

    set_membership_status(user, True)


def create_membership_transaction(user: User) -> tuple[Transaction, str]:
    if not can_pay_membership(user):
        raise ValidationException("Cannot pay membership")

    callback = TransactionMembership.from_user(user)

    description = "{} ({}), {}".format(user.name, user.student_id, "via membership fee")

    transaction, checkout_url = mollie_service.create_transaction(
        amount=get_membership_price_from_via_studies(user.educations),
        description=description,
        user=user,
        callback=callback,
    )

    return transaction, checkout_url


def on_membership_paid(transaction: TransactionMembership):
    # note: this method may be called for the webhook request, a user or a
    # manual check for the payment.
    # the user in the callback may not be the current user,
    # for example when the user session expired when the transaction was
    # completed and the browser was redirected to the user.
    # it completely depends on what payment was completed and notified with
    # the callback.
    user = user_service.find_by_id(transaction.user_id)
    set_membership_status(user, True)


def set_membership_status(user, has_paid: bool = True):
    # No-op when status is the same.
    if user.has_paid == has_paid:
        return

    user.has_paid = has_paid

    user_repository.save(user)

    task = copernica.update_user.si(user.id)
    if has_paid:
        task = celery.chain(
            task, copernica.send_welcome_mail.si(user.id).set(countdown=10)
        )
    task()
