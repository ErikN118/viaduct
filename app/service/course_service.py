import datetime
from typing import Any

from flask_sqlalchemy import Pagination

from app import db
from app.api.schema import PageCourseSearchParameters, PageSearchParameters
from app.exceptions.base import (
    BusinessRuleException,
    DuplicateResourceException,
    ResourceNotFoundException,
    ValidationException,
)
from app.models.course import Course
from app.models.education import Education
from app.repository import course_repository
from app.service import datanose_service, education_service, examination_service


def get_courses_by_ids(course_ids: list[int]) -> list[Course]:
    """Return courses based on a list of ids."""
    return course_repository.get_courses_by_ids(course_ids)


def get_courses_by_datanose_codes(datanose_codes: list[str]) -> list[Course]:
    """Return courses based on a list of datanose codes."""
    return course_repository.get_courses_by_datanose_codes(datanose_codes)


def get_education_course_by_exclusion_ids(
    ignored_pairs: list[tuple[int, int]], education_ids: list[int]
):
    """
    Returns all education_course entries not in the list of pairs, and with
    a provided education id.
    """
    return course_repository.get_education_course_by_exclusion_ids(
        ignored_pairs, education_ids
    )


def paginated_get_courses_by_ids(
    course_ids: list[int], pagination: PageCourseSearchParameters
) -> Pagination:
    return course_repository.paginated_get_courses_by_ids(course_ids, pagination)


def get_course_ids_by_education_year(
    education_year: list[tuple[int, int]]
) -> list[int]:
    """
    Get list of courses that are of a given education-year combination.
    :type education_year: (education_id, year).
    """
    return course_repository.get_course_ids_by_education_year(education_year)


def paginated_search_courses(pagination: PageSearchParameters) -> Pagination:
    return course_repository.paginated_search_all_courses(pagination=pagination)


def get_education_course(education_id: int, course_id: int):
    return course_repository.get_education_course(education_id, course_id)


def update_education_course_year_periods(
    education_id: int, course_id: int, year: int | None, periods: list[int] | None
):
    """Updates the year and periods for a given education_course entry."""
    course_repository.update_education_course_year_periods(
        education_id, course_id, year, periods
    )


def update_education_course_links(
    education: Education,
    course: Course,
    courses_response: list[dict[str, Any]] | None = None,
):
    """
    Updates the education-course links for all bounded educations.
    The provided education must be the main one of the course in Datanose.
    This can be checked via http://api.datanose.nl/Courses/{course_dn_code}.
    """
    if not education.datanose_code:
        return

    # Fetch all bounded educations with year and periods for course.
    programmes = datanose_service.get_course_programmes_by_codes(
        course.datanose_code, education.datanose_code, courses_response
    )

    # Add all education-course links from found programmes, if programme in database.
    if programmes:
        for programme in programmes:
            try:
                education = education_service.get_education_by_datanose_code(
                    programme["ProgrammeCode"]
                )
            except ResourceNotFoundException:
                continue

            # Only add if found programme is a via-programme.
            if not education.is_via_programme:
                continue

            if course_repository.get_education_course(education.id, course.id):
                course_repository.update_education_course_year_periods(
                    education.id, course.id, programme["Year"], programme["Periods"]
                )
            else:
                course_repository.add_education_course(
                    education.id, course.id, programme["Year"], programme["Periods"]
                )


def add_course_using_datanose(
    datanose_code: str, courses_response: list[dict[str, Any]] | None = None
) -> Course:
    """
    :courses_response is an optional list of courses that are part of the
    course's Owner education as in the Datanose API.
    The used course must be the one represented by the :datanose_code argument.
    """
    existing_course = course_repository.find_course(datanose_code=datanose_code)
    if existing_course:
        raise DuplicateResourceException("course", datanose_code)

    datanose_course = datanose_service.get_course_by_code(datanose_code)
    if not datanose_course:
        raise ValidationException("Unknown datanose code")

    # Check if the corresponding education exists. Transform 404 error to 409
    # in order to notify user via Axios.
    education_code = datanose_course[-1]["Owner"]
    try:
        education = education_service.get_education_by_datanose_code(education_code)
    except ResourceNotFoundException:
        raise BusinessRuleException(f"Education {education_code} not supported.")

    # Create the new course.
    course = course_repository.create_course()
    course.name = datanose_course[-1]["NameDutch"]
    course.datanose_code = datanose_code
    course_repository.save_course(course)
    db.session.flush()

    # Add all course-education links for bounded courses.
    update_education_course_links(education, course, courses_response)

    return course


def update_course(course: Course, name: str) -> Course | None:
    if course.datanose_code:
        raise BusinessRuleException("DataNose coupled courses update automatically.")
    name = name.strip()

    if course.name != name and course_repository.find_course(name=name):
        raise DuplicateResourceException("Course", name)

    course.name = name.strip()
    course_repository.save_course(course)

    return course


def delete_course(course: Course):
    if examination_service.count_examinations_by_course(course) >= 1:
        raise BusinessRuleException("Course has examinations")
    else:
        course_repository.delete_course(course)


def course_available(course: Course, date: datetime.date) -> bool:
    if not course.datanose_code:
        return True

    academic_year = date.year
    if date.month < 9:
        academic_year -= 1

    datanose_course = datanose_service.get_course_by_code(course.datanose_code)
    return any(x["AcademicYear"] == academic_year for x in datanose_course)


def get_course(name=None, datanose_code=None) -> Course:
    """
    Get a course that has the exact name or datanose_code as is provided.
    Throw a ResourceNotFound exception when no course was found.
    """
    course = course_repository.find_course(name=name, datanose_code=datanose_code)
    if not course:
        if datanose_code:
            raise ResourceNotFoundException("Course", datanose_code)
        else:
            raise ResourceNotFoundException("Course", name)
    return course
