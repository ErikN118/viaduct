from datetime import date, datetime, timedelta
from typing import TypedDict

import pytz
from babel.dates import format_datetime
from flask import url_for
from unidecode import unidecode
from werkzeug.datastructures import FileStorage

from app import db, get_locale
from app.api.schema import MultilangStringDict, PageSearchParameters
from app.enums import FileCategory
from app.exceptions.base import (
    BusinessRuleException,
    ResourceNotFoundException,
    ValidationException,
)
from app.models.activity import Activity
from app.models.newsletter import NewsletterActivity
from app.models.user import User
from app.repository import activity_repository
from app.service import file_service, pretix_service
from app.service.pretix_service import PretixPaymentStatus
from app.utils import google
from app.views.viewmodel.slideactivity import SlideActivityModel


def get_by_id(activity_id: int) -> Activity:
    activity = activity_repository.find_activity_by_id(activity_id)
    if not activity:
        raise ResourceNotFoundException("activity", activity_id)
    return activity


def get_activities_ending_after_now() -> list[Activity]:
    return activity_repository.get_activities_ending_after_now()


def activity_slug(activity: Activity) -> str:
    if activity.en_name:
        name = "".join(c for c in activity.en_name if c.isalnum())
    else:
        name = "".join(c for c in activity.nl_name if c.isalnum())
    name = unidecode(name)
    year = activity.end_time.year

    # Pretix allows for slugs up to 50, so limit name to 44 + 1 dash + 4 year
    # digits to make sure we are under the limit.
    return f"{name[:44]}-{year}"


def remove_activity(activity: Activity) -> None:
    # Ensure activity is not referenced from newsletter
    # (issue #343)
    is_used_in_newsletter = (
        db.session.query(NewsletterActivity)
        .filter(NewsletterActivity.activity_id == activity.id)
        .first()
        is not None
    )

    if is_used_in_newsletter:
        message = "Activity cannot be removed: it is used in a newsletter"
        raise BusinessRuleException(message)

    # Remove the event from google calendar
    google.delete_activity(activity.google_event_id)

    # Remove it
    db.session.delete(activity)
    db.session.commit()


def get_by_pretix_slugs(event_slugs: list[str]) -> list[Activity]:
    return activity_repository.get_activities_pretix(event_slugs)


def get_upcoming_past_for_user(user: User) -> tuple[list[Activity], list[Activity]]:
    pretix_orders = pretix_service.get_user_orders(user, PretixPaymentStatus.active)
    pretix_event_slugs = list(pretix_orders.keys())

    # Future
    future_pretix_based_activities = activity_repository.get_upcoming_pretix(
        pretix_event_slugs
    )

    # Past
    past_pretix_based_activities = activity_repository.get_past_pretix(
        pretix_event_slugs
    )

    return (
        future_pretix_based_activities,
        past_pretix_based_activities,
    )


class ActivityApiData(TypedDict):
    name: MultilangStringDict
    description: MultilangStringDict
    start_time: datetime
    end_time: datetime
    location: str
    price: str
    pretix_event_slug: str | None


def create_pretix_activity_from_pretix(
    create_data: ActivityApiData, user: User
) -> Activity:
    activity = Activity()

    # Move form fields
    activity.nl_name = create_data["name"]["nl"]
    activity.en_name = create_data["name"]["en"]
    activity.nl_description = create_data["description"]["nl"]
    activity.en_description = create_data["description"]["en"]
    activity.end_time = create_data["end_time"]
    activity.start_time = create_data["start_time"]
    activity.location = create_data["location"]
    activity.price = create_data["price"]

    # Keep track of who created the activity.
    activity.owner_id = user.id

    activity = activity_repository.flush_activity(activity)
    url = url_for("activity.get_activity", activity=activity, _external=True)

    google_activity = google.insert_activity(
        activity.nl_name,
        f"{activity.nl_description}\n\n{url}",
        activity.location_name,
        activity.start_time,
        activity.end_time,
    )

    if google_activity:
        activity.google_event_id = google_activity["id"]

    activity.pretix_event_slug = create_data["pretix_event_slug"]

    return activity_repository.save_activity(activity)


def edit_activity_from_pretix(activity: Activity, update_data: ActivityApiData) -> None:
    activity.nl_name = update_data["name"]["nl"]
    activity.en_name = update_data["name"]["en"]
    activity.nl_description = update_data["description"]["nl"]
    activity.en_description = update_data["description"]["en"]
    activity.start_time = update_data["start_time"]
    activity.end_time = update_data["end_time"]
    activity.location = update_data["location"]
    activity.price = update_data["price"]

    activity.pretix_event_slug = update_data["pretix_event_slug"]

    if activity.google_event_id:
        url = url_for("activity.get_activity", activity=activity, _external=True)

        google.update_activity(
            activity.google_event_id,
            activity.nl_name,
            f"{activity.nl_description}\n\n{url}",
            activity.location_name,
            activity.start_time,
            activity.end_time,
        )

    activity_repository.save_activity(activity)


def set_activity_picture(activity: Activity, file: FileStorage) -> None:
    if not file.mimetype.startswith("image/"):
        raise ValidationException("Invalid filetype for activity picture")

    picture_file = file_service.add_file(FileCategory.ACTIVITY_PICTURE, file)

    old_picture_id = activity.picture_file_id
    activity.picture_file_id = picture_file.id

    if old_picture_id:
        old_picture = file_service.get_file_by_id(old_picture_id)
        file_service.delete_file(old_picture)

    activity_repository.save_activity(activity)


def remove_activity_picture(activity: Activity) -> None:
    old_picture_id = activity.picture_file_id
    activity.picture_file_id = None

    if old_picture_id:
        old_picture = file_service.get_file_by_id(old_picture_id)
        file_service.delete_file(old_picture)

    activity_repository.save_activity(activity)


def paginated_search_all_activities(pagination: PageSearchParameters):
    return activity_repository.paginated_search_all_activities(pagination)


class MultidayActSpacing(SlideActivityModel):
    def __init__(self, i: int) -> None:
        self.weekdays = [i]
        self.is_visible = False


def set_spacing_for_concurrent_md_activities(
    act_list: list[SlideActivityModel],
) -> list[list[SlideActivityModel]]:
    """
    Give every activity the correct spacing and place it in the correct row.

    To do this, we sort from long to short activities, and greedily try to put them in
    the upper most row. If this is not possible, we create a new row at the bottom.
    """
    sorted_act = sorted(act_list, key=lambda x: x.duration, reverse=True)
    rows: list[list[SlideActivityModel]] = [[]]

    # Place every activity in a row
    for activity in sorted_act:
        placed = False
        for i, row in enumerate(rows):
            if not any(i.overlaps(activity) for i in row if i.is_visible):
                row.append(activity)
                activity.cell = i
                placed = True
                break

        # Create a new row if the activity could not be added to an existing row.
        if not placed:
            rows.append([activity])
            activity.cell = len(rows) - 1

    # Add dummy activities for spacing between activities.
    for row in rows:
        for day in range(7):
            if not any(day in act.current_weekdays for act in row):
                row.append(MultidayActSpacing(day))
    return rows


def set_spacing_for_concurrent_sd_activities(
    activities,
) -> list[list[SlideActivityModel]]:
    """
    Give every activity the correct spacing and place it in the correct column.

    To do this, we sort from long to short activities, and greedily try to put them
    in the left most column. If this is not possible, we create a new column on the
    right.
    """
    sorted_activities = sorted(activities, key=lambda x: x.duration, reverse=True)
    rows: list[list[SlideActivityModel]] = [[]]

    # Place every activity in a column
    for activity in sorted_activities:
        placed = False
        for i, row in enumerate(rows):
            if not any(i.overlaps(activity) for i in row):
                row.append(activity)
                activity.cell = i
                placed = True
                break

        # Create a new row if the activity could not be added to an existing row.
        if not placed:
            rows.append([activity])
            activity.cell = len(rows) - 1

    for activity in activities:
        for other in activities:
            if activity != other and activity.overlaps(other):
                activity.has_overlapping_activities = True
                other.has_overlapping_activities = True

    return rows


def get_activities_for_slide() -> (
    tuple[list[list[SlideActivityModel]], list[list[SlideActivityModel]]]
):
    activities_this_week = activity_repository.get_this_week()

    # Split the activities in multi or single day events.
    multi_day = []
    single_day = []
    for act in activities_this_week:
        slide_act = SlideActivityModel(act)
        if slide_act.is_multiday():
            multi_day.append(slide_act)
        else:
            single_day.append(slide_act)

    return (
        set_spacing_for_concurrent_sd_activities(single_day),
        set_spacing_for_concurrent_md_activities(multi_day),
    )


def get_max_weekday(sd, md) -> int:
    sd_max = max([4, *list(item.end_day for sl in sd for item in sl)]) + 1
    md_max = (
        max(
            [
                4,
                *list(
                    item.end_day if item.is_visible else 0 for sl in md for item in sl
                ),
            ]
        )
        + 1
    )
    return max(sd_max, md_max)


def get_time_info(
    single_day_activities, multi_day_activities, start_hour: int = 9, end_hour: int = 24
):
    today = date.today()
    _, week, _ = today.isocalendar()
    monday = today - timedelta(days=today.weekday())

    # Always show monday to friday, but show more if there are events in the weekend.
    # +1 is to change maximum index to a number of days.
    duration_days = get_max_weekday(single_day_activities, multi_day_activities)

    # Get the name of days (monday, tuesday, ..., sunday) in the current locale.
    days_description = []
    for x in range(duration_days):
        day = monday + timedelta(days=x)
        weekday = format_datetime(day, "EEEE", locale=get_locale())
        month = str(day.day)
        is_today = day == today

        days_description.append((weekday, month, is_today))

    tz = pytz.timezone("Europe/Amsterdam")
    now = datetime.now(tz)
    seconds_since_morning = (
        now - now.replace(hour=start_hour, minute=0, second=0, microsecond=0)
    ).total_seconds()
    hour_line_position = seconds_since_morning / ((end_hour - start_hour) * 60 * 60)
    if hour_line_position < 0.0 or hour_line_position > 1.0:
        # Hide it
        hour_line_position = -1.0

    return duration_days, days_description, week, hour_line_position
