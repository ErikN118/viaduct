import logging

from app.exceptions.base import ResourceNotFoundException
from app.models.group import Group
from app.repository import google_repository
from app.repository.google_repository import GoogleGroup
from app.service import group_service
from app.task import google

DOMAIN = "svia.nl"
_logger = logging.getLogger(__name__)


def get_group_key_for_group(group: Group) -> str | None:
    if group.mailtype == "mailinglist":
        return f"{group.maillist}@{DOMAIN}"
    elif group.mailtype == "mailbox" and group.maillist is not None:
        return get_group_key_for_mailbox(group.maillist)
    else:
        return None


def get_group_key_for_mailbox(maillist: str) -> str:
    return f"list-{maillist}@{DOMAIN}"


def get_user_key_for_group(group: Group) -> str | None:
    if group.mailtype == "mailbox" and group.maillist is not None:
        return get_user_key_for_maillist(group.maillist)
    return None


def get_user_key_for_maillist(maillist: str) -> str:
    return f"{maillist}@{DOMAIN}"


def find_google_group_by_id(group_id: int) -> GoogleGroup | None:
    group = group_service.get_by_id(group_id=group_id)
    group_key = get_group_key_for_group(group)
    if group_key:
        google_group = google_repository.find_group_by_group_key(group_key)
        if not google_group:
            raise ResourceNotFoundException("Google group", group_key)
    else:
        raise ResourceNotFoundException("Google group of group", group_id)

    return google_group


def find_google_group_by_maillist(maillist: str) -> GoogleGroup | None:
    group_key = get_group_key_for_mailbox(maillist)
    return google_repository.find_group_by_group_key(group_key)


def remove_email_from_google_group(group: Group, email: str):
    group_key = get_group_key_for_group(group)
    if group_key:
        google.remove_email_from_google_group.delay(email, group_key)


def add_email_to_google_group(group: Group, email: str):
    group_key = get_group_key_for_group(group)
    if group_key:
        google.add_email_to_google_group.delay(email, group_key)


def find_google_user_for_maillist(maillist: str):
    user_key = get_user_key_for_maillist(maillist)
    return google_repository.find_google_user(user_key)
