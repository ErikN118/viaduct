from typing import TypedDict

from pydantic import SecretStr

from app import db
from app.api.schema import PageSearchParameters
from app.exceptions.base import BusinessRuleException, ResourceNotFoundException
from app.models.activity import Activity
from app.models.news import News
from app.models.newsletter import (
    Newsletter,
    NewsletterActivity,
    NewsletterGreeting,
    NewsletterNewsItem,
)
from app.models.user import User
from app.repository import news_repository
from app.service import activity_service
from app.service.activity_service import MultilangStringDict
from app.service.setting_service import DatabaseSettingsMixin
from app.utils.pagination import Pagination


def get_paginated_news(only_published: bool, user: User, page: int) -> Pagination:
    return news_repository.get_paginated_news(only_published, user, page)


def find_all() -> list[News]:
    return news_repository.find_all()


def get_news_by_id(news_id: int) -> News:
    news = news_repository.find_news_by_id(news_id)
    if news is None:
        raise ResourceNotFoundException("news", news_id)
    return news


def get_news_by_user_id(user_id: int) -> list[News]:
    return news_repository.get_news_by_user_id(user_id)


def delete_news(news):
    if news_repository.news_used_in_newsletter(news):
        msg = "News cannot be removed: it is used in a newsletter"
        raise BusinessRuleException(msg)

    news_repository.delete_news(news)


def paginated_search_all_news(pagination: PageSearchParameters):
    return news_repository.paginated_search_all_news(pagination)


def get_initial_newsletter_items() -> tuple[list[Activity], list[News]]:
    activities = activity_service.get_activities_ending_after_now()
    news_items = news_repository.find_all_recent()
    return activities, news_items


class NewsletterItem(TypedDict):
    item_id: int
    overwrite_text: MultilangStringDict


class NewsletterGreetingItem(TypedDict):
    nl: str
    en: str
    sender: str


def create_newsletter(
    news: list[NewsletterItem],
    activity: list[NewsletterItem],
    greetings: list[NewsletterGreetingItem],
):
    newsletter = Newsletter()

    update_newsletter(newsletter, news, activity, greetings)

    return newsletter


def update_newsletter(
    newsletter,
    news: list[NewsletterItem],
    activities: list[NewsletterItem],
    greetings: list[NewsletterGreetingItem],
):
    newsletter.activities.clear()
    newsletter.news_items.clear()
    newsletter.greetings.clear()

    for activity in activities:
        newsletter.activities.append(
            NewsletterActivity(
                activity=activity_service.get_by_id(activity["item_id"]),
                nl_description=activity["overwrite_text"]["nl"],
                en_description=activity["overwrite_text"]["en"],
            )
        )

    for news_item in news:
        newsletter.news_items.append(
            NewsletterNewsItem(
                news_item=get_news_by_id(news_item["item_id"]),
                nl_description=news_item["overwrite_text"]["nl"],
                en_description=news_item["overwrite_text"]["en"],
            )
        )

    for greeting in greetings:
        newsletter.greetings.append(
            NewsletterGreeting(
                nl=greeting["nl"], en=greeting["en"], sender=greeting["sender"]
            )
        )

    news_repository.save(newsletter)


def update_news_item(news: News, data) -> News:
    news.en_title = data["title"]["en"]
    news.nl_title = data["title"]["nl"]
    news.en_content = data["content"]["en"]
    news.nl_content = data["content"]["nl"]
    news.publish_date = data["publish_date"]
    news.sponsored = data.get("sponsored", False)
    news_repository.save(news)
    return news


def create_news_item(data, creator: User):
    news = News()
    news.en_title = data["title"]["en"]
    news.nl_title = data["title"]["nl"]
    news.en_content = data["content"]["en"]
    news.nl_content = data["content"]["nl"]
    news.publish_date = data["publish_date"]
    news.sponsored = data.get("sponsored", False)
    news.user = creator
    news_repository.save(news)
    return news


class NewsletterCopernicaTokenSettings(DatabaseSettingsMixin):
    copernica_newsletter_token = SecretStr("")


def get_newsletter_auth_token():
    settings = NewsletterCopernicaTokenSettings(db_session=db.session)
    return settings.copernica_newsletter_token.get_secret_value()


def get_all_newsletters():
    return news_repository.get_all_newsletters()


def delete_newsletter(newsletter: Newsletter):
    news_repository.delete_newsletter(newsletter)
