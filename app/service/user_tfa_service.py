from datetime import datetime, timedelta, timezone
from typing import NamedTuple

import pyotp
from flask import session

from app.exceptions.base import (
    AuthorizationException,
    BusinessRuleException,
    RateLimitedException,
)
from app.repository import user_repository
from app.service import user_service
from app.service.user_service import LOGIN_RETRY_TIMEOUT

SESSION_TFA_DATA = "tfa_data"
SESSION_TIMEOUT = timedelta(minutes=5)


class SignInSession(NamedTuple):
    created: datetime
    is_single_authenticated: bool
    user_id: int | None
    confirming_student_id: bool


def session_clear_data():
    if SESSION_TFA_DATA in session:
        del session[SESSION_TFA_DATA]


def session_init(
    is_single_authenticated: bool, user_id: int, confirming_student_id=False
):
    session[SESSION_TFA_DATA] = SignInSession(
        created=datetime.now(timezone.utc),
        is_single_authenticated=is_single_authenticated,
        user_id=user_id,
        confirming_student_id=confirming_student_id,
    )


def session_get() -> SignInSession | None:
    data = session.get(SESSION_TFA_DATA)

    if not data:
        return None

    si_session = SignInSession(*data)

    if datetime.now(timezone.utc) - si_session.created > SESSION_TIMEOUT:
        session_clear_data()
        return None

    return si_session


def session_is_active() -> bool:
    return session_get() is not None


def get_two_factor_provisioning_uri(user_id, issuer="svia.nl") -> tuple[str, str]:
    """
    First step in the process of enabling TFA.

    This generates two things;
        - A secret that will be used to generate one time passwords
        - A URI that contains this secret that can be read by
            apps like Google Authenticator to later generate one
            time passwords.

    To actually enable TFA the user has to actually put the secret
    in his/her app and verify that they can generate a one time password.

    :param user_id:
    :param issuer:
    :return: (secret, uri) tuple
    """
    user = user_service.get_user_by_id(user_id)

    if user.tfa_enabled:
        raise BusinessRuleException(
            "User already has two-factor" "authentication enabled."
        )

    secret = pyotp.random_base32()

    user.totp_secret = secret
    user.secret_gen_time = datetime.now()
    user_repository.save(user)

    return (
        secret,
        pyotp.totp.TOTP(user.totp_secret).provisioning_uri(
            user.email, issuer_name=issuer
        ),
    )


def enable_two_factor_authentication(user_id, secret, otp):
    """
    Second step in the process of enabling TFA.

    The user has to verify they can generate a one time password by
    the given secret. The secret also has to match with what we generated
    server-side before
    :param user_id:
    :param secret:
    :param otp:
    :return:
    """
    user = user_service.get_user_by_id(user_id)

    if user.tfa_enabled:
        raise BusinessRuleException(
            "User already has two-factor" "authentication enabled."
        )

    if user.totp_secret != secret:
        raise BusinessRuleException("Invalid totp secret given.")

    totp = pyotp.TOTP(user.totp_secret)

    if totp.verify(otp, valid_window=3):
        if datetime.now() - user.secret_gen_time > timedelta(minutes=5):
            raise BusinessRuleException("Took to long to verify secret/otp")

        user.tfa_enabled = True
        user_repository.save(user)
    else:
        raise AuthorizationException("Invalid OTP given.")


def disable_two_factor_authentication(user_id, otp):
    """
    Disables 2fa, if the user can provide the right otp.

    :param user_id:
    :param otp:
    :return:
    """
    user = user_service.get_user_by_id(user_id)

    if not user.tfa_enabled:
        raise BusinessRuleException(
            "User does not have two-factor" "authentication enabled."
        )

    verify_totp(user_id, otp)

    user.totp_secret = None
    user.tfa_enabled = False
    user.secret_gen_time = None

    user_repository.save(user)


def verify_totp(user_id, otp: str):
    """Checks if a one time password for 2fa is correct."""
    user = user_service.get_user_by_id(user_id)

    if not user.tfa_enabled or not user.totp_secret:
        raise BusinessRuleException(
            "User does not have two-factor" "authentication enabled."
        )

    # Prevents replay attack
    if user.last_otp == otp:
        raise AuthorizationException("Cannot reuse OTP.")

    # Prevents brute force
    if (
        user.last_login_attempt is not None
        and datetime.now() - user.last_login_attempt < LOGIN_RETRY_TIMEOUT
    ):
        raise RateLimitedException("Cannot verify OTP at this time.")

    totp = pyotp.TOTP(user.totp_secret)

    if not totp.verify(otp, valid_window=3):
        user.last_login_attempt = datetime.now()
        user_repository.save(user)

        raise AuthorizationException("Invalid one time password given.")

    user.last_otp = otp
    user_repository.save(user)
