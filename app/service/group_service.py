from datetime import datetime

import pydantic
from flask_sqlalchemy import Pagination
from sqlalchemy.orm import Session

from app.api.schema import PageSearchParameters
from app.exceptions.base import (
    AuthorizationException,
    DuplicateResourceException,
    ResourceNotFoundException,
)
from app.models.group import Group
from app.models.user import User
from app.repository import group_repository, role_repository
from app.service import google_service

from ..utils import google
from . import user_service


def find_group_by_id(group_id: int) -> Group:
    return group_repository.find_by_id(group_id)


def get_by_id(group_id: int) -> Group:
    group = group_repository.find_by_id(group_id)
    if not group:
        raise ResourceNotFoundException("group", group_id)
    return group


def get_group_by_name(group_name):
    group = group_repository.get_group_by_name(group_name)
    if not group:
        raise ResourceNotFoundException("group", group_name)
    return group


def paginated_search_all_groups(pagination: PageSearchParameters) -> Pagination:
    """Get all groups with search and pagination."""
    return group_repository.paginated_search_all_groups(pagination)


def get_groups_for_user(user: User) -> list[Group]:
    user_groups = group_repository.get_groups_for_user(user)
    return [ug.group for ug in user_groups]


def paginated_search_group_users(
    group_id: int, pagination: PageSearchParameters
) -> Pagination:
    return group_repository.paginated_search_group_users(group_id, pagination)


def get_group_users(group_id: int):
    return group_repository.get_group_users(group_id)


def check_user_member_of_group(user, group_id):
    if user_member_of_group(user, group_id):
        return
    raise AuthorizationException(f"User not in group identified by {group_id}")


def user_member_of_group(user: User, group_id: int) -> bool:
    if not group_id:
        return False

    group = group_repository.find_by_id(group_id)
    group_users = get_group_users(group_id)
    return group is not None and user in group_users


def remove_group_users(group: Group, user_ids: list[int]) -> None:
    group_users = get_group_users(group.id)

    users = [user_service.get_user_by_id(user_id) for user_id in user_ids]

    for user in users:
        if user not in group_users:
            raise ResourceNotFoundException("user in group", user.id)

    group_repository.remove_group_users(group, user_ids)

    for user in users:
        google_service.remove_email_from_google_group(group, user.email)


def add_group_users(group: Group, user_ids: list[int]) -> None:
    users = user_service.get_users_by_ids(user_ids)

    new_users = list(set(users) - set(group.users))

    group_repository.add_group_users(group, new_users)

    for u in new_users:
        google_service.add_email_to_google_group(group, u.email)


def group_users_have_tfa_enabled(group_id) -> bool:
    group = get_by_id(group_id)

    return all(u.tfa_enabled for u in group.users.all())


def group_requires_tfa(group_id: int) -> bool:
    """
    Checks if the group has roles that require two-factor auth.

    :param group_id:
    :return:
    """
    roles = role_repository.find_all_roles_by_group_id(group_id)

    return any(role.tfa_required for role in roles)


def check_mailbox_ready(maillist: str) -> None:
    google_user = google_service.find_google_user_for_maillist(maillist)
    if not google_user:
        key = google_service.get_user_key_for_maillist(maillist)
        raise ResourceNotFoundException("Google user", key)
    google_group = google_service.find_google_group_by_maillist(maillist)
    if not google_group:
        key = google_service.get_group_key_for_mailbox(maillist)
        raise ResourceNotFoundException("Google group", key)


def edit_group(group: Group, name: str, mailtype: str, maillist: str):
    existing_group = group_repository.get_group_by_name(name)
    if existing_group is not None and group.id != existing_group.id:
        raise DuplicateResourceException("group", "name")

    if maillist:
        existing_group = group_repository.get_group_by_maillist(maillist)
        if existing_group is not None and group.id != existing_group.id:
            raise DuplicateResourceException("group", "maillist")

    group.name = name
    group.mailtype = mailtype
    if mailtype == "none":
        group.maillist = None
    else:
        group.maillist = maillist

        # Only automatically create mailing lists
        if mailtype == "mailinglist":
            google.create_group_if_not_exists(name, maillist)
        if mailtype == "mailbox":
            check_mailbox_ready(maillist)

        for user in group.users.all():
            google_service.add_email_to_google_group(group, user.email)

    group_repository.save(group)


def create_group(name: str, mailtype: str, maillist: str | None) -> Group:
    if group_repository.get_group_by_name(name) is not None:
        raise DuplicateResourceException("group", "name")
    if maillist:
        if group_repository.get_group_by_maillist(maillist) is not None:
            raise DuplicateResourceException("group", "maillist")
        if mailtype == "mailinglist":
            google.create_group_if_not_exists(name, maillist)
        if mailtype == "mailbox":
            check_mailbox_ready(maillist)

    return group_repository.create_group(name, mailtype, maillist)


def delete_group(group: Group) -> None:
    if group.mailtype == "mailinglist":
        google.delete_group(group.maillist)
    # elif group.mailtype == "mailbox":
    #     google.delete_group(group.maillist)
    group_repository.delete_group(group)


def remove_user_history(db_session: Session, user: User) -> None:
    return group_repository.remove_user_history(db_session, user)


class UserGroupMembership(pydantic.BaseModel):
    member_since: datetime
    member_until: datetime | None = None
    group_name: str
    group_id: int


def get_user_group_memberships(
    db_session: Session, user: User
) -> list[UserGroupMembership]:
    current_groups = group_repository.get_groups_for_user(user)
    history_groups = group_repository.get_groups_history_for_user(db_session, user)

    return [
        UserGroupMembership(
            member_since=cg.created, group_name=cg.group.name, group_id=cg.group_id
        )
        for cg in current_groups
    ] + [
        UserGroupMembership(
            member_since=hg.created,
            member_until=hg.deleted,
            group_name=hg.group.name,
            group_id=hg.group_id,
        )
        for hg in history_groups
    ]
