import re

from app.exceptions.base import (
    BusinessRuleException,
    DuplicateResourceException,
    ResourceNotFoundException,
)
from app.models.mailinglist_model import MailingList
from app.repository import mailinglist_repository
from app.service import copernica_service

_COPERNICA_COLUMN_NAME_REGEX = re.compile(r"^[A-Za-z]\w*$")


def _check_copernica_column_name(copernica_column_name: str) -> None:
    if not _COPERNICA_COLUMN_NAME_REGEX.match(copernica_column_name):
        raise BusinessRuleException("The format of copernica_column_name is invalid")

    # We cannot perform these checks when the Copernica
    # integration is not enabled (e.g. during local development)
    # so we just accept it.
    if copernica_service.copernica_integration_enabled():
        database_fields = copernica_service.get_database_fields()
        copernica_service.validate_column(copernica_column_name, database_fields)


def create_mailing(
    nl_name: str,
    en_name: str,
    nl_description: str,
    en_description: str,
    copernica_column_name: str,
    members_only: bool = False,
    default: bool = False,
) -> MailingList:
    duplicate = mailinglist_repository.find_mailinglist_by_nl_name(nl_name)
    if duplicate:
        raise DuplicateResourceException(duplicate, "nl_name")

    duplicate = mailinglist_repository.find_mailinglist_by_en_name(en_name)
    if duplicate:
        raise DuplicateResourceException(duplicate, "en_name")

    duplicate = mailinglist_repository.find_mailinglist_by_copernica_column_name(
        copernica_column_name
    )
    if duplicate:
        raise DuplicateResourceException(duplicate, "copernica_column_name")

    _check_copernica_column_name(copernica_column_name)

    mailinglist = mailinglist_repository.create_mailinglist()
    mailinglist.nl_name = nl_name
    mailinglist.en_name = en_name
    mailinglist.nl_description = nl_description
    mailinglist.en_description = en_description
    mailinglist.copernica_column_name = copernica_column_name
    mailinglist.members_only = members_only
    mailinglist.default = default

    mailinglist_repository.save_mailinglist(mailinglist)

    return mailinglist


def update_mailinglist(
    mailinglist_id: int,
    nl_name: str,
    en_name: str,
    nl_description: str,
    en_description: str,
    copernica_column_name: str,
    members_only: bool = False,
    default: bool = False,
):
    mailinglist = mailinglist_repository.find_mailinglist_by_id(mailinglist_id)
    if not mailinglist:
        raise ResourceNotFoundException("Mailing list", mailinglist_id)

    duplicate = mailinglist_repository.find_mailinglist_by_nl_name(nl_name, mailinglist)
    if duplicate:
        raise DuplicateResourceException(duplicate, "nl_name")

    duplicate = mailinglist_repository.find_mailinglist_by_en_name(en_name, mailinglist)
    if duplicate:
        raise DuplicateResourceException(duplicate, "en_name")

    duplicate = mailinglist_repository.find_mailinglist_by_copernica_column_name(
        copernica_column_name, mailinglist
    )
    if duplicate:
        raise DuplicateResourceException(duplicate, "copernica_column_name")

    _check_copernica_column_name(copernica_column_name)

    mailinglist.nl_name = nl_name
    mailinglist.en_name = en_name
    mailinglist.nl_description = nl_description
    mailinglist.en_description = en_description
    mailinglist.copernica_column_name = copernica_column_name
    mailinglist.members_only = members_only
    mailinglist.default = default

    mailinglist_repository.save_mailinglist(mailinglist)


def get_mailinglist(mailinglist_id: int) -> MailingList:
    mailinglist = mailinglist_repository.find_mailinglist_by_id(mailinglist_id)

    if not mailinglist:
        raise ResourceNotFoundException("Mailing list", mailinglist_id)

    return mailinglist


def delete_mailinglist(mailinglist_id: int):
    mailinglist = mailinglist_repository.find_mailinglist_by_id(mailinglist_id)

    if not mailinglist:
        raise ResourceNotFoundException("Mailing list", mailinglist_id)

    if mailinglist_repository.find_subscribed_users_of_mailinglist(mailinglist):
        raise BusinessRuleException(
            "There are still users subscribed to this mailing list"
        )

    mailinglist_repository.delete_mailinglist(mailinglist)


def get_all_mailinglists(include_members_only: bool = True) -> list[MailingList]:
    return mailinglist_repository.find_all_mailinglists(include_members_only)


def get_default_mailinglists() -> list[MailingList]:
    return mailinglist_repository.find_default_mailinglists()


def get_mailinglist_name(mailinglist: MailingList, locale: str) -> str:
    if locale == "nl":
        return mailinglist.nl_name
    else:
        return mailinglist.en_name
