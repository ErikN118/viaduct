from datetime import datetime

from dateutil.relativedelta import relativedelta
from flask_babel import lazy_gettext as _
from flask_wtf import FlaskForm
from flask_wtf.recaptcha import Recaptcha, RecaptchaField
from wtforms import (
    BooleanField,
    DateField,
    FileField,
    PasswordField,
    SelectField,
    StringField,
)
from wtforms.ext.sqlalchemy.fields import QuerySelectMultipleField
from wtforms.validators import EqualTo, InputRequired, Length, Optional, ValidationError

from app import constants, get_locale
from app.forms.fields import EmailField
from app.forms.util import FieldVerticalSplit
from app.models.education import Education
from app.service import education_service


class ResetPasswordForm(FlaskForm):
    password = PasswordField(
        _("New password"),
        validators=[
            InputRequired(),
            Length(
                message=(
                    _(
                        "Minimal password length: %(length)d",
                        length=constants.MIN_PASSWORD_LENGTH,
                    )
                ),
                min=constants.MIN_PASSWORD_LENGTH,
            ),
        ],
        render_kw={"autocomplete": "new-password"},
    )
    password_repeat = PasswordField(
        _("Repeat new password"),
        validators=[
            InputRequired(),
            EqualTo("password", message=_("Passwords do not match")),
        ],
        render_kw={"autocomplete": "new-password"},
    )


def get_education_name(education: Education) -> str:
    return education_service.get_education_name(education, get_locale())


class UnconfirmedStudentIDForm(FlaskForm):
    student_id = StringField(_("Student ID"))
    educations = QuerySelectMultipleField(
        _("Study programmes"),
        validators=[InputRequired()],
        query_factory=education_service.get_all_educations,
        get_label=get_education_name,
        get_pk=lambda e: e.id,
    )


class BaseUserForm(FlaskForm):
    email = EmailField(
        _("E-mail adress"),
        validators=[InputRequired(), Length(max=200)],
        render_kw={"autocomplete": "email"},
    )
    first_name = StringField(
        _("First name"),
        validators=[InputRequired(), Length(max=256)],
        render_kw={"autocomplete": "given-name"},
    )
    last_name = StringField(
        _("Last name"),
        validators=[InputRequired(), Length(max=256)],
        render_kw={"autocomplete": "family-name"},
    )

    address = StringField(
        _("Address"),
        validators=[InputRequired(), Length(max=256)],
        render_kw={"autocomplete": "street-address"},
    )
    zip = StringField(
        _("Zip code"),
        validators=[InputRequired(), Length(max=8)],
        render_kw={"autocomplete": "postal-code"},
    )
    city = StringField(
        _("City"),
        validators=[InputRequired(), Length(max=256)],
        render_kw={"autocomplete": "address-level2"},
    )
    country = SelectField(
        _("Country"),
        choices=[
            (
                "Nederland",
                _("Netherlands"),
            ),
            ("No dutch address", _("I don't have a Dutch address")),
        ],
        default="Nederland",
        validators=[InputRequired(), Length(max=256)],
        render_kw={"autocomplete": "country"},
    )

    # Dates
    birth_date = DateField(
        _("Birthdate"), validators=[InputRequired()], render_kw={"autocomplete": "bday"}
    )

    # Optional fields
    locale = SelectField(
        _("Language"),
        validators=[Optional()],
        choices=list(constants.LANGUAGES.items()),
        default="",
        render_kw={"autocomplete": "language"},
    )
    phone_nr = StringField(
        _("Phone"),
        validators=[Optional(), Length(max=16)],
        render_kw={"autocomplete": "tel"},
    )


class UserAvatarForm(FlaskForm):
    avatar = FileField(_("Upload new avatar:"), validators=[InputRequired()])


class SAMLSignUpForm(BaseUserForm):
    birth_date = DateField(
        _("Birthdate"), validators=[InputRequired()], render_kw={"autocomplete": "bday"}
    )
    recaptcha = RecaptchaField(validators=[Recaptcha(message="Check Recaptcha")])
    agree_with_privacy_policy = BooleanField(
        validators=[InputRequired(_("Please agree with our Privacy Policy."))]
    )

    register_split = FieldVerticalSplit(
        [
            ["first_name", "last_name", "birth_date", "email"],
            ["address", "zip", "city", "country"],
        ],
        large_spacing=True,
    )

    _RenderIgnoreFields = [
        "locale",
        "phone_nr",
        "avatar",
        "agree_with_privacy_policy",
        "recaptcha",
    ]

    def validate_birth_date(self, field):  # pylint: disable=no-self-use
        sixteen_years_ago = datetime.now().date() - relativedelta(years=16)

        if field.data is None:
            return

        if field.data > sixteen_years_ago:
            raise ValidationError(_("You need to be at least 16 years old."))


class ManualSignUpForm(SAMLSignUpForm, UnconfirmedStudentIDForm):
    password = PasswordField(
        _("New password"),
        validators=[
            InputRequired(),
            Length(
                message=(
                    _(
                        "Minimal password length: %(length)d",
                        length=constants.MIN_PASSWORD_LENGTH,
                    )
                ),
                min=constants.MIN_PASSWORD_LENGTH,
            ),
        ],
        render_kw={"autocomplete": "new-password"},
    )
    register_split = FieldVerticalSplit(
        [
            [
                "first_name",
                "last_name",
                "birth_date",
                "address",
                "zip",
                "city",
                "country",
            ],
            [
                "email",
                "password",
                "student_id",
                "educations",
            ],
        ],
        large_spacing=True,
    )


class EditUserForm(BaseUserForm):
    """Edit a user as administrator."""

    register_split = FieldVerticalSplit(
        [
            ["first_name", "last_name", "address", "zip", "city", "country"],
            ["email", "birth_date", "student_info"],
        ],
        large_spacing=True,
    )

    optional_split = FieldVerticalSplit([["phone_nr"], ["locale"]], large_spacing=True)

    alumnus = BooleanField(_("Alumnus"))


class SignInForm(FlaskForm):
    email = EmailField(
        _("E-mail address"),
        validators=[InputRequired()],
        render_kw={"autocomplete": "username"},
    )
    password = PasswordField(
        _("Password"),
        validators=[InputRequired()],
        render_kw={"autocomplete": "current-password"},
    )


class SignInOtpForm(FlaskForm):
    otp = StringField(
        _("Authentication code"),
        validators=[InputRequired()],
        render_kw={
            "pattern": "[0-9]*",
            "autocomplete": "one-time-code",
            "inputmode": "numeric",
        },
    )


class RequestPassword(FlaskForm):
    email = EmailField(
        _("E-mail address"),
        validators=[InputRequired()],
        render_kw={"autocomplete": "username"},
    )
    recaptcha = RecaptchaField(validators=[Recaptcha(message="Check Recaptcha")])


class ChangePasswordForm(ResetPasswordForm):
    current_password = PasswordField(
        _("Current Password"),
        validators=[
            InputRequired(),
        ],
        render_kw={"autocomplete": "current-password"},
    )


class EditUvALinkingForm(FlaskForm):
    student_id = StringField(_("Student ID"), validators=[Optional()])
    student_id_confirmed = BooleanField(
        _("Link this account to the corresponding UvA account")
    )

    def validate_student_id(self, field):
        if self.student_id_confirmed.data and not field.data:
            raise ValidationError(
                _(
                    "A student ID is required when this account"
                    " is linked to a UvA account."
                )
            )
