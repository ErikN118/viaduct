from flask_babel import lazy_gettext as _
from flask_wtf import FlaskForm
from flask_wtf.file import FileField
from wtforms import BooleanField


class FileForm(FlaskForm):
    file = FileField(_("File"), render_kw={"multiple": True})
    members_only = BooleanField(_("Member only"))
