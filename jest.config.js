const {defaults} = require('jest-config');

module.exports = {
    transform: {
        "^.+\\.vue$": "vue-i18n-jest",
    },
    preset: "ts-jest",
    testEnvironment: "jsdom",
    roots: [
        "<rootDir>/frontend"
    ],
    clearMocks: true,
    globals: {
        Flask: true,
    },
    collectCoverage: true,
    collectCoverageFrom: [
        "frontend/**/*.{js,ts,vue}",
    ],
    coverageReporters: [
        ...defaults.coverageReporters,
        "html"
    ],
    moduleFileExtensions: [
        ...defaults.moduleFileExtensions,
        "js",
        "json",
        // tell Jest to handle `*.vue` files
        "vue"
    ],
};
