import re
from datetime import datetime, timedelta, timezone

import pytest
from sqlalchemy.orm import Session

from app import hashfs
from app.models.activity import Activity
from app.models.user import User
from conftest import CustomClient


def test_create_activity(admin_client, admin_user):
    admin_client.login(admin_user)

    rv = admin_client.get("/activities/create/")
    assert rv.status_code == 200


def test_edit_activity(admin_client, admin_user, activity_factory):
    activity = activity_factory()
    admin_client.login(admin_user)

    rv = admin_client.get(f"/activities/{activity.id}/edit/")
    assert rv.status_code == 200


@pytest.mark.parametrize("activity_price", ["not free", "0", ""])
def test_load_activity(
    activity_price,
    activity_factory,
    db_session,
    admin_client,
):
    activity: Activity = activity_factory()
    activity.price = activity_price
    db_session.commit()

    rv = admin_client.get(f"/activities/{activity.id}")
    assert rv.status_code == 200


def test_load_activity_with_slug(activity_factory, admin_client, admin_user):
    activity: Activity = activity_factory()
    admin_client.login(admin_user)
    rv = admin_client.get(f"/activities/{activity.id}/{activity.to_slug}/")
    assert rv.status_code == 200


def test_load_activity_with_random_slug(activity_factory, admin_client, admin_user):
    activity: Activity = activity_factory()
    admin_client.login(admin_user)
    rv = admin_client.get(f"/activities/{activity.id}/random_slug/")
    assert rv.status_code == 200


def test_activity_picture(
    admin_client: CustomClient, activity_picture, admin_user, activity
):
    admin_client.login(admin_user)
    with admin_client.get(f"/activities/{activity.id}/picture/") as rv:
        assert rv.status_code == 200

        with hashfs.open(activity_picture.hash) as f:
            assert rv.data == f.read()

    with admin_client.get(f"/activities/{activity.id}/picture/thumbnail/") as rv:
        assert rv.status_code == 200


def test_activity_with_pretix_form(
    anonymous_client: CustomClient,
    admin_user,
    activity: Activity,
    db_session,
    requests_mocker,
):
    requests_mocker.get(
        "https://pretix.svia.nl/api/v1/organizers/via/events/someslug/",
        json={
            "testmode": False,
            "plugins": [
                "pretix.plugins.banktransfer",
                "pretix.plugins.reports",
                "pretix.plugins.sendmail",
                "pretix.plugins.statistics",
                "pretix.plugins.ticketoutputpdf",
                "pretix_mollie",
                "pretix_pages",
                "pretix_via",
            ],
        },
    )
    requests_mocker.get(
        "https://pretix.svia.nl/api/v1/organizers/via/events/someslug/settings/",
        json={
            "via_membership_required": True,
            "via_favourer_allowed": True,
        },
    )
    matcher = re.compile(
        r"https://pretix.svia.nl/api/v1/organizers/via"
        r"/events/someslug/via_users/\d+/orders/\?status__in=p%2Cn"
    )
    requests_mocker.get(
        matcher,
        status_code=404,
    )
    anonymous_client.login(admin_user)
    activity.pretix_event_slug = "someslug"
    db_session.commit()

    rv = anonymous_client.get(f"/activities/{activity.id}/")
    assert rv.status_code == 200


@pytest.mark.parametrize("archive", [True, False])
def test_list_activities_anonymous(
    anonymous_client: CustomClient,
    activity: Activity,
    archive: str,
    db_session: Session,
) -> None:
    assert activity.is_in_past()
    activity.pretix_event_slug = "someslug"

    if not archive:
        now = datetime.now(tz=timezone.utc)
        activity.start_time = now + timedelta(days=2)
        activity.end_time = now + timedelta(days=3)
    db_session.commit()

    rv = anonymous_client.get(f"/activities/{'archive' if archive else ''}")
    assert rv.status_code == 200
    if archive:
        assert b"You must be logged in to view this page." in rv.data
    else:
        assert activity.en_name.encode() in rv.data


@pytest.mark.parametrize("archive", [True, False])
def test_list_activities_member(
    anonymous_client: CustomClient,
    activity: Activity,
    archive: bool,
    db_session: Session,
    member_user: User,
) -> None:
    assert activity.is_in_past()
    activity.pretix_event_slug = "someslug"

    if not archive:
        now = datetime.now(tz=timezone.utc)
        activity.start_time = now + timedelta(days=2)
        activity.end_time = now + timedelta(days=3)
    db_session.commit()

    anonymous_client.login(member_user)
    rv = anonymous_client.get(f"/activities/{'archive' if archive else ''}")
    assert rv.status_code == 200
    assert activity.en_name.encode() in rv.data
