def test_create_edit_vue(anonymous_client, admin_user, examination_factory):
    examination = examination_factory()
    anonymous_client.login(admin_user)

    rv = anonymous_client.get("/examination/create")
    assert rv.status_code == 200, rv.data

    rv = anonymous_client.get(f"/examination/{examination.id}/edit")
    assert rv.status_code == 200, rv.data
