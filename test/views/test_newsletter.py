import os

import pytest
from lxml import etree
from sqlalchemy import insert

from app.models.setting_model import Setting


def test_create_edit(anonymous_client, admin_user, newsletter):
    anonymous_client.login(admin_user)

    rv = anonymous_client.get("/newsletter/create")
    assert rv.status_code == 200

    rv = anonymous_client.get(f"/newsletter/{newsletter.id}/edit")
    assert rv.status_code == 200


@pytest.mark.usefixtures("newsletter")
@pytest.mark.parametrize("auth_token", ["", "SOME_TOKEN"])
def test_missing_wrong_auth_token(
    auth_token, anonymous_client, member_user, db_session
):
    db_session.execute(
        insert(Setting).values(
            [
                {
                    Setting.key: "COPERNICA_NEWSLETTER_TOKEN",
                    Setting.value: auth_token + "_CORRECT",
                }
            ]
        )
    )
    db_session.commit()
    anonymous_client.login(member_user)
    rv = anonymous_client.get(
        "/newsletter/latest/committees/",
        query_string={"auth_token": auth_token},
        follow_redirects=False,
    )
    assert rv.status_code == 403


@pytest.mark.parametrize("lang", ["nl", "en"])
@pytest.mark.parametrize(
    "item",
    [
        ("activities", "Nieuwsbrief_2023_04_Activiteiten_Overzicht"),
        ("activities", "Nieuwsbrief_2023_04_Activiteiten"),
        ("news", "Nieuwsbrief_2023_04_Nieuws_Overzicht"),
        ("news", "Nieuwsbrief_2023_04_Nieuws"),
        ("greetings", "Nieuwsbrief_2023_04_Groetjes"),
    ],
)
def test_xslt(lang, item, app, anonymous_client, db_session, newsletter):
    url, filename = item
    db_session.execute(
        insert(Setting).values(
            [
                {
                    Setting.key: "COPERNICA_NEWSLETTER_TOKEN",
                    Setting.value: "SOMETOKEN",
                }
            ]
        )
    )
    db_session.commit()
    rv = anonymous_client.get(
        f"/newsletter/latest/{url}/{lang}/",
        query_string={"auth_token": "SOMETOKEN"},
    )
    assert rv.status_code == 200
    assert rv.mimetype == "text/xml"

    dom = etree.fromstring(rv.data)

    path = os.path.join(
        os.path.abspath(""),
        f"test/fixtures/copernica/{filename}.xml",
    )
    parser = etree.XMLParser(recover=True)
    xslt = etree.parse(path, parser=parser)
    transform = etree.XSLT(xslt)
    result = str(transform(dom))
    news_item = newsletter.news_items[0].news_item
    activity = newsletter.activities[0].activity

    if filename == "Nieuwsbrief_2023_04_Nieuws":
        assert f"/users/{news_item.user.id}/avatar" in result

    if filename in (
        "Nieuwsbrief_2023_04_Nieuws",
        "Nieuwsbrief_2023_04_Activiteiten",
    ):
        assert f"<strong>via</strong> {lang}" in result
        assert '<a href="https://svia.nl">svia.nl</a>' in result
        assert '<img alt="img" src="https://svia.nl/favicon.ico" />' in result
        assert "OVERWRITTEN" not in result

    if filename in (
        "Nieuwsbrief_2023_04_Nieuws",
        "Nieuwsbrief_2023_04_Nieuws_Overzicht",
    ):
        news_item_title = getattr(news_item, f"{lang}_title")

        assert news_item_title in result

    # The description is only in the full text xml, not the overview.
    if filename == "Nieuwsbrief_2023_04_Nieuws":
        assert "OVERWRITTEN" not in result
        assert f"OVERWRITE NEWS {lang}" in result

    if filename in (
        "Nieuwsbrief_2023_04_Activiteiten",
        "Nieuwsbrief_2023_04_Activiteiten_Overzicht",
    ):
        activity_title = getattr(activity, f"{lang.lower()}_name")
        assert activity_title in result

    # The description is only in the full text xml, not the overview.
    if filename == "Nieuwsbrief_2023_04_Activiteiten":
        assert "OVERWRITTEN" not in result
        assert f"OVERWRITE ACTIVITY {lang}" in result
