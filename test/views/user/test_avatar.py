from datetime import date

import pytest
from werkzeug.datastructures import FileStorage

from app.models.user import User
from app.service import user_service


@pytest.fixture
def avatar_file_storage():
    f = open("./app/static/img/user.png", "rb")
    fs = FileStorage(f, "avatar.png")
    yield fs
    f.close()


def test_anonymous_random_user_without_avatar(
    anonymous_client, user_factory, db_session
):
    user = user_factory()
    db_session.commit()

    rv = anonymous_client.get(f"/users/{user.id}/avatar/", follow_redirects=False)
    assert rv.status_code == 302
    assert rv.location.startswith("https://www.gravatar.com/avatar")


def test_member_random_user_without_avatar(
    anonymous_client, admin_user, member_user, db_session
):
    admin_user.email = "john.doe@gmail.com"
    db_session.commit()
    anonymous_client.login(member_user)

    rv = anonymous_client.get(f"/users/{admin_user.id}/avatar/", follow_redirects=False)

    assert rv.status_code == 302
    assert (
        rv.location == "https://www.gravatar.com/avatar/"
        "e13743a7f1db7f4246badd6fd6ff54ff?d=identicon&s=100"
    )


def test_admin_random_user_without_avatar(anonymous_client, admin_user, db_session):
    admin_user.email = "john.doe@gmail.com"
    db_session.commit()
    anonymous_client.login(admin_user)

    rv = anonymous_client.get(f"/users/{admin_user.id}/avatar/", follow_redirects=False)

    assert rv.status_code == 302
    assert (
        rv.location == "https://www.gravatar.com/avatar/"
        "e13743a7f1db7f4246badd6fd6ff54ff?d=identicon&s=100"
    )


# 200 -> serves avatar
# 302 -> serves gravatar redirect
# 404 -> no such thing
#
#             avatar of: | member | self | news_item_owner | member of merit |
# | request by admin     |   200  |  200 |      200        |      200        |
# | request by member    |   302  |  200 |      200        |      200        |
# | request by anonymous |   302  |  302 |      200        |      200        |


def test_anonymous_random_user_with_avatar(
    anonymous_client, admin_user, avatar_file_storage, db_session
):
    """Avatars of random users should display gravatar to anonymous users."""
    user_service.set_avatar(admin_user.id, avatar_file_storage)
    db_session.commit()

    rv = anonymous_client.get(f"/users/{admin_user.id}/avatar/", follow_redirects=False)
    assert rv.status_code == 302
    assert rv.location.startswith("https://www.gravatar.com/avatar")


def test_anonymous_news_item_owner(
    anonymous_client, news_factory, admin_user, avatar_file_storage, db_session
):
    news = news_factory()
    news.user = admin_user
    db_session.commit()
    user_service.set_avatar(admin_user.id, avatar_file_storage)
    db_session.commit()

    with anonymous_client.get(
        f"/users/{admin_user.id}/avatar/", follow_redirects=False
    ) as rv:
        assert rv.status_code == 200
        assert rv.content_type == "image/png"


def test_anonymous_self_avatar(anonymous_client):
    rv = anonymous_client.get("/users/self/avatar/", follow_redirects=False)
    assert rv.status_code == 302
    assert rv.location.startswith("https://www.gravatar.com/avatar")


def test_admin_random_user(
    anonymous_client, admin_user, avatar_file_storage, db_session
):
    """Avatars of random users should not be displayed to anonymous users."""

    anonymous_client.login(admin_user)

    user_service.set_avatar(admin_user.id, avatar_file_storage)
    db_session.commit()

    with anonymous_client.get(
        f"/users/{admin_user.id}/avatar/", follow_redirects=False
    ) as rv:
        assert rv.status_code == 200
        assert rv.content_type == "image/png"


def test_admin_self(anonymous_client, admin_user, avatar_file_storage, db_session):
    """Avatars of random users should not be displayed to anonymous users."""

    anonymous_client.login(admin_user)

    user_service.set_avatar(admin_user.id, avatar_file_storage)
    db_session.commit()

    with anonymous_client.get("/users/self/avatar/", follow_redirects=False) as rv:
        rv.close()


def test_admin_news_item_owner(
    anonymous_client,
    news_factory,
    admin_user,
    member_user,
    avatar_file_storage,
    db_session,
):
    news = news_factory()
    news.user = member_user
    db_session.commit()
    user_service.set_avatar(member_user.id, avatar_file_storage)
    db_session.commit()
    anonymous_client.login(admin_user)
    with anonymous_client.get(
        f"/users/{member_user.id}/avatar/", follow_redirects=False
    ) as rv:
        assert rv.status_code == 200
        assert rv.content_type == "image/png"


def test_member_random_user(
    anonymous_client, member_user, admin_user, avatar_file_storage, db_session
):
    """Avatars of random users should not be displayed to anonymous users."""

    anonymous_client.login(member_user)

    user_service.set_avatar(admin_user.id, avatar_file_storage)
    db_session.commit()

    rv = anonymous_client.get(f"/users/{admin_user.id}/avatar/", follow_redirects=False)
    assert rv.status_code == 302
    assert rv.location.startswith("https://www.gravatar.com/avatar")


def test_member_self(anonymous_client, member_user, avatar_file_storage, db_session):
    """Avatars of random users should not be displayed to anonymous users."""

    anonymous_client.login(member_user)

    user_service.set_avatar(member_user.id, avatar_file_storage)
    db_session.commit()

    with anonymous_client.get("/users/self/avatar/", follow_redirects=False) as rv:
        assert rv.status_code == 200
        assert rv.content_type == "image/png"


def test_member_news_item_owner(
    anonymous_client,
    news_factory,
    admin_user,
    member_user,
    avatar_file_storage,
    db_session,
):
    news = news_factory()
    news.user = admin_user
    db_session.commit()
    user_service.set_avatar(admin_user.id, avatar_file_storage)
    db_session.commit()
    anonymous_client.login(member_user)
    with anonymous_client.get(
        f"/users/{admin_user.id}/avatar/", follow_redirects=False
    ) as rv:
        assert rv.status_code == 200
        assert rv.content_type == "image/png"


def test_member_of_merit_avatar(
    anonymous_client,
    admin_user: User,
    member_user,
    avatar_file_storage,
    db_session,
):
    user_service.set_avatar(admin_user.id, avatar_file_storage)
    admin_user.member_of_merit_date = date.today()
    db_session.commit()
    anonymous_client.login(member_user)
    with anonymous_client.get(
        f"/users/{admin_user.id}/avatar/", follow_redirects=False
    ) as rv:
        assert rv.status_code == 200
        assert rv.content_type == "image/png"
