import os


def read_datafile(filename):
    with open(os.path.join(os.path.dirname(__file__), filename)) as f:
        return f.read()
