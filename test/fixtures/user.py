from datetime import datetime

import bcrypt
import faker
import pyotp
import pytest

from app.models.group import UserGroup
from app.models.user import User
from app.service import user_tfa_service


@pytest.fixture(scope="session")
def password() -> dict[str, str]:
    pw = faker.Faker().password(length=12)
    return {
        "raw": pw,
        "hash": bcrypt.hashpw(pw.encode("utf-8"), bcrypt.gensalt()).decode("utf-8"),
    }


@pytest.fixture
def admin_user(user_factory, admin_group, db_session, password):
    user: User = user_factory()
    user.password = password["hash"]
    user.totp_secret = pyotp.random_base32()
    user.locale = "en"
    user.secret_gen_time = datetime.now()
    opt = pyotp.TOTP(user.totp_secret).now()
    db_session.commit()

    user_tfa_service.enable_two_factor_authentication(user.id, user.totp_secret, opt)
    user.paid_date = datetime.now()
    db_session.flush()
    db_session.add(UserGroup(user_id=user.id, group_id=admin_group.id))
    db_session.commit()
    return user


@pytest.fixture
def member_user(user_factory, db_session, password):
    user: User = user_factory()
    user.password = password["hash"]
    user.paid_date = datetime.now()
    db_session.commit()
    return user


@pytest.fixture
def unpaid_user(user_factory, db_session, password):
    user: User = user_factory()
    user.password = password["hash"]
    user.paid_date = None
    db_session.commit()
    return user
