import pydantic
import pytest
from sqlalchemy import delete, insert
from sqlalchemy.orm import Session

from app.models.setting_model import Setting
from app.service.setting_service import DatabaseSettingsMixin
from app.task.user import UserEducationSetting


def test_datanose_settings(db_session):
    s = UserEducationSetting(db_session=db_session)
    assert s.user_education_updates_enabled is True, "default not present"
    assert isinstance(s.__config__.db_session, Session)

    setting = Setting(key="user_education_updates_enabled", value="False")
    db_session.add(setting)
    db_session.commit()
    assert (
        UserEducationSetting(db_session=db_session).user_education_updates_enabled
        is False
    )

    setting.value = "True"
    db_session.commit()
    assert (
        UserEducationSetting(db_session=db_session).user_education_updates_enabled
        is True
    )

    setting.value = "0"
    db_session.commit()
    assert (
        UserEducationSetting(db_session=db_session).user_education_updates_enabled
        is False
    )

    setting.value = "false"
    db_session.commit()
    assert (
        UserEducationSetting(db_session=db_session).user_education_updates_enabled
        is False
    )

    setting.value = "true"
    db_session.commit()
    assert (
        UserEducationSetting(db_session=db_session).user_education_updates_enabled
        is True
    )

    setting.value = "akjsdflkjasdf"
    db_session.commit()
    with pytest.raises(pydantic.ValidationError):
        assert (
            UserEducationSetting(db_session=db_session).user_education_updates_enabled
            is False
        )


def test_database_settings_mixin_case_sensitivity(db_session):
    class TestSetting(DatabaseSettingsMixin):
        UPPER: str
        Mixed: str
        lower: str

    db_session.execute(
        insert(Setting).values(
            [
                {"key": "upper", "value": "value"},
                {"key": "mixed", "value": "value"},
                {"key": "lower", "value": "value"},
            ]
        )
    )
    db_session.commit()

    obj = TestSetting(db_session=db_session)
    assert obj.UPPER == "value"
    assert obj.Mixed == "value"
    assert obj.lower == "value"

    db_session.execute(
        delete(Setting).where(Setting.key.in_(("upper", "mixed", "lower")))
    )
    db_session.commit()
    db_session.execute(
        insert(Setting).values(
            [
                {"key": "UPPER", "value": "VALUE"},
                {"key": "MIXED", "value": "VALUE"},
                {"key": "LOWER", "value": "VALUE"},
            ]
        )
    )
    db_session.commit()

    obj = TestSetting(db_session=db_session)
    assert obj.UPPER == "VALUE"
    assert obj.Mixed == "VALUE"
    assert obj.lower == "VALUE"
