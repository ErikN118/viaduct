from app.models.redirect import Redirect
from app.service import redirect_service

redirects = [
    Redirect(fro="a", to="b"),
    Redirect(fro="c", to="b"),
    Redirect(fro="b", to="d"),
    Redirect(fro="a", to="c"),
    Redirect(fro="d", to="c"),
]


def test_is_a_redirect_chain():
    assert redirect_service.is_a_redirect_chain(redirects, "a") is True
    assert redirect_service.is_a_redirect_chain(redirects, "b") is True
    assert redirect_service.is_a_redirect_chain(redirects, "c") is True
    assert redirect_service.is_a_redirect_chain(redirects, "f") is False
