from flask import Flask
from flask_wtf.csrf import generate_csrf
from sqlalchemy import inspect

from app.models.user import User


def test_create_db(requests_mocker, app, db, db_session, anonymous_client):
    runner = app.test_cli_runner()
    result = runner.invoke(args=["dropdb"], catch_exceptions=False)
    assert result.exit_code == 0, result.output

    with app.app_context():
        tables = inspect(db.engine).get_table_names()
        # The dropped tables are based on available metadata, alembic_version
        # is not in the metadata. However createdb may leave it from previous
        # runs.
        assert tables in ([], ["alembic_version"])

    create_input = [
        "john.doe@student.uva.nl",
        "password",
        "password",  # confirm
        "Maico",
        "Timmerman",
    ]

    # Mocks for course_available when adding examinations in cli.py.
    requests_mocker.get(
        "https://api.datanose.nl/Courses/5204DLNL6Y",
        json=[
            {
                "NameDutch": "Deep Learning for Natural Language Processing",
                "NameEnglish": "Deep Learning for Natural Language Processing",
                "EC": 6,
                "CatalogNumber": "5204DLNL6Y",
                "AcademicYear": 2020,
                "SISCourseID": 175676,
                "Owner": "MSc AI",
            }
        ],
    )

    result = runner.invoke(
        args=["createdb"], catch_exceptions=False, input="\n".join(create_input)
    )
    assert result.exit_code == 0, result.output

    table_names = set(inspect(db.engine).get_table_names())
    # The "alembic_version" is not in the metadata.
    assert set(db.metadata.tables).issubset(table_names)

    user = db_session.query(User).one()
    assert user.first_name == "Maico"
    assert user.last_name == "Timmerman"

    rv = anonymous_client.get("/sign-in/")
    assert rv.status_code == 200

    rv = anonymous_client.post(
        "/sign-in/",
        data={
            "email": user.email,
            # Raw password is only set in the pytest fixtures.
            "password": "password",
            "csrf_token": generate_csrf(),
        },
    )
    assert rv.status_code == 200

    # Test re-runability.
    result = runner.invoke(
        args=["createdb"], catch_exceptions=False, input="\n".join(create_input)
    )
    assert result.exit_code == 0, result.output


def test_createdb_skip_exists(app: Flask) -> None:
    create_input = [
        "john.doe@student.uva.nl",
        "password",
        "password",  # confirm
        "Maico",
        "Timmerman",
    ]
    runner = app.test_cli_runner()
    result = runner.invoke(
        args=["createdb", "--skip-exists"],
        catch_exceptions=False,
        input="\n".join(create_input),
    )
    assert result.exit_code == 0, result.output
