import urllib.parse

import pytest


@pytest.mark.parametrize(
    "project_id,project",
    [
        (4110282, "viaduct"),
        (6644893, "pos"),
        (14120498, "pretix"),
    ],
)
def test_create_gitlab_issue(
    unpaid_user_client, unpaid_user, project_id, project, requests_mocker
):
    requests_mocker.post(
        f"https://gitlab.com/api/v4/projects/{project_id}/issues",
        json={
            # TODO easter egg: new issue that mentions that ICT is looking for members?
            "web_url": "https://gitlab.com/studiverenigingvia/viaduct/issues/1035",
        },
    )
    rv = unpaid_user_client.post(
        "/api/bugs/",
        json={"project": project, "title": "title", "description": "description"},
    )
    assert rv.status_code == 200, rv.json
    assert (
        rv.json["web_url"]
        == "https://gitlab.com/studiverenigingvia/viaduct/issues/1035"
    )

    query = urllib.parse.parse_qs(requests_mocker.last_request.query)
    assert query["title"][0] == "title"
    assert f"/users/{unpaid_user.id}/view" in query["description"][0]
    assert "description" in query["description"][0]


def test_create_gitlab_issue_anonymous(anonymous_client):
    rv = anonymous_client.post(
        "/api/bugs/",
        json={"project": "pos", "title": "title", "description": "description"},
    )
    assert rv.status_code == 401


# TODO
@pytest.mark.xfail(
    strict=True, reason="check request.get_json result when argument to pydantic"
)
def test_send_list_json(unpaid_user_client):
    rv = unpaid_user_client.post(
        "/api/bugs/",
        json=[{"project": "1", "title": "title", "description": "description"}],
    )
    assert rv.status_code == 200
