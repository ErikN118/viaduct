import json
from datetime import date

from app.models.education import Education
from app.models.user import User
from app.repository import user_repository


def test_user_details_admin_id(user_factory, admin_client):
    user = user_factory()
    rv = admin_client.get(f"/api/users/{user.id}")
    assert rv.status_code == 200
    data = json.loads(rv.data)
    assert type(data["disabled"]) == bool


def test_user_details_user_id(member_user, member_client):
    rv = member_client.get(f"/api/users/{member_user.id}")
    assert rv.status_code == 403


def test_user_details_user_self(member_client):
    rv = member_client.get("/api/users/self")
    assert rv.status_code == 200


def test_user_educations_self(
    admin_client, db_session, education_factory, admin_user: User
):
    e1: Education = education_factory()
    # Make sure both education have same type, so they do not get filtered
    user_repository.set_user_education(
        db_session,
        admin_user,
        [
            e1,
            education_factory(programme_type=e1.programme_type.value),
        ],
    )
    db_session.commit()
    rv = admin_client.get("/api/users/self/educations/")
    assert rv.status_code == 200
    assert len(rv.json) == 2


def test_user_groups_admin_id(admin_client, member_user):
    rv = admin_client.get(f"/api/users/{member_user.id}/groups")
    assert rv.status_code == 200


def test_user_groups_user_id(member_client, member_user):
    rv = member_client.get(f"/api/users/{member_user.id}/groups")
    assert rv.status_code == 403


def test_user_groups_user_self(member_client):
    rv = member_client.get("/api/users/self/groups")
    assert rv.status_code == 200


def test_user_applications_admin_id(admin_client, member_user):
    rv = admin_client.get(f"/api/users/{member_user.id}/applications")
    assert rv.status_code == 200


def test_user_applications_user_id(member_client, member_user):
    rv = member_client.get(f"/api/users/{member_user.id}/applications")
    assert rv.status_code == 403


def test_user_applications_user_self(member_client):
    rv = member_client.get("/api/users/self/applications")
    assert rv.status_code == 200


def test_update_user_details_as_member(member_client, member_user, db):
    rv = member_client.patch(
        "/api/users/self/",
        json={
            "first_name": "Some_new_first_name",
            "last_name": "Some_new_last_name",
            "phone_nr": "06-somenumber",
        },
    )
    assert rv.status_code == 200

    u = db.session.get(User, member_user.id)
    assert u.first_name == "Some_new_first_name"
    assert u.last_name == "Some_new_last_name"
    assert u.phone_nr == "06-somenumber"

    rv = member_client.patch("/api/users/self/", json={"member": True})
    assert rv.status_code == 403
    rv = member_client.patch("/api/users/self/", json={"disabled": True})
    assert rv.status_code == 403
    rv = member_client.patch("/api/users/self/", json={"member_of_merit_date": None})
    assert rv.status_code == 403
    rv = member_client.patch("/api/users/self/", json={"favourer": True})
    assert rv.status_code == 403


def test_update_member_of_merit(db_session, admin_client, admin_user):
    assert admin_user.member_of_merit_date is None

    rv = admin_client.patch(
        "/api/users/self/", json={"member_of_merit_date": "2020-04-21"}
    )
    assert rv.status_code == 200, rv.json
    db_session.refresh(admin_user)
    assert admin_user.member_of_merit_date is not None

    # Update without member_of_merit, checks that it is not wiped.
    rv = admin_client.patch("/api/users/self/", json={})
    assert rv.status_code == 200, rv.json
    db_session.refresh(admin_user)
    assert admin_user.member_of_merit_date is not None

    rv = admin_client.patch("/api/users/self/", json={"member_of_merit_date": None})
    assert rv.status_code == 200, rv.json
    db_session.refresh(admin_user)
    assert admin_user.member_of_merit_date is None


def test_update_user_details_length_validation(member_client, member_user, db):
    rv = member_client.patch("/api/users/self/", json={"first_name": "a" * 255})
    assert rv.status_code == 200
    rv = member_client.patch("/api/users/self/", json={"first_name": "a" * 256})
    assert rv.status_code == 200
    rv = member_client.patch("/api/users/self/", json={"first_name": "a" * 257})
    assert rv.status_code == 400

    u = db.session.get(User, member_user.id)
    assert u.first_name == "a" * 256


def test_member_of_merit_list(db_session, anonymous_client, member_user: User):
    member_user.member_of_merit_date = date.today()
    db_session.commit()

    rv = anonymous_client.get("/api/users/merit/")
    assert rv.status_code == 200, rv.json
    assert len(rv.json) == 1, rv.json
    assert set(rv.json[0].keys()) == {
        "id",
        "first_name",
        "last_name",
        "member_of_merit_date",
        "path",
    }, rv.json


def test_admin_make_user_member(db_session, admin_client, unpaid_user):
    rv = admin_client.patch(f"/api/users/{unpaid_user.id}/", json={"member": True})
    assert rv.status_code == 200, rv.json
    db_session.refresh(unpaid_user)
    assert unpaid_user.has_paid
    assert not unpaid_user.favourer


def test_admin_make_user_favourer(db_session, admin_client, unpaid_user):
    rv = admin_client.patch(f"/api/users/{unpaid_user.id}/", json={"favourer": True})
    assert rv.status_code == 200, rv.json
    db_session.refresh(unpaid_user)
    assert unpaid_user.favourer
    assert not unpaid_user.has_paid


def test_admin_make_user_favourer_and_member(db_session, admin_client, unpaid_user):
    rv = admin_client.patch(
        f"/api/users/{unpaid_user.id}/", json={"member": True, "favourer": True}
    )
    assert rv.status_code == 400, rv.json
    assert rv.json == {
        "code": 400,
        "data": {
            "details": "Schema validation failed.",
            "errors": {
                "favourer": "Member and favourer are mutually exclusive",
                "member": "Member and favourer are mutually exclusive",
            },
        },
        "detail": "The request was not according to semantic rules. Schema validation "
        "failed.",
        "title": "Validation Error",
        "type": "about:blank",
    }

    db_session.refresh(unpaid_user)
    assert not unpaid_user.has_paid
    assert not unpaid_user.favourer
