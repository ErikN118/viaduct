from collections.abc import Callable
from io import BytesIO

from app.models.course import Course
from conftest import CustomClient


def test_examination_course_not_available(
    admin_client, examination_factory, course_factory, requests_mocker
):
    course = course_factory()
    examination = examination_factory()

    requests_mocker.get(
        f"https://api.datanose.nl/Courses/{course.datanose_code}",
        json=[
            {
                "NameDutch": "Course 1",
                "NameEnglish": "Course 1",
                "EC": 1,
                "CatalogNumber": "DNTESTCODE",
                "AcademicYear": 2019,
                "SISCourseID": 1,
                "CatalogCourseURL": "http://studiegids.uva.nl/",
                "Owner": "BSc IN",
            }
        ],
    )

    for method, path in [
        (admin_client.post, "/api/examinations/"),
        (admin_client.put, f"/api/examinations/{examination.id}/"),
    ]:
        rv = method(
            path,
            json={
                "date": "2018-09-06",
                "course_id": course.id,
                "test_type": "End-term",
                "comment": "",
            },
        )
        assert rv.status_code == 409, rv.json
        assert "unavailable on 2018-09-06" in rv.json["data"]["details"]


def test_create_exam(
    admin_client, admin_user, anonymous_client, course_factory, requests_mocker
):
    course = course_factory()
    anonymous_client.login(admin_user)

    requests_mocker.get(
        f"https://api.datanose.nl/Courses/{course.datanose_code}",
        json=[
            {
                "NameDutch": "Course 1",
                "NameEnglish": "Course 1",
                "EC": 1,
                "CatalogNumber": "DNTESTCODE",
                "AcademicYear": 2019,
                "SISCourseID": 1,
                "CatalogCourseURL": "http://studiegids.uva.nl/",
                "Owner": "BSc IN",
            }
        ],
    )

    # Test academic year wrapping.
    for date_str in ["2019-09-06", "2020-03-06"]:
        rv = admin_client.post(
            "/api/examinations/",
            json={
                "date": date_str,
                "course_id": course.id,
                "test_type": "End-term",
                "comment": "",
            },
        )
        assert rv.status_code == 201, rv.json

    exam_id = rv.json["id"]
    # Upload files twice, to also test deleting of old files.
    for i in range(2):
        for file_type in ["answers", "exam"]:
            rv = admin_client.post(
                f"/api/examinations/{exam_id}/{file_type}/",
                data={
                    "file": (
                        BytesIO(f"file contents {i}".encode()),
                        "test.pdf",
                    )
                },
            )
            assert rv.status_code == 200, rv.data

            with anonymous_client.get(
                f"/examination/view/{exam_id}/{file_type}/"
            ) as rv:
                assert rv.status_code == 200, rv.data
                assert rv.data == f"file contents {i}".encode(), rv.data


def test_update_exam(
    admin_client, examination_factory, course_factory, requests_mocker
):
    course = course_factory()
    examination = examination_factory()

    requests_mocker.get(
        f"https://api.datanose.nl/Courses/{course.datanose_code}",
        json=[
            {
                "NameDutch": "Course 1",
                "NameEnglish": "Course 1",
                "EC": 1,
                "CatalogNumber": "DNTESTCODE",
                "AcademicYear": 2019,
                "SISCourseID": 1,
                "CatalogCourseURL": "http://studiegids.uva.nl/",
                "Owner": "BSc IN",
            }
        ],
    )

    # Test academic year wrapping.
    for date_str in ["2019-09-06", "2020-03-06"]:
        rv = admin_client.put(
            f"/api/examinations/{examination.id}/",
            json={
                "date": date_str,
                "course_id": course.id,
                "test_type": "End-term",
                "comment": "",
            },
        )
        assert rv.status_code == 200, rv.json


def test_list_examinations_of_course(
    member_client: CustomClient, course_factory: Callable[[], Course]
) -> None:
    course = course_factory()
    rv = member_client.get(f"/api/courses/{course.id}/examinations/")
    assert rv.status_code == 200
