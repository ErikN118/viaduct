from app.models.activity import Activity
from app.models.news import News


def test_home(db, anonymous_client, news_factory, activity_factory):
    news_factory.create_batch(10)
    assert db.session.query(News).count() == 10

    activity_factory.create_batch(10)
    assert db.session.query(Activity).count() == 10

    rv = anonymous_client.get("/")
    assert rv.status_code == 200
