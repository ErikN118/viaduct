from datetime import date

import pytest

from app.models.news import News


@pytest.fixture
def news_item(news_factory):
    return news_factory()


def test_news_list(admin_client, news_item):
    rv = admin_client.get("/api/news")
    assert rv.status_code == 200
    assert rv.json["data"][0]["id"] == news_item.id


def test_news_get(admin_client, news_item):
    rv = admin_client.get(f"/api/news/{news_item.id}")
    assert rv.status_code == 200
    assert rv.json["id"] == news_item.id


def test_news_update(db, admin_client, news_item):
    rv = admin_client.put(
        f"/api/news/{news_item.id}/",
        json={
            "title": {"en": "en_title", "nl": "nl_title"},
            "content": {"en": "en_content", "nl": "nl_content"},
            "publish_date": date.today().isoformat(),
        },
    )
    assert rv.status_code == 200
    news = db.session.get(News, news_item.id)
    assert news.nl_title == "nl_title"
    assert news.en_title == "en_title"
    assert news.nl_content == "nl_content"
    assert news.en_content == "en_content"
