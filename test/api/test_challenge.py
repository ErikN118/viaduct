from datetime import date, timedelta

import pytest
from sqlalchemy import func, select
from sqlalchemy.orm import Session

from app.models.challenge import Challenge, Competitor, Submission


@pytest.fixture
def competitor(db_session, admin_user):
    c = Competitor()
    c.user = admin_user
    c.point = 10
    db_session.add(c)
    db_session.commit()
    return c


@pytest.fixture
def challenge(db_session):
    c = Challenge()
    c.name = "name"
    c.description = "description"
    c.hint = "hint"
    c.start_date = date.today()
    c.end_date = date.today() + timedelta(days=1)
    c.weight = 10
    c.type = "Text"
    c.answer = "answer"
    db_session.add(c)
    db_session.commit()
    return c


@pytest.fixture
def submission(db_session, admin_user, challenge):
    s = Submission()
    s.challenge_id = challenge.id
    s.user = admin_user
    s.answer = "submission"
    s.approved = True
    db_session.add(s)
    db_session.commit()
    return s


@pytest.mark.usefixtures("challenge", "submission", "competitor")
def test_wipe_ranking(db_session: Session, admin_client):
    stmt1 = select(func.count(Challenge.id))
    stmt2 = select(func.count(Submission.id))
    stmt3 = select(func.count(Competitor.id))

    assert db_session.execute(stmt1).scalar() == 1
    assert db_session.execute(stmt2).scalar() == 1
    assert db_session.execute(stmt3).scalar() == 1

    rv = admin_client.delete("/api/challenges/ranking/")
    assert rv.status_code == 204

    assert db_session.execute(stmt1).scalar() == 1
    assert db_session.execute(stmt2).scalar() == 0
    assert db_session.execute(stmt3).scalar() == 0, ""
