import json
from datetime import datetime, timedelta, timezone
from unittest.mock import patch

from app.models.group import Group, UserGroupHistory
from app.utils import google


def test_group_members(admin_client, admin_group, admin_user):
    rv = admin_client.get(f"/api/groups/{admin_group.id}/users/")
    assert rv.status_code == 200
    data = json.loads(rv.data)
    assert admin_user.email in data["data"][0]["email"]


def test_group_members_history(db, admin_client, admin_group, admin_user):
    rv = admin_client.delete(
        f"/api/groups/{admin_group.id}/users/", json={"user_ids": [admin_user.id]}
    )
    assert rv.status_code == 204
    # admin_user was the only user of admin_group, it should now be empty.
    assert not admin_group.users.all()
    history = (
        db.session.query(UserGroupHistory)
        .filter(UserGroupHistory.group_id == admin_group.id)
        .first()
    )
    assert history.user_id == admin_user.id
    assert history.deleted - datetime.now(timezone.utc) < timedelta(minutes=1)


def test_create_group_without_maillist(admin_client):
    rv = admin_client.post("/api/groups/", json=dict(mailtype="none", name="name"))
    assert rv.status_code == 201


@patch.object(google, "create_group_if_not_exists")
def test_create_group_with_maillist(mock_method, admin_client):
    rv = admin_client.post(
        "/api/groups/",
        json=dict(mailtype="mailinglist", name="name", maillist="maillist"),
    )
    assert rv.status_code == 201
    mock_method.assert_called_once()


def test_group_delete_without_maillist(admin_client, db_session):
    g = Group(name="name", mailtype="none")
    db_session.add(g)
    db_session.commit()
    rv = admin_client.delete(f"/api/groups/{g.id}")
    assert rv.status_code == 204
    assert (
        not db_session.query(Group)
        .filter(Group.id == g.id)
        .filter(Group.deleted_at.is_(None))
        .first()
    )


@patch.object(google, "delete_group")
@patch.object(google, "create_group_if_not_exists")
def test_group_with_maillist(mock_method, mock_method2, admin_client):
    rv = admin_client.post(
        "/api/groups/",
        json=dict(mailtype="mailinglist", name="name2", maillist="maillist2"),
    )
    assert rv.status_code == 201
    mock_method.assert_called_once()
    rv = admin_client.delete(f"/api/groups/{rv.json['id']}")
    assert rv.status_code == 204
    mock_method2.assert_called_once()
