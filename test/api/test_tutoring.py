from unittest import mock

import pytest

from app.models.tutoring import Tutor, Tutoring


@pytest.fixture
def tutoring(db_session, admin_user, course_factory):
    course = course_factory()
    t = Tutoring()
    t.course = course
    t.tutor = admin_user
    t.tutee = admin_user
    t.num_hours = 3
    db_session.add(t)
    db_session.commit()
    return t


@pytest.fixture
def tutor(db_session, admin_user, course_factory):
    t = Tutor()
    t.course = course_factory()
    t.user = admin_user
    t.grade = 80
    db_session.add(t)
    db_session.commit()
    return t


@pytest.mark.usefixtures("tutor")
def test_list_tutors(admin_client, admin_user):
    rv = admin_client.get("/api/tutors/")
    assert rv.status_code == 200, rv.json
    assert all(r["user"]["id"] == admin_user.id for r in rv.json["data"]), rv.json


@pytest.mark.usefixtures("tutor")
def test_list_tutor_courses(admin_client, admin_user):
    rv = admin_client.get("/api/tutors/self/courses/")
    assert rv.status_code == 200, rv.json
    assert all(r["user"]["id"] == admin_user.id for r in rv.json), rv.json


@pytest.mark.usefixtures("tutoring")
def test_list_tutorings(admin_client, admin_user):
    rv = admin_client.get("/api/tutorings/")
    assert rv.status_code == 200, rv.json
    assert all(r["tutee"]["email"] == admin_user.email for r in rv.json), rv.json


@pytest.mark.usefixtures("tutoring")
def test_get_tutorings_for_tutee_user(admin_client, admin_user):
    rv = admin_client.get("/api/users/self/tutoring/")
    assert rv.status_code == 200, rv.json
    assert all(r["tutee"]["email"] == admin_user.email for r in rv.json), rv.json


@pytest.mark.usefixtures("tutoring")
def test_get_tutorings_for_tutor_user(admin_client, admin_user):
    rv = admin_client.get("/api/tutors/self/tutorings/")
    assert rv.status_code == 200, rv.json
    assert all(r["tutor"]["email"] == admin_user.email for r in rv.json), rv.json


@mock.patch("app.service.mail_service.send_mail_task.delay")
def test_request_tutoring(mock, admin_client, course_factory):
    rv = admin_client.post(
        "/api/users/self/tutoring/",
        json={"course_id": course_factory().id, "num_hours": 0, "notes": "imstupid"},
    )
    assert rv.status_code == 200


@mock.patch("app.service.mail_service.send_mail_task.delay")
def test_register_as_tutor_for_courses(mock, admin_client, course_factory):
    rv = admin_client.post(
        "/api/tutors/self/courses/",
        json={
            "courses": [
                {
                    "grade": 80,
                    "course_id": course_factory().id,
                },
                {
                    "grade": 75,
                    "course_id": course_factory().id,
                },
            ],
        },
    )
    assert rv.status_code == 201, rv.json
