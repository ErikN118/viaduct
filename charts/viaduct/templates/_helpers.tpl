{{/*
Expand the name of the chart.
*/}}
{{- define "viaduct.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "viaduct.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "viaduct.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "viaduct.labels" -}}
helm.sh/chart: {{ include "viaduct.chart" . }}
{{ include "viaduct.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "viaduct.selectorLabels" -}}
app.kubernetes.io/name: {{ include "viaduct.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "viaduct.databaseDsn" -}}
postgresql+psycopg2://{{ .Values.postgresql.auth.username }}:{{ .Values.postgresql.auth.password }}@{{ (include "viaduct.fullname" .) }}-postgresql/{{ .Values.postgresql.auth.database }}
{{- end -}}

{{- define "viaduct.brokerDsn" -}}
redis://:{{ .Values.redis.auth.password }}@{{ (include "viaduct.fullname" .) }}-redis-master
{{- end -}}

{{/*
Create the environment variables used by the Python app
*/}}
{{- define "viaduct.appEnvironment" }}
  - name: SQLALCHEMY_DATABASE_URI
    value: {{ include "viaduct.databaseDsn" . }}
  - name: POSTGRES_HOST
    value: {{ (include "viaduct.fullname" .) }}-postgresql
  - name: POSTGRES_USER
    value: {{ .Values.postgresql.auth.username }}
  - name: POSTGRES_PASS
    value: {{ .Values.postgresql.auth.password }}
  - name: POSTGRES_DB
    value: {{ .Values.postgresql.auth.database }}
  - name: BROKER_URL
    value: {{ include "viaduct.brokerDsn" . }}
{{ if .Values.viaduct.googleApiKeyExistingSecret }}
  - name: GOOGLE_API_KEY_FILE
    value: "/secrets/google/api-key.json"
{{ end }}
{{ if .Values.viaduct.samlCertsExistingSecret }}
  - name: SAML_PATH
    value: "/secrets/saml/"
{{ end }}
{{- end }}


{{/*
App volumes and volumeMounts for secret files (api keys).
*/}}
{{- define "viaduct.volumeMounts" -}}
{{- if .Values.viaduct.googleApiKeyExistingSecret }}
  - name: google-api-key-file
    mountPath: /secrets/google/
    readOnly: true
{{- end }}
{{- if .Values.viaduct.samlCertsExistingSecret }}
  - name: saml-certs
    mountPath: /secrets/saml/
    readOnly: true
{{- end }}
{{- end }}

{{- define "viaduct.volumes" -}}
{{- if .Values.viaduct.googleApiKeyExistingSecret }}
  - name: google-api-key-file
    secret:
      secretName: {{ .Values.viaduct.googleApiKeyExistingSecret }}
      items:
        - key: google-api-key
          path: "api-key.json"
{{- end }}
{{- if .Values.viaduct.samlCertsExistingSecret }}
  - name: saml-certs
    secret:
      secretName: {{ .Values.viaduct.samlCertsExistingSecret }}
{{- end }}
{{- end }}
