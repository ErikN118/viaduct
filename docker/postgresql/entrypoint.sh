#!/bin/bash

set -euo pipefail

function load_extensions() {
  echo "Waiting for PostgreSQL to start"
  while ! psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" 2>/dev/null; do
      sleep 1
  done

  /docker-entrypoint-initdb.d/load-extensions.sh
}

# Call image entrypoint
/docker-entrypoint.sh "$@" &
postgresql_pid=$!


# If already initialized, load the extensions
if [ -s "$PGDATA/PG_VERSION" ]; then
  load_extensions &
fi

# If not initialized, let the image entrypoint initialize the database
# and call load-extensions.sh

trap 'kill -INT $postgresql_pid; exit 0' SIGINT
trap 'kill -TERM $postgresql_pid; exit 0' SIGTERM
wait "$postgresql_pid"
