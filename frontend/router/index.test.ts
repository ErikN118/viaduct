import { routes } from "./index";

describe("Test router routes", function () {
    test("Check the trailing slashes in routes.", () => {
        function checkChild(parentPath, child) {
            let childPath;
            if (child.path.startsWith("/")) {
                childPath = child.path;
            } else {
                childPath = parentPath + child.path;
            }
            expect(childPath).toMatch(/^\/.*\//);
            if (child.children && child.children.length) {
                for (const grandChild of child.children) {
                    checkChild(childPath, grandChild);
                }
            }
        }

        for (const route of routes) {
            // All top-level routes must start and end with a slash.
            expect(route.path).toMatch(/(\/.*\/)|[*]/);

            if (route.children && route.children.length) {
                for (const child of route.children) {
                    checkChild(route.path, child);
                }
            }
        }
    });
});
