import en from "./en.json";
import nl from "./nl.json";
import VueI18n from "vue-i18n";
import Vue, { getCurrentInstance } from "vue";

Vue.use(VueI18n);

export const i18n = new VueI18n({
    locale: window.viaduct?.locale || "en",
    fallbackLocale: "en",
    messages: {
        en: en,
        nl: nl,
    },
    silentFallbackWarn: true,
});

/* Workaround to use vue-i18n $t mixin as */
export const useTranslation = () => {
    const vm = getCurrentInstance();

    const t = (key: string, ...values: any) => {
        return vm!.proxy.$i18n.t(key, ...values);
    };
    return { t };
};
