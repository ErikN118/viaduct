import { Api, PaginatedResponse } from "./api";
import { AxiosResponse } from "axios";
import { User } from "../../types/user";
import Flask from "../../utils/flask";
import instance from "../../utils/axios";

type Mailtype = "none" | "maillist" | "mailbox";

export class Group {
    public id: number;
    public created: string;
    public modified: string;
    public name: string;
    public maillist: null | string;
    public mailtype: Mailtype;
}

class GroupApi extends Api {
    public async getGroup(groupId: string): Promise<AxiosResponse<Group>> {
        return this.get(Flask.url_for("api.group", { group: groupId }));
    }

    public async getGroupUsers(
        groupId: string,
        search: string,
        page: number
    ): Promise<AxiosResponse<PaginatedResponse<User>>> {
        return this.get(
            Flask.url_for("api.group.users", {
                group: groupId,
                search: search,
                page: page,
            })
        );
    }

    public async addGroupUsers(groupId: string, body: { user_ids: number[] }) {
        return this.post(
            Flask.url_for("api.group.users", {
                group: groupId,
            }),
            body
        );
    }

    public async removeGroupUsers(
        groupId: string,
        body: { user_ids: number[] }
    ) {
        return instance.delete(
            Flask.url_for("api.group.users", {
                group: groupId,
            }),
            { data: body }
        );
    }

    async createGroup(values) {
        return this.post<Group>(Flask.url_for("api.groups", {}), values);
    }

    async deleteGroup(groupId: string) {
        return this.delete(Flask.url_for("api.group", { group: groupId }));
    }

    async updateGroup(groupId: string, values) {
        return this.put<Group>(
            Flask.url_for("api.group", { group: groupId }),
            values
        );
    }
}

export const groupApi = new GroupApi();
