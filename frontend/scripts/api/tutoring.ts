import { Api, PaginatedResponse } from "./api";
import Flask from "../../utils/flask";
import { User } from "../../types/user";
import { Course } from "./examination";

export class Tutor {
    id: number;
    approved: boolean;
    course: Required<Course>;
    user: Pick<User, "id" | "first_name" | "last_name">;
    grade: number;
}

export class TutoringBody {
    course_id: number;
    notes: string;
    num_hours: number;
    tutor: Tutor;
    tutee: Tutor;
}

export class TutorRequest {
    course_id: number;
    grade: number;
    num_hours: number;
}

export class Tutoring extends TutoringBody {
    id: number;
}

class TutoringApi extends Api {
    public async getOpenTutorings() {
        return this.get<Tutoring[]>(Flask.url_for("api.tutorings"));
    }

    public async getUserTutoring(tutoringId: number) {
        return this.get<Tutoring>(
            Flask.url_for("api.user.tutoring", {
                user: "self",
                tutoring: tutoringId,
            })
        );
    }

    public async getUserTutorings() {
        return this.get<Tutoring[]>(
            Flask.url_for("api.user.tutorings", {
                user: "self",
            })
        );
    }

    public async createUserTutoring(tutoring: TutoringBody) {
        return this.post<Tutoring>(
            Flask.url_for("api.user.tutorings", { user: "self" }),
            tutoring
        );
    }

    public async updateUserTutoring(
        tutoringId: number,
        tutoring: TutoringBody
    ) {
        return this.patch<Tutoring>(
            Flask.url_for("api.user.tutoring", {
                user: "self",
                tutoring: tutoringId,
            }),
            tutoring
        );
    }

    public async deleteTutoring(tutoringId: number) {
        return this.delete(
            Flask.url_for("api.user.tutoring", {
                user: "self",
                tutoring: tutoringId,
            })
        );
    }

    async getTutorTutorings(userId: null | number) {
        return this.get<Tutoring[]>(
            Flask.url_for("api.tutors.tutorings", {
                user: userId ? userId : "self",
            })
        );
    }

    async acceptTutoring(userId: null | number, tutoringId: number) {
        return this.post(
            Flask.url_for("api.tutors.tutoring.accept", {
                user: userId ? userId : "self",
                tutoring: tutoringId,
            }),
            null
        );
    }

    async withdrawTutoring(userId: null | number, tutoringId: number) {
        return this.delete(
            Flask.url_for("api.tutors.tutoring", {
                user: userId ? userId : "self",
                tutoring: tutoringId,
            })
        );
    }

    async registerTutor(userId: null | number, values: TutorRequest) {
        return this.post(
            Flask.url_for("api.tutor.courses", {
                user: userId ? userId : "self",
            }),
            values
        );
    }

    async getTutors(search: string, page: number) {
        return this.get<PaginatedResponse<Tutor>>(
            Flask.url_for("api.tutors", {
                search: search,
                page: page,
            })
        );
    }

    async acceptTutor(tutor: Tutor) {
        return this.patch(
            Flask.url_for("api.tutor.course", {
                user: tutor.user.id,
                tutor: tutor.id,
            }),
            { approved: true }
        );
    }

    getTutorCourses(userId: null | number) {
        return this.get<Tutor[]>(
            Flask.url_for("api.tutor.courses", {
                user: userId ? userId : "self",
            })
        );
    }

    async removeTutor(userId: null | number, tutorId: number) {
        return this.delete(
            Flask.url_for("api.tutor.course", {
                user: userId ? userId : "self",
                tutor: tutorId,
            })
        );
    }
}

export const tutoringApi = new TutoringApi();
