import { Api } from "./api";
import { AxiosResponse } from "axios";
import Flask from "../../utils/flask";

class OAuthClientOwner {
    id: number;
    first_name: string;
    last_name: string;
}

export class OAuthClient {
    client_id: string;
    client_name: string;
    user: OAuthClientOwner;
}

class OAuthApi extends Api {
    public async revokeUserApplication(
        userId: number,
        clientId: number
    ): Promise<AxiosResponse<void>> {
        return this.post<void>(
            Flask.url_for("api.user.applications.revoke", {
                user: userId,
                client_id: clientId,
            }),
            {}
        );
    }

    public async revokeUserSelfApplication(
        clientId: number
    ): Promise<AxiosResponse<void>> {
        return this.post<void>(
            Flask.url_for("api.user.applications.revoke", {
                user: "self",
                client_id: clientId,
            }),
            {}
        );
    }

    public async getUserSelfApplications(): Promise<
        AxiosResponse<OAuthClient[]>
    > {
        return this.get(
            Flask.url_for("api.user.applications", { user: "self" })
        );
    }

    public async getUserApplications(
        userId: number
    ): Promise<AxiosResponse<OAuthClient[]>> {
        return this.get(
            Flask.url_for("api.user.applications", { user: userId })
        );
    }
}

export const oAuthApi = new OAuthApi();
