import { Api } from "./api";
import { AxiosPromise, AxiosResponse } from "axios";
import Flask from "../../utils/flask";
import { Page } from "../../types/page";

export type NavigationOrderEntry = {
    id: number;
    nl_title?: string;
    en_title?: string;
    href?: string;
    children: NavigationOrderEntry[];
};

export type NavigationEntryBase = {
    id: number;
    nl_title: string;
    en_title: string;
};

export type NavigationEntry =
    | (NavigationEntryBase & {
          type: "url";
          url: string;
          external: boolean;
          order_children_alphabetically: boolean;
      })
    | (NavigationEntryBase & {
          type: "page";
          page_id?: number;
          page?: Page;
          order_children_alphabetically: boolean;
      })
    | (NavigationEntryBase & { type: "activities" });

class NavigationApi extends Api {
    public getEntry(entryId: string): AxiosPromise<NavigationEntry> {
        return this.get<NavigationEntry>(
            Flask.url_for("api.navigation", { entry: entryId })
        );
    }

    public createEntry(data: NavigationEntry, parentId: string) {
        return this.post<NavigationEntry>(
            Flask.url_for(
                "api.navigations",
                parentId ? { parentId: parentId } : {}
            ),
            data
        );
    }

    async updateEntry(entryId: string, entry: NavigationEntry) {
        return this.put<NavigationEntry>(
            Flask.url_for("api.navigation", { entry: entryId }),
            entry
        );
    }

    public async getNavigation(): Promise<
        AxiosResponse<NavigationOrderEntry[]>
    > {
        return this.get(Flask.url_for("api.navigation.order"));
    }

    public async saveNavigation(
        navigation: NavigationOrderEntry[]
    ): Promise<AxiosResponse<void>> {
        return this.put<void>(
            Flask.url_for("api.navigation.order"),
            navigation
        );
    }

    async deleteEntry(entryId: number) {
        return this.delete(Flask.url_for("api.navigation", { entry: entryId }));
    }
}

export const navigationApi = new NavigationApi();
