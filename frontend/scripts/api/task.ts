import { Api } from "./api";
import Flask from "../../utils/flask";

type TaskStatus = "STARTED" | "FAILURE" | "SUCCESS" | "PENDING";

export interface TaskProgress {
    count: number;
    current: number;
}
export interface TaskResponse {
    task_id: string;
    status: TaskStatus;
    result: TaskProgress | null;
    traceback?: unknown;
    children?: string[];
    date_done?: string | null;
    name?: string;
    args?: any[];
    kwargs?: { [keyword: string]: any };
    worker?: string;
    retries?: number;
    queue?: string;
}

class TaskApi extends Api {
    getTask(taskId: string) {
        return this.get<TaskResponse>(
            Flask.url_for("api.async.task", {
                task_id: taskId,
            })
        );
    }

    syncCopernica() {
        return this.post<TaskResponse>(
            Flask.url_for("api.async.copernica"),
            undefined
        );
    }

    syncDatanose() {
        return this.post<TaskResponse>(
            Flask.url_for("api.async.datanose"),
            undefined
        );
    }
}

export const taskApi = new TaskApi();
