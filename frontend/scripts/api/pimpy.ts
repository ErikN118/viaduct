import { Api } from "./api";
import { AxiosPromise } from "axios";
import Flask from "../../utils/flask";
import { Groups } from "./user";

export type GroupMinutes = {
    minutes: Minute[];
    group: Groups;
};

export type GroupTasks = {
    tasks: Task[];
    group: Groups;
};

export type Minute = {
    id?: number;
    created?: string;
    content: string;
    group_id: number;
    date: string;
};

export type Task = {
    b32_id: string;
    status: string;
    title: string;
    group_id: number;
    content: string;
    line: number;
    minute_id: number;
    users: string[];
};

class PimpyApi extends Api {
    createMinute(minute: Minute): AxiosPromise<Minute> {
        return this.post<Minute>(
            Flask.url_for("api.group.minutes", { group: minute.group_id }),
            minute
        );
    }

    async getMinutes() {
        return this.get<GroupMinutes[]>(Flask.url_for("api.minutes"));
    }

    async getMinutesForGroup(groupId: number) {
        return this.get<GroupMinutes[]>(
            Flask.url_for("api.group.minutes", { group: groupId })
        );
    }

    async getTasks() {
        return this.get<GroupTasks[]>(Flask.url_for("api.tasks"));
    }

    async getTasksForGroup(groupId: number) {
        return this.get<GroupTasks[]>(
            Flask.url_for("api.group.tasks", { group: groupId })
        );
    }

    async getTasksForUser() {
        return this.get<GroupTasks[]>(
            Flask.url_for("api.user.tasks", { user: "self" })
        );
    }

    async updateTask(b32_id: string, data: Partial<Task>) {
        return this.patch<Task>(
            Flask.url_for("api.task", { task_id: b32_id }),
            data
        );
    }

    async createTask(data: any) {
        return this.post<Task>(Flask.url_for("api.tasks"), data);
    }

    async getMinute(minuteId: number) {
        return this.get<Minute>(
            Flask.url_for("api.minute", { minute: minuteId })
        );
    }
}

export const pimpyApi = new PimpyApi();
