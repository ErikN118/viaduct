import { Api } from "./api";
import { AxiosPromise } from "axios";
import Flask from "../../utils/flask";
import { MultilangString } from "../../types/page";
import { Activity } from "./activity";
import { User } from "../../types/user";
import { AlvEventData, AlvMetaData } from "../../types/alv";

export type AlvRequest = {
    name: MultilangString;
    date: string;
    chairman_user_id?: number;
    secretary_user_id?: string;
    activity_id?: string;
    minutes_accepted?: boolean | null;
};

export type AlvDocument = {
    id?: number;
    name: MultilangString;
    versions: number;
};

export type AlvResponse = {
    id?: number;
    name: MultilangString;
    date: string;
    chairman?: Pick<User, "id" | "first_name" | "last_name">;
    secretary?: Pick<User, "id" | "first_name" | "last_name">;
    activity?: Pick<Activity, "id" | "name" | "start_time" | "end_time">;
    documents?: AlvDocument[];
    minutes_accepted?: boolean | null;
    minutes_file_id?: number;
    meta: Exclude<AlvMetaData, "present" | "proxies">;
};

export type AlvParseMinutes = {
    meta_data: AlvMetaData;
    events: AlvEventData[];
    errors: string[];
};

class AlvApi extends Api {
    getAll() {
        return this.get<AlvResponse[]>(Flask.url_for("api.alvs"));
    }

    createAlvDocument(
        alvId: number,
        file: File,
        nlName: string,
        enName: string
    ) {
        const formData = new FormData();
        formData.append("file", file);
        formData.append("nl_name", nlName);
        formData.append("en_name", enName);

        return this.post<AlvDocument>(
            Flask.url_for("api.alv.documents", { alv: alvId }),
            formData
        );
    }

    uploadAlvDocument(docId: number, data: FormData) {
        return this.post<void>(
            Flask.url_for("api.alv.document", { doc_id: docId }),
            data
        );
    }

    uploadAlvMinutes(alvId: number, data: FormData) {
        return this.post<void>(
            Flask.url_for("api.alv.minutes", { alv: alvId }),
            data
        );
    }

    setAlvDocumentMetaData(docId: number, nlName: string, enName: string) {
        return this.put<void>(
            Flask.url_for("api.alv.document", { doc_id: docId }),
            {
                name: {
                    nl: nlName,
                    en: enName,
                },
            }
        );
    }

    getAlv(alvId: number): AxiosPromise<AlvResponse> {
        return this.get<AlvResponse>(Flask.url_for("api.alv", { alv: alvId }));
    }

    createAlv(values: AlvRequest): AxiosPromise<AlvResponse> {
        return this.post<AlvResponse>(Flask.url_for("api.alvs"), values);
    }

    updateAlv(alvId: number, values: AlvRequest): AxiosPromise<AlvResponse> {
        return this.patch<AlvResponse>(
            Flask.url_for("api.alv", { alv: alvId }),
            values
        );
    }

    deleteAlv(alvId: number): AxiosPromise<void> {
        return this.delete<void>(Flask.url_for("api.alv", { alv: alvId }));
    }

    parseAlvMinutes(data: FormData) {
        return this.post<AlvParseMinutes>(
            Flask.url_for("api.alv.parse_minutes"),
            data
        );
    }

    updateParsedMinutes(alvId: number, data: AlvParseMinutes) {
        return this.post<void>(
            Flask.url_for("api.alv.parsed_minutes", { alv: alvId }),
            data
        );
    }

    getParsedMinutes(alvId: number) {
        return this.get<AlvParseMinutes>(
            Flask.url_for("api.alv.parsed_minutes", { alv: alvId })
        );
    }
}

export const alvApi = new AlvApi();
