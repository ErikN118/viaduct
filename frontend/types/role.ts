export type Roles =
    | "ACTIVITY_WRITE"
    | "ALV_WRITE"
    | "CHALLENGE_WRITE"
    | "DOMJUDGE_ADMIN"
    | "EXAMINATION_WRITE"
    | "FILE_READ"
    | "FILE_WRITE"
    | "GROUP_READ"
    | "GROUP_WRITE"
    | "GROUP_PERMISSIONS"
    | "MOLLIE_READ"
    | "MAILINGLIST_ADMIN"
    | "NAVIGATION_WRITE"
    | "REDIRECT_WRITE"
    | "NEWS_WRITE"
    | "PAGE_WRITE"
    | "PIMPY_READ"
    | "PIMPY_WRITE"
    | "SEO_WRITE"
    | "USER_WRITE"
    | "VACANCY_READ"
    | "VACANCY_WRITE"
    | "COMPANY_WRITE"
    | "FINANCIAL_ADMIN"
    | "FORM_ADMIN"
    | "BANNER_ADMIN"
    | "TECHNICAL_ADMIN";

export type RoleDetails = {
    has_role: boolean;
    needs_tfa: boolean;
};

export type RolesResponse = {
    [R in Roles]: RoleDetails;
};
