export enum CompanyModuleType {
    None = "None",
    Company = "Company contract",
    CompanyJob = "Job",
    CompanyBanner = "Banner",
    CompanyProfile = "Profile",
}

export class Company {
    public id: string;
    public name = "";
    public website = "";
    public slug = "";
    public active = false;
    public contract_start_date = "";
    public contract_end_date = "";
    public logo = false;
}

export class CompanyInfo {
    public id: string;
    public slug: string;
    public name: string;
    public profile: boolean;
    public jobs: number[];
}

export class CompanyJobBase {
    public id: string;
    public company_id: string;

    public title_nl = "";
    public title_en = "";
    public contract_of_service = "";

    public description_nl = "";
    public description_en = "";

    public description_nl_raw?: string;
    public description_en_raw?: string;
}

export class CompanyJob extends CompanyJobBase {
    public contact_name = "";
    public contact_email = "";
    public contact_address = "";
    public contact_city = "";

    public website = "";
    public phone = "";

    // ModuleMixin
    public start_date = "";
    public end_date = "";
    public enabled = true;
    public active = true;
}

export class CompanyBannerBase {
    public company_id: number;
    public website = "";
    readonly logo_id;
}

export class CompanyBanner extends CompanyBannerBase {
    public id;
    public created = "";
    public active = false;

    // ModuleMixin
    public enabled = true;
    public start_date = "";
    public end_date = "";
}

export class CompanyProfilesList {
    public id;
    public company_id: number;
    public company_name: string;
    public company_slug: string;
}

export class CompanyProfile {
    public id: string;
    public name: string;
    public company_id: number;
    public created = "";
    readonly active = false;

    public description_nl = "";
    public description_en = "";

    public description_nl_raw?: string;
    public description_en_raw?: string;

    // ModuleMixin
    public enabled = true;
    public start_date = "";
    public end_date = "";
}

export class ContractOfServiceTypes {
    public contract_of_service: string[] = [];
}

export class CompanyJobs {
    company_id: string;
    company_name: string;
    contract_of_service: ContractOfServiceTypes;
    company_jobs: CompanyJob[];
}
