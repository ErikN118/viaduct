import { pathValidation } from "./form";

describe("form validators function correctly", () => {
    test("pathValidation accepts paths with slashes in the middle", () => {
        const callback = jest.fn();
        pathValidation(null, "laksjdf/asdflkj", callback);

        expect(callback).toBeCalledTimes(1);
        expect(callback).toBeCalledWith();
    });
    test("pathValidation does not accept paths starting with slashes", () => {
        const callback = jest.fn();
        pathValidation(null, "/laksjdf/asdflkj", callback);

        expect(callback).toBeCalledTimes(1);
        expect(callback).toBeCalledWith(
            "Please enter a path without slashes at the start or end."
        );
    });
    test("pathValidation does not accept paths ending with slashes", () => {
        const callback = jest.fn();
        pathValidation(null, "laksjdf/asdflkj/", callback);

        expect(callback).toBeCalledTimes(1);
        expect(callback).toBeCalledWith(
            "Please enter a path without slashes at the start or end."
        );
    });
});
