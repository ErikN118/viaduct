import { Component, Vue } from "vue-property-decorator";
import debounce from "./debounce";

export interface VuePaginatedAnt {
    refreshListView(): Promise<void>;
}

@Component
export class VuePaginatedAnt extends Vue {
    protected loading = false;
    protected pageCount = 1;
    protected pagination = { current: 1, pageSize: 10, total: 0 };

    private debouncedRefreshListView = debounce(this.refreshListView, 300);

    private handleTableChange(pagination, filters, sorter) {
        if (sorter["columnKey"]) {
            let sort = sorter["columnKey"];
            if (sorter["order"] === "descend") {
                sort = `-${sort}`;
            }
            this.sort = sort;
        }
        this.pagination = pagination;
        this.page = pagination.current;
        this.debouncedRefreshListView();
    }

    public get sort(): string {
        return (this.$route.query.sort as string) || "";
    }

    public set sort(value: string) {
        if (this.sort !== value) {
            this.page = 1;
            this.$router.replace({
                query: {
                    ...this.$route.query,
                    sort: value,
                },
            });
        }

        this.debouncedRefreshListView();
    }

    public get search(): string {
        return (this.$route.query.search as string) || "";
    }

    public set search(value: string) {
        if (this.search !== value) {
            this.page = 1;
            this.$router.replace({
                query: {
                    ...this.$route.query,
                    search: value,
                },
            });
        }

        this.debouncedRefreshListView();
    }

    get page(): number {
        const page = parseInt(this.$route.query.page as string) || 1;
        this.pagination.current = page;
        return page;
    }

    set page(value: number) {
        if (this.page !== value) {
            this.pagination.current = value;
            this.$router.replace({
                query: {
                    ...this.$route.query,
                    page: value.toString(),
                },
            });
        }
        this.debouncedRefreshListView();
    }

    protected async pageSwitch(pageNew: number): Promise<void> {
        this.page = pageNew;
        await this.refreshListView();
    }
}
