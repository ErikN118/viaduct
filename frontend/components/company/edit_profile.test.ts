import { createLocalVue } from "@vue/test-utils";
import { render } from "@testing-library/vue";
import VueI18n from "vue-i18n";
import EditCompanyProfileForm from "./edit_profile.vue";
import VueRouter from "vue-router";

jest.mock("../../utils/flask.ts");
const localVue = createLocalVue();
localVue.use(VueRouter);
localVue.use(VueI18n);
import { i18n } from "../../translations";

const consoleSpy = jest.spyOn(console, "error");

describe(EditCompanyProfileForm.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(EditCompanyProfileForm, {
            localVue: localVue,
            router: new VueRouter(),
            i18n: i18n,
        });

        getByText("Create company profile");
    });
});
