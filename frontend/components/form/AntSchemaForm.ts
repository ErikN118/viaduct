import { Component, Vue } from "vue-property-decorator";
import {
    ValidationRule,
    WrappedFormUtils,
} from "ant-design-vue/types/form/form";
import { Button, Checkbox, Form, Input, Select } from "ant-design-vue";
import { CreateElement, VNode, VNodeDirective } from "vue";
import {
    emailValidation,
    studentIdValidation,
    validateAntForm,
} from "../../utils/form";
import { schemaApi } from "../../scripts/api/schema";
import {
    JsonSchema,
    JsonSchemaBooleanProperty,
    JsonSchemaIntegerProperty,
    JsonSchemaProperty,
    JsonSchemaStringProperty,
} from "../../types/schema";
import FormUserField from "./form_user_field.vue";
import Recaptcha from "./Recaptcha.vue";

export interface AntSchemaForm {
    schemaName: string;

    save(r: unknown): Promise<void>;
}

@Component({
    components: {
        "a-form": Form,
        "a-form-item": Form.Item,
        "a-input": Input,
        "a-button": Button,
        "a-select": Select,
        "a-checkbox": Checkbox,
        "a-select-option": Select.Option,
        "a-textarea": Input.TextArea,
        "form-user-field": FormUserField,
        Recaptcha: Recaptcha,
    },
})
export class AntSchemaForm extends Vue {
    form: WrappedFormUtils | null = null;
    schema: JsonSchema = {
        title: "Not loaded",
        type: "object",
        properties: {},
    };
    schemaName = "Set this in children";
    loading = false;

    async created(): Promise<void> {
        this.schema = (await schemaApi.getSchema(this.schemaName)).data;
        this.form = this.$form.createForm(this);
        this.$i18n.mergeLocaleMessage("nl", {
            required: "{field} is required",
        });
        this.$i18n.mergeLocaleMessage("en", {
            required: "{field} is vereist",
        });
    }

    render(createElement: CreateElement): VNode {
        if (!this.schema.properties) {
            return createElement("div");
        }

        const children: VNode[] = Object.entries(
            this.schema.properties
        ).map(([propKey, prop]) =>
            this.renderFormItem(createElement, propKey, prop)
        );

        // submit button
        children.push(
            createElement("a-form-item", [
                createElement(
                    "a-button",
                    {
                        props: {
                            loading: this.loading,
                        },
                        attrs: {
                            type: "primary",
                            htmlType: "submit",
                        },
                    },
                    [this.$t("Submit") as string]
                ),
            ])
        );

        return createElement("div", [
            createElement(
                "a-form",
                {
                    props: {
                        form: this.form,
                    },

                    on: {
                        submit: async (e: Event): Promise<void> => {
                            this.loading = true;
                            e.preventDefault();
                            let values;
                            try {
                                values = await validateAntForm(this.form!);
                                await this.save(values);
                            } catch (e) {
                                return;
                            } finally {
                                this.loading = false;
                            }
                        },
                    },
                },
                children
            ),
        ]);
    }

    renderFormItem(
        createElement: CreateElement,
        propKey: string,
        prop: JsonSchemaProperty
    ): VNode {
        if (prop["$ref"]) {
            throw new Error(
                "Nested schemas are not yet supported in AntSchemaForm."
            );
        }
        let child: VNode;
        switch (prop.type) {
            case "string":
                child = this.renderFormInput(createElement, propKey, prop);
                break;
            case "boolean":
                child = this.renderFormBool(createElement, propKey, prop);
                break;
            case "integer":
                child = this.renderFormInteger(createElement, propKey, prop);
                break;
            default:
                // This is here to force correct types.
                this.assertNever(prop);
        }

        return createElement(
            "a-form-item",
            {
                props: {
                    label: this.renderFormItemLabel(propKey),
                    labelCol: {
                        span: 24,
                        lg: { span: 4 },
                    },
                    wrapperCol: {
                        span: 24,
                        lg: { span: 20 },
                    },
                },
            },
            [child]
        );
    }

    renderFormItemLabel(propKey: string): string {
        return this.$t(`form.${propKey}.label`) as string;
    }

    renderFormInput(
        createElement: CreateElement,
        propKey: string,
        prop: JsonSchemaStringProperty
    ): VNode {
        if (prop.enum) {
            return this.renderFormSelect(
                createElement,
                propKey,
                prop,
                prop.enum
            );
        }

        let el = "a-input";
        const formatRules: ValidationRule[] = [];
        switch (prop.format) {
            case "textarea":
                el = "a-textarea";
                break;
            case "email":
                formatRules.push({
                    validator: emailValidation,
                });
                break;
            case "studentid":
                formatRules.push({
                    validator: studentIdValidation,
                });
                break;
            case "recaptcha":
                el = "Recaptcha";
                break;
        }
        return createElement(el, {
            attrs: { placeholder: this.renderFormItemLabel(propKey) },
            directives: [
                this.renderDecoratorDirective(propKey, prop, formatRules),
            ],
        });
    }

    renderFormSelect(
        createElement: CreateElement,
        propKey: string,
        prop: JsonSchemaStringProperty,
        enum_: string[]
    ): VNode {
        const children: VNode[] = enum_.map((v) => {
            return createElement("a-select-option", { attrs: { value: v } }, [
                this.$t(`form.${propKey}.value.${v}`) as string,
            ]);
        });

        return createElement(
            "a-select",
            {
                directives: [this.renderDecoratorDirective(propKey, prop)],
            },
            children
        );
    }

    renderDecoratorDirective(
        propKey: string,
        prop: JsonSchemaProperty,
        rules: ValidationRule[] = []
    ): VNodeDirective {
        let required = {};
        if (this.schema.required?.includes(propKey))
            required = {
                required: true,
                message: this.$t(`required`, {
                    field: this.renderFormItemLabel(propKey),
                }),
            };

        let initialValue = {};
        if (prop.default) {
            initialValue = {
                initialValue: prop.default,
            };
        }
        return {
            name: "decorator",
            value: [propKey, { ...initialValue, rules: [required, ...rules] }],
        };
    }

    renderFormBool(
        createElement: CreateElement,
        propKey: string,
        prop: JsonSchemaBooleanProperty
    ): VNode {
        return createElement("a-checkbox", {
            directives: [this.renderDecoratorDirective(propKey, prop)],
        });
    }

    private renderFormInteger(
        createElement: CreateElement,
        propKey: string,
        prop: JsonSchemaIntegerProperty
    ) {
        let el: string;
        switch (prop.format) {
            case "user":
                el = "form-user-field";
                break;
            default:
                throw new Error(`Unknown integer type ${prop.format}`);
        }
        return createElement(el, {
            attrs: { placeholder: this.renderFormItemLabel(propKey) },
            directives: [this.renderDecoratorDirective(propKey, prop)],
        });
    }

    private assertNever(s: never): never {
        throw new Error(
            `Unexpected value ${s}. Should have been never and this should not be called.`
        );
    }
}
