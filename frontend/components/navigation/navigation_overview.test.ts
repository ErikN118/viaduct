import { createLocalVue } from "@vue/test-utils";
import { render } from "@testing-library/vue";
import VueI18n from "vue-i18n";
import NavigationOverview from "./navigation_overview.vue";
import VueRouter from "vue-router";

jest.mock("../../utils/flask.ts");
const localVue = createLocalVue();
localVue.use(VueRouter);
localVue.use(VueI18n);
import { i18n } from "../../translations";

const consoleSpy = jest.spyOn(console, "error");

describe(NavigationOverview.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(NavigationOverview, {
            localVue: localVue,
            router: new VueRouter(),
            i18n: i18n,
            provide: { locale: "en" },
        });

        getByText("Navigation overview");
    });
});
