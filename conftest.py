import logging
import os

import pytest
import requests_mock
from celery.signals import after_task_publish
from flask.testing import FlaskClient
from lxml import html
from sqlalchemy import create_engine, inspect
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.session import close_all_sessions

from config import ConnectionTestUrls

# Update the connection URLs in the environment before loading anything
# from :module:`app`.
connection_urls = ConnectionTestUrls()
os.environ.update(
    {
        "SQLALCHEMY_DATABASE_URI": connection_urls.sqlalchemy_database_test_uri,
        "BROKER_URL": connection_urls.broker_test_url,
    }
)

from app import app as _app  # noqa: E402
from app import db as _db  # noqa: E402
from app.models.group import Group  # noqa: E402
from app.models.oauth.client import OAuthClient  # noqa: E402
from app.models.oauth.token import OAuthToken  # noqa: E402
from app.models.role_model import GroupRole  # noqa: E402
from app.models.user import User  # noqa: E402
from app.oauth_scopes import Scopes  # noqa: E402
from app.roles import Roles  # noqa: E402

# TODO Make the session a parameter of the service and swap back to service
# from app.service import role_service
from app.utils.json import JSONEncoder  # noqa: E402

collect_ignore = ["node_modules", "builds"]
pytest_plugins = [
    "test.fixtures.models",
    "test.fixtures.activity",
    "test.fixtures.page",
    "test.fixtures.navigation",
    "test.fixtures.user",
    "test.fixtures.alv",
    "test.fixtures.newsletter",
]


@pytest.fixture(scope="session")
def app():
    """Session-wide test `Flask` application."""
    settings_override = {
        "TESTING": True,
        "SECRET_KEY": "TEST_SECRET",
    }

    # Establish an application context before running the tests.
    _app.config.update(settings_override)

    # The pytest 'caplog' fixture relies on all logging message propagating to the root
    # logger. We have propagate=false in logging.conf
    logging.getLogger("app").propagate = True

    from app import jinja_env
    from app.utils import context_filters

    jinja_env
    context_filters
    from app import init_app

    app.json_encoder = JSONEncoder

    init_app()

    yield _app


@pytest.fixture(autouse=True)
def worker():
    from app import worker as _worker

    _worker.config_from_object(
        {
            "result_backend": "rpc://",
            "broker_url": connection_urls.broker_test_url,
            "task_routes": {"*": "celery_test_queue"},
            "broker_transport_options": {"max_retries": 1},
        }
    )
    _worker.control.purge()
    return _worker


@pytest.fixture(scope="session")
def db_tables(app):
    with app.app_context():
        _db.create_all()

    yield

    with app.app_context():
        _db.drop_all()


@pytest.fixture(autouse=True, scope="function")
def db(app, db_tables):
    """Session-wide test database."""

    inspector = inspect(_db.engine)
    with app.app_context():
        close_all_sessions()
        meta = _db.metadata
        table_names = inspector.get_table_names()
        for table in reversed(meta.sorted_tables):
            if table.name in table_names:
                _db.session.execute(table.delete())
        _db.session.commit()

    yield _db


@pytest.fixture(scope="function")
def db_session(app, db):
    """
    Creates a new database connection for tests.

    This connection is completely separate from the connection
    flask uses to communicate with the database.
    """
    some_engine = create_engine(app.config["SQLALCHEMY_DATABASE_URI"], future=True)

    Session = sessionmaker(bind=some_engine, future=True)

    # create a Session
    session = Session()

    yield session

    session.close()


@pytest.fixture(autouse=True)
def app_context(app):
    with app.app_context():
        yield


@pytest.fixture(autouse=True, scope="function")
def requests_mocker():
    with requests_mock.Mocker() as m:
        yield m


@pytest.fixture
def admin_group(db_session):
    group = Group(name="Admin")
    db_session.add(group)
    db_session.commit()
    all_roles = [choice for choice in Roles]
    # TODO Make the session a parameter of the service and swap back to service
    # role_service.set_roles_for_group(group.id, all_roles)
    db_session.add_all(
        [GroupRole(group_id=group.id, role=role.name) for role in all_roles]
    )
    db_session.commit()
    return group


@pytest.fixture
def oauth_client(db_session):
    oauth_client = OAuthClient(
        client_id="admin",
        client_secret="admin",
        auto_approve=False,
    )
    oauth_client.set_client_metadata(
        {
            "client_name": "Test client",
            "response_types": ["code", "token"],
            "scope": "\n".join(s.name for s in Scopes),
            "redirect_uris": ["https://localhost:5000"],
            "grant_types": ["authorization_code", "refresh_token"],
            "token_endpoint_auth_method": "client_secret_post",
        }
    )

    db_session.add(oauth_client)
    db_session.commit()
    return oauth_client


@pytest.fixture
def admin_token(db_session, admin_user, oauth_client):
    oauth_token = OAuthToken(
        user=admin_user,
        client_id=oauth_client.client_id,
        token_type="Bearer",
        access_token="SOMEACCESSTOKEN",
        refresh_token="SOMEREFRESHTOKEN",
        issued_at=0,
        expires_in=2069451803,
        scope="\n".join(s.name for s in Scopes),
    )
    db_session.add(oauth_token)
    db_session.commit()
    return oauth_token


@pytest.fixture
def member_token(db_session, member_user, oauth_client):
    oauth_token = OAuthToken(
        user=member_user,
        client_id=oauth_client.client_id,
        token_type="Bearer",
        access_token="SOMEACCESSTOKEN2",
        refresh_token="SOMEREFRESHTOKEN2",
        issued_at=0,
        expires_in=2069451803,
        scope="\n".join(s.name for s in Scopes),
    )
    db_session.add(oauth_token)
    db_session.commit()
    return oauth_token


@pytest.fixture
def unpaid_user_token(db_session, unpaid_user, oauth_client):
    oauth_token = OAuthToken(
        user=unpaid_user,
        client_id=oauth_client.client_id,
        token_type="Bearer",
        access_token="SOMEACCESSTOKEN3",
        refresh_token="SOMEREFRESHTOKEN3",
        issued_at=0,
        expires_in=2069451803,
        scope="\n".join(s.name for s in Scopes),
    )
    db_session.add(oauth_token)
    db_session.commit()
    return oauth_token


class CustomClient(FlaskClient):
    def __init__(
        self, *args, headers: dict[str, str] = None, accept_language="en", **kwargs
    ):
        if headers is None:
            headers = dict()

        headers["Accept-Language"] = accept_language

        super().__init__(*args, **kwargs)
        self._headers = headers

    def login(self, user: User):
        # We need to clear the authentication header, as the server will
        # prioritize tokens over sessions.
        if "Authorization" in self._headers:
            del self._headers["Authorization"]

        with self.session_transaction() as sess:  # type: ignore[func-returns-value]
            # Copied from flask_login.login_user()
            # Use this instead of POST /login as it requires no password
            # validation
            sess["_user_id"] = user.id

    # We need to overwrite every single http method, as overwriting the
    # underlying super().open() method will cause issues.
    # See  bug report at flask: https://github.com/pallets/flask/issues/3396
    def get(self, *args, follow_redirects=True, **kwargs):
        kwargs["headers"] = self._headers
        kwargs["follow_redirects"] = follow_redirects
        return _wrap_response(super().get(*args, **kwargs))

    def post(self, *args, follow_redirects=True, **kwargs):
        kwargs["headers"] = self._headers
        kwargs["follow_redirects"] = follow_redirects
        return _wrap_response(super().post(*args, **kwargs))

    def patch(self, *args, follow_redirects=True, **kwargs):
        kwargs["headers"] = self._headers
        kwargs["follow_redirects"] = follow_redirects
        return _wrap_response(super().patch(*args, **kwargs))

    def put(self, *args, follow_redirects=True, **kwargs):
        kwargs["headers"] = self._headers
        kwargs["follow_redirects"] = follow_redirects
        return _wrap_response(super().put(*args, **kwargs))

    def delete(self, *args, follow_redirects=True, **kwargs):
        kwargs["headers"] = self._headers
        kwargs["follow_redirects"] = follow_redirects
        return _wrap_response(super().delete(*args, **kwargs))


def _wrap_response(rv):
    # wrap the flask Response class with more attributes that are useful
    try:
        rv.content_main = str(
            html.tostring(
                html.fromstring(rv.data).xpath('//*[@id="container-main"]')[0]
            )
        )
    except Exception:
        rv.content_main = None

    return rv


@pytest.fixture
def admin_client(app, admin_token, admin_user):
    app.test_client_class = CustomClient
    with app.test_client(
        headers={"Authorization": f"Bearer {admin_token.access_token}"}
    ) as test_client:
        yield test_client


@pytest.fixture
def member_client(app, member_token, member_user):
    app.test_client_class = CustomClient
    with app.test_client(
        headers={"Authorization": f"Bearer {member_token.access_token}"}
    ) as test_client:
        yield test_client


@pytest.fixture
def unpaid_user_client(app, unpaid_user_token, unpaid_user):
    app.test_client_class = CustomClient
    with app.test_client(
        headers={"Authorization": f"Bearer {unpaid_user_token.access_token}"}
    ) as test_client:
        yield test_client


@pytest.fixture
def anonymous_client(app):
    app.test_client_class = CustomClient
    return app.test_client()


TASK_LIST = []


@after_task_publish.connect
def add_to_task_list(**kwargs):
    """Track all tasks published for assertions using the task_list fixture."""
    TASK_LIST.append(kwargs)


@pytest.fixture(autouse=True)
def task_list():
    """After every test, empty the task list."""
    yield TASK_LIST
    TASK_LIST.clear()
