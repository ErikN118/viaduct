"""add ov slide settings.

Revision ID: 8115890f14f5
Revises: 731d0b19cc3c
Create Date: 2020-02-21 17:07:07.716979

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.orm import relationship, declarative_base

# revision identifiers, used by Alembic.
revision = "8115890f14f5"
down_revision = "731d0b19cc3c"

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


settings_table = db.table(
    "setting",
    db.Column("key", db.String(128), unique=True, nullable=False),
    db.Column("value", db.String(256)),
)


def upgrade():
    create_session()
    settings = [
        {"key": "OV_TRAIN_LOCATION", "value": "station-amsterdam-science-park"},
        {"key": "OV_BUS_LOCATION", "value": "amsterdam/bushalte-university-college"},
    ]
    op.bulk_insert(settings_table, settings)


def downgrade():
    create_session()


# vim: ft=python
