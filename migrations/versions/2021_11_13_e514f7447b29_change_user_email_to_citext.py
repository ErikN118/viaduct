"""Change user email to citext

Revision ID: e514f7447b29
Revises: 370cd2ee5993
Create Date: 2021-11-13 14:51:37.607046

"""
from alembic import op
import sqlalchemy as sa
from app.models.citext import CIText


from sqlalchemy.orm import relationship, declarative_base

# revision identifiers, used by Alembic.
revision = "e514f7447b29"
down_revision = "28655b495cea"


Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


def upgrade():
    create_session()

    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column(
        "user",
        "email",
        existing_type=sa.VARCHAR(length=200),
        type_=citext.CIText(),
        existing_nullable=False,
    )
    # ### end Alembic commands ###


def downgrade():
    create_session()

    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column(
        "user",
        "email",
        existing_type=citext.CIText(),
        type_=sa.VARCHAR(length=200),
        existing_nullable=False,
    )
    # ### end Alembic commands ###


# vim: ft=python
