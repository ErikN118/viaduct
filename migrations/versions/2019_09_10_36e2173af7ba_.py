"""empty message.

Revision ID: 36e2173af7ba
Revises: ('49d9debe1d84', '5c784b13c22f')
Create Date: 2019-09-10 14:00:29.795593

"""
from alembic import op
import sqlalchemy as sa


from sqlalchemy.orm import relationship, declarative_base

# revision identifiers, used by Alembic.
revision = "36e2173af7ba"
down_revision = ("49d9debe1d84", "5c784b13c22f")

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


def upgrade():
    create_session()


def downgrade():
    create_session()


# vim: ft=python
