"""empty message.

Revision ID: c2841207d9f1
Revises: ('7573fb8f0ba2', 'd5efaa9aacaa')
Create Date: 2019-10-04 15:23:26.923020

"""
from alembic import op
import sqlalchemy as sa


from sqlalchemy.orm import relationship, declarative_base

# revision identifiers, used by Alembic.
revision = "c2841207d9f1"
down_revision = ("7573fb8f0ba2", "d5efaa9aacaa")

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


def upgrade():
    create_session()


def downgrade():
    create_session()


# vim: ft=python
