"""Resize avatars.

Revision ID: 5f39098b589a
Revises: c2841207d9f1
Create Date: 2019-10-12 13:34:25.315375

"""
import sqlalchemy as sa
from alembic import op
from hashfs import HashFS
from io import BytesIO
from PIL import Image
from sqlalchemy.orm import relationship, declarative_base

from app.enums import FileCategory

# revision identifiers, used by Alembic.
revision = "5f39098b589a"
down_revision = "122527a3693d"

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


class User(db.Model):
    __tablename__ = "user"

    id = db.Column(db.Integer, primary_key=True)
    avatar_file_id = db.Column(db.Integer, db.ForeignKey("file.id"))


class File(db.Model):
    __tablename__ = "file"

    id = db.Column(db.Integer, primary_key=True)
    hash = db.Column(db.String(200), nullable=False)
    extension: str = db.Column(db.String(20), nullable=False)
    display_name = db.Column(db.String(200))

    category = db.Column(db.Enum(FileCategory, name="file_category"), nullable=False)


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


def upgrade():
    create_session()
    files = db.session.query(File).filter_by(category=FileCategory.USER_AVATAR)

    deleted = []
    hashfs = HashFS("app/uploads/")
    for file_ in files:
        try:
            with hashfs.open(file_.hash) as data:
                # Check if the image is already small enough
                im = Image.open(data)
                width, height = im.size
                if width <= 400 and height <= 400:
                    continue

                # Save the resized image
                data = im.thumbnail((400, 400))
                out = BytesIO()
                im.save(out, format="jpeg")

            # Replace the file with the thumbnailed file
            deleted.append(file_.hash)
            address = hashfs.put(out)
            file_.hash = address.id

            db.session.add(file_)
        except OSError:
            # File does not exist in hashfs.
            pass

    db.session.commit()

    # Delete files last, so they won't be deleted
    # if something goes wrong earlier.
    for hash_ in deleted:
        hashfs.delete(hash_)

    print(f"Resized {len(deleted)} avatars")


def downgrade():
    pass


# vim: ft=python
